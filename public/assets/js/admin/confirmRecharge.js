function confirmRecharge(url, title, confirmButtonText) {
    Swal.fire({
        title: `Do you want to ${title}?`,
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: confirmButtonText,
    }).then(function (result) {
        if (result.isConfirmed) {
            window.location.href = url;
        }
    });
}
