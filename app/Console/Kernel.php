<?php

namespace App\Console;

use App\Models\LotteryDate;
use App\Services\DrawIdService;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $draw_id = (new DrawIdService())->getPreviousDrawId();

        $schedule->command("result:create {$draw_id}")->when(function () {
            $dates = LotteryDate::all()->pluck('date')->toArray();
            $currentDate = date('m-d');
            return in_array($currentDate, $dates);
        })->hourly();

        $schedule->command("logs:clean")->cron('0 0 */3 * *');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
