<?php

namespace App\Console\Commands;

use App\Actions\FindResult;
use App\Actions\SendNotification as ActionsSendNotification;
use App\Actions\ValidateDrawId;
use App\Models\Notification;
use App\Models\Result;
use App\Models\TicketResult;
use App\Models\User;
use App\Models\UserTicket;
use App\Notifications\SendNotification;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Validator;

class CalculateResult extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'result:calculate {draw_id} {--recalculate}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $draw_id = $this->argument('draw_id');
        $options = $this->options();

        // Validate user input
        $this->info('Validating draw id...');

        $error = (new ValidateDrawId())->execute($draw_id);

        if ($error) {
            $this->error($error);
        }

        $draw_id_date = Carbon::parse($draw_id);

        $calculated = TicketResult::where('draw_id', $draw_id_date)->first();

        if ($calculated) {
            $this->info('Results already calculated for this draw id:' . $draw_id);
            // return 0;

            if (!$options['recalculate']) {
                return;
            }

            $this->info('Recalculating...');

            TicketResult::where('draw_id', $draw_id_date)->delete();

            $this->info('Deleted...');
        }

        $results = Result::where('date', $draw_id_date)->get();
        if (!count($results)) {
            $this->info('There is no results for this draw id:' . $draw_id);
            return 0;
        }
        $user_tickets = UserTicket::where('draw_id', $draw_id_date)->get();

        if (!count($user_tickets)) {
            $this->info('There is no user tickets for this draw id:' . $draw_id);
            return 0;
        }

        $winner_tickets = [];

        if (count($user_tickets)) {
            foreach ($results as $result) {
                // calculate result by users' tickets
                foreach ($user_tickets as $user_ticket) {
                    $found = (new FindResult($result))->execute([$user_ticket->number]);

                    if (count($found)) {
                        if (isset($winner_tickets[$user_ticket->user->id])) {
                            array_push(
                                $winner_tickets[$user_ticket->user->id],
                                $result->result_type->name
                            );
                        } else {
                            $winner_tickets[$user_ticket->user->id] = [$result->result_type->name];
                        }

                        $user_ticket->ticket_results()->create([
                            'result_id' => $result->id,
                            'result_type_id' => $result->result_type->id,
                            'amount' => $result->amount,
                            'draw_id' => $draw_id_date,
                        ]);
                    }
                }
            }
        }

        foreach ($winner_tickets as $user_id => $result_types) {
            $price = '';
            $result_types = array_unique($result_types);

            if (count($result_types) === 1) {
                $price = 'price';

                $result_type_str = trans('ticket.' . $result_types[0]);
            } else {
                $price = 'prices';

                foreach ($result_types as $key => $result_type) {
                    if ($key === count($result_types) - 1) {
                        $result_types[$key] = trans('ticket.and') . ' ' . trans('ticket.' . $result_type);
                        continue;
                    }
                    $result_types[$key] = trans('ticket.' . $result_type);
                }
                $result_type_str = implode(', ', $result_types);
            }

            $title = __("ticket.won.title");
            $message = __("ticket.won.message") . ' ' . $result_type_str . ' ' . $price . '.';

            $user = User::find($user_id);
            $user->notify(new SendNotification($title, $message));

            if (!app()->environment('local')) {
                (new ActionsSendNotification())->execute([$user->noti_token], [
                    'Type' => 'Notification',
                    'title' => $title,
                    'message' => $message,
                ]);
                continue;
            }
        }
        $this->info('Calculated');
        return 0;
    }
}
