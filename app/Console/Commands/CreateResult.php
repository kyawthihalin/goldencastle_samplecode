<?php

namespace App\Console\Commands;

use App\Actions\CreateResult as ActionsCreateResult;
use App\Actions\ValidateDrawId;
use App\Models\Result;
use App\Models\ResultType;
use App\Services\LotteryApi\LotteryService;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class CreateResult extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'result:create {draw_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $draw_id = $this->argument('draw_id');

        (new ActionsCreateResult())->execute($draw_id);
    }
}
