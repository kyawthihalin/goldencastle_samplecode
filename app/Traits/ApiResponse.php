<?php

namespace App\Traits;

use App\Services\Crypto;

trait ApiResponse
{
    protected function responseWithSuccess($message = 'Success', $data = [], $code = 200)
    {
        return response()->json([
            'message' => $message,
            'data' => (new Crypto())->encrypt($data),
            'status' => $code
        ], $code);
    }

    protected function responseWithError($message = '', $data = [], $code = 400)
    {
        return response()->json([
            'message' => $message,
            'data' => (new Crypto())->encrypt($data),
            'status' => $code,
        ], $code);
    }
}
