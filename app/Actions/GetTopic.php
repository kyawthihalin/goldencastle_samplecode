<?php

namespace App\Actions;

use App\Models\Topic;

class GetTopic
{
    public function execute(): array
    {
        $topics = Topic::all();
        $topic_arr = [];
        foreach ($topics as $topic) {
            $topic_arr[$topic->id] = $topic->topic_name;
        }
        return $topic_arr;
    }
}
