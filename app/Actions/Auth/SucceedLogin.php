<?php

namespace App\Actions\Auth;

use Illuminate\Http\Request;

class SucceedLogin
{
    public function handle($user, Request $request): void
    {
        $user->device_name = $request->device_name;
        $user->device_model = $request->device_model;
        $user->os_version = $request->os_version;
        $user->os_type = $request->os_type;
        $user->app_version_id = $request->app_version_id;
        $user->noti_token = $request->noti_token;
        $user->language = $request->language;
        $user->save();
    }
}
