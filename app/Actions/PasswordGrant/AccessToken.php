<?php

namespace App\Actions\PasswordGrant;

use App\Models\Agent;
use App\Models\User;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\Http;
use Laravel\Passport\Client as PassportClient;

class AccessToken
{
    private $user;

    public function __construct($user)
    {
        $this->user = $user;
    }

    public function execute($user_name, $password): Response
    {
        $this->revoke();
        $client = $this->getClientByUser();

        if (!$client) {
            abort(500);
        }
        $response = Http::asForm()->post(url('/') . '/oauth/token', [
            'grant_type' => 'password',
            'client_id' => $client->id,
            'client_secret' => $client->secret,
            'username' => $user_name,
            'password' => $password,
        ]);

        return $response;
    }

    private function revoke()
    {
        if ($this->user->tokens->count() > 0) {
            $this->user->tokens->each(function ($auth_token, $key) {
                $auth_token->revoke();
            });
        }
    }

    private function getClientByUser()
    {
        if ($this->user instanceof User) {
            return PassportClient::whereName('User Password Grant Client')->first();
        } else if ($this->user instanceof Agent) {
            return PassportClient::whereName('Agent Password Grant Client')->first();
        }
    }
}
