<?php

namespace App\Actions;

use App\Services\DrawIdService;
use Carbon\Carbon;

class GetLastDrawsDate
{
    public function execute(int $count = 1, $date = null)
    {
        $current_last_draw = (new DrawIdService())->getPreviousDrawId($date);

        $draw_dates = [$current_last_draw];
        while ($count > 1) {
            $date = Carbon::parse($current_last_draw)->subDay();

            $current_last_draw = (new DrawIdService())->getPreviousDrawId($date->format('Y-m-d'));

            array_push($draw_dates, $current_last_draw);

            $count--;
        }

        return $draw_dates;
    }
}
