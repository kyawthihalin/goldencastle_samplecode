<?php

namespace App\Actions;

use Carbon\Carbon;
// unused
class GenerateCurrentDraw
{
    private $draw_day;
    private $draw_month;
    private $draw_year;

    public function execute($date = null)
    {
        $now = $date ? Carbon::parse($date) : now();
        $current_year = $now->year;
        $current_month = $now->month;
        $current_day = $now->day;

        $this->draw_day = 16;
        $this->draw_month = $current_month;
        $this->draw_year = $current_year;

        if ($current_day >= 16) {
            $this->draw_day = 1;
            $this->draw_month++;
        }

        if ($current_month === 1 && $current_day < 17) {
            $this->draw_day = 17;
            $this->draw_month = 1;
            return $this->getDrawDate();
        }

        if ($current_month === 4 && $current_day >= 16) {
            $this->draw_day = 2;
            return $this->getDrawDate();
        }

        if ($current_month === 5 && $current_day < 2) {
            $this->draw_day = 2;
            return $this->getDrawDate();
        }

        if ($current_month === 12 && $current_day >= 16 && $current_day < 30) {
            $this->draw_day = 30;
            $this->draw_month = 12;
            return $this->getDrawDate();
        }

        if ($current_month === 12 && $current_day >= 30) {
            $this->draw_day = 17;
            $this->draw_month = 1;
            $this->draw_year++;

            return $this->getDrawDate();
        }
        return $this->getDrawDate();
    }

    private function getDrawDate()
    {
        return date('Y-m-d', strtotime($this->draw_year . "/" . $this->draw_month . "/" . $this->draw_day));
    }
}
