<?php

namespace App\Actions;

use App\Models\AppFile;
use App\Models\TicketPhoto;
use Carbon\Carbon;

class GetTicketPhotoUrl
{
    public function execute(): string
    {
        $type = now()->day < 16 ? 'first draw photo' : 'last draw photo';
        
        $file = AppFile::whereType($type)->pluck('file')->first();

        return url('/') . '/' . $file;
    }
}
