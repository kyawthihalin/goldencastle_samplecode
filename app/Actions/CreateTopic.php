<?php

namespace App\Actions;
class CreateTopic
{
    public function execute($tokens,$topic = ''): void
    {
        $curl = curl_init();
        $tokens = json_encode($tokens);
        $topic  = json_encode($topic);
        curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://iid.googleapis.com/iid/v1:batchAdd',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS =>'{
            "to":'.$topic.',
            "registration_tokens":'.$tokens.',
        }',
        CURLOPT_HTTPHEADER => array(
            'Authorization: key='.config("serverkey.key"),
            'Content-Type: application/json'
        ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);
    }
}
