<?php

namespace App\Actions;

use App\Models\Result;
use App\Models\ResultType;
use App\Services\LotteryApi\LotteryService;
use Carbon\Carbon;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Log;

class CreateResult
{
    public function execute(string $draw_id)
    {
        Log::channel('dev')->info("Creating Result for {$draw_id}");

        $result_exist = Result::where('date', Carbon::parse($draw_id))->first();

        $message = '';
        if ($result_exist) {
            $message = 'Result already exit for this draw id...';
            Log::channel('dev')->info($message);
            return $message;
        }

        $response = (new LotteryService())->getResults($draw_id);

        if (!$response['success']) {
            $message = 'Error >>> ' . json_encode($response);
            Log::channel('dev')->error('Response Error...');
            Log::channel('dev')->info($message);

            return $message;
        }

        if ($response['success']) {
            Log::channel('dev')->info('Response Ok...');
            $results = $response['data'];

            if (!$results) {
                $message = "Message >>> There is no result with this draw id {$draw_id}";
                Log::channel('dev')->info($message);

                return $message;
            }

            foreach ($results as $result_type_name => $result) {
                $result_type = ResultType::whereName($result_type_name)->first();

                //store lottery results
                if ($result_type) {
                    $result_type->results()->create([
                        'result' => $result,
                        'amount' => $result_type->amount,
                        'date' => $draw_id,
                    ]);
                }
            }

            Artisan::call('result:calculate', [
                'draw_id' => $draw_id
            ]);
        }

        return 'success';
    }
}
