<?php

namespace App\Actions;

use Carbon\Carbon;
// unused
class GenerateLastDraw
{
    private $draw_day;
    private $draw_month;
    private $draw_year;

    public function execute($date = null): string
    {
        $now = $date ? Carbon::parse($date) :   now();
        $current_year = $now->year;
        $current_month = $now->month;
        $current_day = $now->day;

        $this->draw_day = 1;
        $this->draw_month = $current_month;
        $this->draw_year = $current_year;

        if ($current_day >= 16) {
            $this->draw_day = 16;
        }

        if ($current_month === 1 && $current_day < 17) {
            $this->draw_day = 30;
            $this->draw_month = 12;
            $this->draw_year = $current_year - 1;
            return $this->getDrawDate();
        }

        if ($current_month === 1 && $current_day >= 17) {
            $this->draw_day = 17;
            return $this->getDrawDate();
        }

        if ($current_month === 5 && $current_day < 16) {
            $this->draw_day = 2;
            return $this->getDrawDate();
        }

        if ($current_month === 12 && $current_day >= 30) {
            $this->draw_day = 30;
            return $this->getDrawDate();
        }

        return $this->getDrawDate();
    }

    private function getDrawDate()
    {
        return date('Y-m-d', strtotime($this->draw_year . "/" . $this->draw_month . "/" . $this->draw_day));
    }
}
