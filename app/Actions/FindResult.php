<?php

namespace App\Actions;

use App\Models\Result;
use App\Models\ResultType;

class FindResult
{
    private Result $result;
    public function __construct(Result $result)
    {
        $this->result = $result;
    }

    public function execute(array $ticket_numbers)
    {
        $won_tickets = [];
        foreach ($ticket_numbers as $ticket_number) {
            if ($this->result->result_type->name === 'first_three') {
                $first_three = substr($ticket_number, 0, 3);
                if (in_array($first_three, $this->result->result)) {
                    array_push($won_tickets, $ticket_number);
                }
                continue;
            }

            if ($this->result->result_type->name === 'last_three') {
                $last_three = substr($ticket_number, -3);
                if (in_array($last_three, $this->result->result)) {
                    array_push($won_tickets, $ticket_number);
                }
                continue;
            }

            if ($this->result->result_type->name === 'last_two') {
                $last_two = substr($ticket_number, -2);
                if (in_array($last_two, $this->result->result)) {
                    array_push($won_tickets, $ticket_number);
                }
                continue;
            }


            if (in_array($this->result->result_type->name, ['first', 'second', 'third', 'forth', 'fifth', 'closest_first'])) {
                if (in_array($ticket_number, $this->result->result)) {
                    array_push($won_tickets, $ticket_number);
                }
                continue;
            }
        }
        return $won_tickets;
    }
}
