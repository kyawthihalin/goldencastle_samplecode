<?php

namespace App\Actions;

use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;

class ValidateDrawId
{
    public function execute(string $draw_id): string
    {
        $validator = Validator::make([
            'draw_id' => $draw_id
        ], [
            'draw_id' => ['required', 'date']
        ]);

        if ($validator->fails()) {
            return $validator->errors()->first();
        }

        return "";
    }
}
