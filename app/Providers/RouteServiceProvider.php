<?php

namespace App\Providers;

use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * The path to the "home" route for your application.
     *
     * This is used by Laravel authentication to redirect users after login.
     *
     * @var string
     */
    public const HOME = '/home';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        $this->configureRateLimiting();

        $this->routes(function () {
            Route::middleware('web')
                ->group(base_path('routes/web.php'));

            Route::prefix('api')
                ->middleware(['api', 'decrypted', 'locale'])
                ->prefix('api/v1')
                ->group(base_path('routes/api/index.php'));

            if (!app()->environment('production')) {
                Route::middleware('api')
                    ->prefix('dev')
                    ->group(base_path('routes/api/dev.php'));
            }

            Route::prefix('api')
                ->middleware(['api', 'decrypted', 'locale', 'auth:user'])
                ->prefix('api/v1/user')
                ->group(base_path('routes/api/user.php'));

            Route::prefix('api')
                ->middleware(['api', 'decrypted', 'locale', 'auth:agent'])
                ->prefix('api/v1/agent')
                ->group(base_path('routes/api/agent.php'));
        });
    }

    /**
     * Configure the rate limiters for the application.
     *
     * @return void
     */
    protected function configureRateLimiting()
    {
        RateLimiter::for('api', function (Request $request) {
            return Limit::perMinute(60);
        });
    }
}
