<?php

namespace App\Providers;

use App\Interfaces\LotteryApiInterface;
use App\Services\LotteryApi\LottoService;
use App\Services\LotteryApi\RayRiffyService;
use Illuminate\Support\ServiceProvider;

class LotteryApiServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        // Check if the first API is available
        // if ((new LottoService())->health()) {
        //     $this->app->bind(LotteryApiInterface::class, LottoService::class);
        // } else if ((new RayRiffyService())->health()) {
        //     $this->app->bind(LotteryApiInterface::class, RayRiffyService::class);
        // }
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
