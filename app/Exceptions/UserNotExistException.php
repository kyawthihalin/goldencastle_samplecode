<?php

namespace App\Exceptions;

use App\Traits\ApiResponse;
use Exception;

class UserNotExistException extends Exception
{
    use ApiResponse;
    /**
     * Render the exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function render($request)
    {
        return $this->responseWithError($this->message, [], 401);
    }
}
