<?php

namespace App\Exceptions;

use App\Traits\ApiResponse;
use Exception;

class UnprocessableException extends Exception
{
    use ApiResponse;
    public array $errors;
    public function __construct(array $errors)
    {
        $this->errors = $errors;
    }

    /**
     * Render the exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function render($request)
    {
        if (config('app.dev_validation')) {
            return $this->responseWithError(
                'Error',
                $this->errors,
                422
            );
        }

        return $this->responseWithError(
            'Missing required parameters.',
            [],
            422
        );
    }
}
