<?php

namespace App\Exceptions;

use App\Traits\ApiResponse;
use Exception;

class RechargeAlreadyExistException extends Exception
{
    use ApiResponse;
    /**
     * Render the exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function render($request)
    {
        return $this->responseWithError(__('recharge.failed.already_exist'), [], 422);
    }
}
