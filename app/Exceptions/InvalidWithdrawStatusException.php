<?php

namespace App\Exceptions;

use App\Traits\ApiResponse;
use Exception;

class InvalidWithdrawStatusException extends Exception
{
    use ApiResponse;
    private string $status;
    public function __construct(string $status)
    {
        $this->status = $status;
    }

    public function render()
    {
        return $this->responseWithError("Cannot process. Withdraw request is in {$this->status} status.", [], 400);
    }
}
