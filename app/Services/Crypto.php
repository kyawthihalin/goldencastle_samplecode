<?php

namespace App\Services;

use Illuminate\Encryption\Encrypter;
use Illuminate\Support\Facades\Crypt;

class Crypto
{
    public function generateKey($user)
    {
        if (auth()->check()) {
            return;
        }
        $encryption_key = base64_encode(random_bytes(32));

        $user->secret_key = $encryption_key;
        $user->save();

        return $encryption_key;
    }

    public function encrypt($data)
    {
        if (!config('app.crypto')) {
            return $data;
        }

        if (auth()->check()) {
            $key = base64_decode(auth()->user()->secret_key);
            $newEncrypter = new Encrypter($key, config('app.cipher'));
            return $newEncrypter->encryptString(json_encode($data));
        }
        return Crypt::encryptString(json_encode($data));
    }

    public function decrypt(string $data)
    {
        if (!config('app.crypto')) {
            return $data;
        }
        
        $decrypted = null;
        if (auth()->check()) {
            $key = base64_decode(auth()->user()->secret_key);
            $newEncrypter = new Encrypter($key, config('app.cipher'));

            $decrypted = $newEncrypter->decryptString($data);
        } else {
            $decrypted = Crypt::decryptString($data);
        }

        if ($this->isJson($decrypted)) {
            return json_decode($decrypted, true);
        }

        return $decrypted;
    }

    private function isJson($str)
    {
        $json = json_decode($str);
        return $json && $str != $json;
    }
}
