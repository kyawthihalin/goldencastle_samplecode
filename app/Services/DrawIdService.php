<?php

namespace App\Services;

use App\Models\LotteryDate;
use Carbon\Carbon;

class DrawIdService
{
  public function getCurrentDrawId($date = null)
  {
    $currentDate = $date ? Carbon::parse($date) : now();
    $monthDay = $currentDate->format('m-d');

    $lottery_dates = LotteryDate::all()->pluck('date')->toArray();

    foreach ($lottery_dates as $drawId) {
      if ($monthDay <= $drawId) {
        return $currentDate->year . '-' . $drawId;
      }
    }

    // If the current date is after the last draw, return the first draw of the next year
    return  $currentDate->year . '-01-17';
  }

  public function getPreviousDrawId($date = null)
  {
    $currentDate = $date ? Carbon::parse($date) : now();
    $monthDay = $currentDate->format('m-d');
    $lottery_dates = LotteryDate::all()->pluck('date')->toArray();

    foreach (array_reverse($lottery_dates) as $drawId) {
      if ($monthDay >= $drawId) {
        return $currentDate->year . '-' . $drawId;
      }
    }

    // If the current date is before the first draw, return the last draw of the previous year
    return $currentDate->subYear()->year . '-12-16';
  }
}
