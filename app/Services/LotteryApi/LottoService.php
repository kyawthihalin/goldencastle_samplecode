<?php

namespace App\Services\LotteryApi;

use Illuminate\Support\Facades\Http;
use App\Interfaces\LotteryApiInterface;
use App\Services\Telegram;
use Exception;
use Illuminate\Http\Client\ConnectionException;

class LottoService implements LotteryApiInterface
{
  public function health(): bool
  {
    try {
      $response = Http::withHeaders([
        'api-key' => 'my-api-key'
      ])->get('https://lotto.ter.app/api/latest-results');
    } catch (Exception $e) {
      // $this->warning("https://lotto.ter.app/api/latest-results health check failed {$e->getMessage()}");
      return false;
    }

    if ($response->failed() || $response->status() === 404) {
      return false;
    }

    return false;
  }

  public function getResults(string $draw_id)
  {
    $response = Http::withHeaders([
      'api-key' => 'my-api-key'
    ])->get('https://lotto.ter.app/api/draw-results?drawId=' . $draw_id);

    if (!$response->ok()) {
      $this->warning("Getting lottery results from https://lotto.ter.app/api/draw-results?drawId={$draw_id} unsuccess.");
      return $this->formatResponse(null);
    }

    return $this->formatResponse($response->json());
  }

  private function formatResponse($data)
  {
    if (!$data) {
      return [
        'success' => false,
      ];
    }

    return [
      'success' => true,
      'data' => $data['data']
    ];
  }

  private function warning($message)
  {
    (new Telegram(config('services.telegram.chat_ids.developers')))->sendMessage($message);
  }
}
