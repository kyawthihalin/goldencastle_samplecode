<?php

namespace App\Services\LotteryApi;

use App\Interfaces\LotteryApiInterface;
use App\Services\Telegram;
use Carbon\Carbon;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Http;

class RayRiffyService implements LotteryApiInterface
{
  private $key_map = [
    'prizeFirst' => 'first',
    'prizeFirstNear' => 'closest_first',
    'prizeSecond' => 'second',
    'prizeThird' => 'third',
    'prizeForth' => 'forth',
    'prizeFifth' => 'fifth',
    'runningNumberFrontThree' => 'first_three',
    'runningNumberBackThree' => 'last_three',
    'runningNumberBackTwo' => 'last_two',
  ];

  public function health(): bool
  {
    try {
      $client = new Client();
      $client->get('https://lotto.api.rayriffy.com/latest');
    } catch (Exception $e) {
      $this->warning("https://lotto.api.rayriffy.com/latest health check failed {$e->getMessage()}");
      return false;
    }
    return true;
  }

  public function getResults(string $draw_id)
  {
    $date = Carbon::createFromFormat('Y-m-d', $draw_id);

    // Convert the year to Thailand year
    $thailandYear = $date->year + 543;
    $date->year($thailandYear);

    // Format the date in the Thailand date format
    $thailandDate = $date->format('dmY');

    $response = Http::withHeaders([
      'Accept' => 'application/json'
    ])->get("https://lotto.api.rayriffy.com/lotto/{$thailandDate}");

    if (!$response->ok()) {
      $this->warning("Getting lottery results from https://lotto.api.rayriffy.com/lotto/{$thailandDate} unsuccess.");
      return $this->formatResponse(null);
    }

    return $this->formatResponse($response->json());
  }

  private function formatResponse($data)
  {
    if (!$data) {
      return [
        'success' => false,
      ];
    }

    try {
      $results = array_merge($data['response']['prizes'], $data['response']['runningNumbers']);
      $finals = [];

      foreach ($results as $result) {
        $finals[$this->key_map[$result['id']]] = $result['number'];
      }
    } catch (Exception $e) {
      $this->warning("Formatting results from Ray Reffy Service failed, {$e->getMessage()}");
      return false;
    }

    return [
      'success' => true,
      'data' => $finals
    ];
  }

  private function warning($message)
  {
    (new Telegram(config('services.telegram.chat_ids.developers')))->sendMessage($message);
  }
}
