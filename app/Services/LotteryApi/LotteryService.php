<?php

namespace App\Services\LotteryApi;

use App\Interfaces\LotteryApiInterface;
use App\Services\Telegram;
use Carbon\Carbon;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\Http;

class LotteryService implements LotteryApiInterface
{
  private $key_map = [
    'รางวัลที่1' => 'first',
    'รางวัลข้างเคียงรางวัลที่1' => 'closest_first',
    'รางวัลที่2' => 'second',
    'รางวัลที่3' => 'third',
    'รางวัลที่4' => 'forth',
    'รางวัลที่5' => 'fifth',
    'เลขหน้า3ตัว' => 'first_three',
    'เลขท้าย3ตัว' => 'last_three',
    'เลขท้าย2ตัว' => 'last_two',
  ];

  public function health(): bool
  {
    try {
      $client = new Client();
      $client->request('GET', 'https://thai-lottery1.p.rapidapi.com/reto', [
        'headers' => [
          'X-RapidAPI-Host' => 'thai-lottery1.p.rapidapi.com',
          'X-RapidAPI-Key' => config('services.lottery.api_key'),
        ],
      ]);
    } catch (Exception $e) {
      $this->warning("https://thai-lottery1.p.rapidapi.com/reto health check failed {$e->getMessage()}");
      return false;
    }
    return true;
  }

  public function getResults(string $draw_id)
  {
    $date = Carbon::createFromFormat('Y-m-d', $draw_id);

    // Convert the year to Thailand year
    $thailandYear = $date->year + 543;
    $date->year($thailandYear);

    // Format the date in the Thailand date format
    $thailandDate = $date->format('dmY');

    $request = new Request('GET', "https://thai-lottery1.p.rapidapi.com/?date={$thailandDate}", [
      'X-RapidAPI-Host' => 'thai-lottery1.p.rapidapi.com',
      'X-RapidAPI-Key' => config('services.lottery.api_key'),
    ]);
    $res = (new Client())->sendAsync($request)->wait();

    if ($res->getStatusCode() != 200) {
      $this->warning("Getting lottery results from https://lotto.api.rayriffy.com/lotto/{$thailandDate} unsuccess.");
      return $this->formatResponse(null);
    }

    $bodystream = $res->getBody();

    return $this->formatResponse(json_decode($bodystream->getContents()));
  }

  private function formatResponse($data)
  {
    if (!$data) {
      return [
        'success' => false,
      ];
    }

    try {
      $finals = [];
      foreach ($data as $result) {
        $type = $this->key_map[$result[0]];
        array_shift($result);

        if(!$result[0]){
          $finals = [];
          break;
        }

        $finals[$type] = $result;
      }
    } catch (Exception $e) {
      $this->warning("Formatting results from Ray Reffy Service failed, {$e->getMessage()}");
      return false;
    }

    return [
      'success' => true,
      'data' => $finals
    ];
  }

  private function warning($message)
  {
    (new Telegram(config('services.telegram.chat_ids.developers')))->sendMessage($message);
  }
}
