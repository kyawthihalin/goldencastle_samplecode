<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;

class Telegram
{
    private $chat_id;
    protected $url = '';
    public function __construct($chat_id)
    {
        $bot_token = config('services.telegram.bot_token');

        $this->url = "https://api.telegram.org/bot{$bot_token}/";
        $this->chat_id = $chat_id;
    }

    public function sendMessage(string $text, $url = null)
    {
        if (!$this->chat_id) {
            return;
        }

        if (request()->secure() && $url) {
            Http::post($this->url . 'sendPhoto', [
                'chat_id' => $this->chat_id,
                'caption' => $text,
                'photo' =>  $url,
            ]);

            return;
        }

        Http::post($this->url . 'sendMessage', [
            'chat_id' => $this->chat_id,
            'text' => $text
        ]);
    }
}
