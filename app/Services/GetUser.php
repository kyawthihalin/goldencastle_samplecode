<?php

namespace App\Services;
use App\Models\User;
use App\Models\Agent;

class GetUser
{
   
    public function getUser(): array
    {
        $users = User::all();
        $user_arr = [];
        foreach ($users as $user) {
            $user_arr[$user->id] = "$user->name($user->reference_id)";
        }
        return $user_arr;
    }


    public function getAgent(): array
    {
        $agents = Agent::all();
        $agent_arr = [];
        foreach ($agents as $agent) {
            $agent_arr[$agent->id] = "$agent->name($agent->code)";
        }
        return $agent_arr;
    }



}
