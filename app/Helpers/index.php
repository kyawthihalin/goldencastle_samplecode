<?php

use App\Models\AppType;
use App\Models\UserTicket;
use Illuminate\Support\Facades\DB;

/**
 * merge array with required
 */
if (!function_exists('get_app_types')) {
    function get_app_types(): array
    {
        $app_types = AppType::get();
        $apps = [];
        foreach ($app_types as $value) {
            $apps[$value->id] = $value->name;
        }
        return $apps;
    }
}
if (!function_exists('get_color_code')) {
    function get_color_code($num)
    {
        $hash = md5('color' . $num);
        return array(hexdec(substr($hash, 0, 2)), hexdec(substr($hash, 2, 2)), hexdec(substr($hash, 4, 2)));
    }
}

if (!function_exists('get_random_color_set')) {
    function get_random_color_set($num)
    {

        $random_colors = [
            get_color_code($num),
            get_color_code($num),
            get_color_code($num),
            get_color_code($num),
            get_color_code($num),
            get_color_code($num),
            get_color_code($num),
            get_color_code($num),
            get_color_code($num),
            get_color_code($num),
        ];
        $color = $random_colors[array_rand($random_colors, 1)];
        return $color;
    }
}

if (!function_exists('get_draw_id')) {
    function get_draw_id()
    {
        return DB::table('results')
            ->orderBy('date', 'desc')
            ->select('date')->distinct()
            ->pluck('date', 'date')->toArray();
    }
}
