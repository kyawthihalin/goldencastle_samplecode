<?php

namespace App\Observers;

use App\Models\AppVersion;
use Illuminate\Support\Facades\Auth;

class AppVersionObserver
{

    /**
     * Handle the AppVersion "created" event.
     *
     * @param  \App\Models\AppVersion  $appVersion
     * @return void
     */
    public function created(AppVersion $appVersion)
    {
        $appVersion->admin_id = Auth()->user()->id;
        $appVersion->save();
    }

    /**
     * Handle the AppVersion "updated" event.
     *
     * @param  \App\Models\AppVersion  $appVersion
     * @return void
     */
    public function updated(AppVersion $appVersion)
    {
        //
    }

    /**
     * Handle the AppVersion "deleted" event.
     *
     * @param  \App\Models\AppVersion  $appVersion
     * @return void
     */
    public function deleted(AppVersion $appVersion)
    {
        //
    }

    /**
     * Handle the AppVersion "restored" event.
     *
     * @param  \App\Models\AppVersion  $appVersion
     * @return void
     */
    public function restored(AppVersion $appVersion)
    {
        //
    }

    /**
     * Handle the AppVersion "force deleted" event.
     *
     * @param  \App\Models\AppVersion  $appVersion
     * @return void
     */
    public function forceDeleted(AppVersion $appVersion)
    {
        //
    }
}
