<?php

namespace App\Interfaces;

interface LotteryApiInterface
{
  public function health(): bool;
  /**
   * Get the instance as an array.
   *
   * @return array
   */
  public function getResults(string $draw_id);
}
