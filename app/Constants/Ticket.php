<?php

namespace App\Constants;

class Ticket
{
    const RESUT_COUNT = [
        'first' => 1,
        'second' => 5,
        'third' => 10,
        'forth' => 50,
        'fifth' => 100,
        'closest_first' => 2,
        'first_three' => 2,
        'last_three' => 2,
        'last_two' => 1,
    ];
}
