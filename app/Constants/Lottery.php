<?php

namespace App\Constants;

class Lottery
{
    const CALL_CENTER = '+95977137832';
    const SEEDER_DRAW_ID = [
        '2022-12-30',
        '2022-12-16',
        '2022-12-01',
        '2022-11-16',
        '2022-11-01',
        '2022-10-16',
        '2022-10-01',
        '2022-09-16',
        '2022-09-01',
    ];
}
