<?php

namespace App\Http\Resources\Api\Agent;

use App\Models\User;
use App\Models\UserTicket;
use App\Services\DrawIdService;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\LiveOpening;
class AgentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $users = User::whereAgentId($this->id)->pluck('id')->toArray();
        $draw_id = (new DrawIdService())->getCurrentDrawId();
        $ticket_count = UserTicket::whereIn('user_id',$users)->where('draw_id', $draw_id)->get()->count();
        $new_noti_count = $this->user_notifications()->whereNull('read_at')->count();

        return [
            'state' => $this->state->name_en,
            'name' => $this->name,
            'language' => $this->language,
            'code' => $this->code,
            'phone_number' => $this->phone_number,
            'point' => $this->point,
            'nrc' => $this->nrc,
            'ticket_count' => $ticket_count,
            'unread_noti_count' => $new_noti_count > 9 ? '9+' : $new_noti_count,
            'live_opening' => LiveOpening::first()->link,
        ];
    }
}
