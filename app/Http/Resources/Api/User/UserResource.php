<?php

namespace App\Http\Resources\Api\User;

use App\Models\LiveOpening;
use App\Services\DrawIdService;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $current_draw_id = (new DrawIdService())->getCurrentDrawId();
        $current_ticket_count = $this->tickets()->where('draw_id', $current_draw_id)->count();
        $new_noti_count = $this->user_notifications()->whereNull('read_at')->count();

        return [
            'name' => $this->name,
            'point' => $this->point,
            'user_name' => $this->user_name,
            'phone_number' => $this->phone_number,
            'nrc' => $this->nrc,
            'language' => $this->language,
            'agent_code' => $this->agent->code,
            'agent_phone' => $this->agent->phone_number,
            'current_ticket_count' => $current_ticket_count,
            'unread_noti_count' => $new_noti_count > 9 ? '9+' : $new_noti_count,
            'live_opening' => LiveOpening::first()->link,
        ];
    }
}
