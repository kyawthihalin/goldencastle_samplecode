<?php

namespace App\Http\Resources\Api\User;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class RechargeRequestResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $point = "{$this->confirmed_amount}";

        return [
            'icon' =>  url('/') . '/' . str_replace('\\', '/', $this->account->account_photo),
            'account_type' => $this->account->name,
            'transaction_id' => $this->reference_id,
            'point' => $point,
            'status' => $this->status,
            'screenshot' => url('/') . '/' . Storage::url("Recharge/Requests/$this->id/$this->screenshot"),
            'date' => $this->created_at->format('Y-m-d H:i:s'),
        ];
    }
}
