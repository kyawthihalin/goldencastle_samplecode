<?php

namespace App\Http\Resources\Api\User\Ticket;

use Illuminate\Http\Resources\Json\JsonResource;

class PurchasedTicketResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'number' => $this->number,
            'amount' => $this->amount,
            'purchased_date' => $this->purchased_date,
        ];
    }
}
