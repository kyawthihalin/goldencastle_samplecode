<?php

namespace App\Http\Resources\Api\User;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class WithdrawRequestResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $withdraw_option = $this->withdraw_option;

        $point = 0;
        $date = null;

        if ($this->status === 'Completed') {
            $point = "- {$this->point}";
            $date = Carbon::parse($this->completed_at)->format('Y-m-d H:i:s');
        }
        if ($this->status === 'Requested') {
            $point = "- {$this->point}";
            $date = $this->created_at->format('Y-m-d H:i:s');
        }
        
        if($this->status === 'Refunded'){
            $point = "+ {$this->point}";
            $date = Carbon::parse($this->completed_at)->format('Y-m-d H:i:s');
        }

        $screenshot = null;

        if ($this->screenshot) {
            $screenshot = url('/') . '/' . 'storage/Withdraw/Requests/' . $this->id . '/' .
                $this->screenshot;
        }

        $qr_code = null;
        if ($this->qr_code) {
            $qr_code = url('/') . '/' . 'storage/Withdraw/Requests/' . $this->id . '/' .
                $this->qr_code;
        }

        return [
            'icon' =>  url('/') . '/' . str_replace('\\', '/', $withdraw_option->icon),
            'option' => $withdraw_option->name,
            'transaction_id' => $this->reference_id,
            'screenshot' => $screenshot,
            'qr_code' => $qr_code,
            'point' => $point,
            'status' => $this->status,
            'screenshot' => $screenshot,
            'date' => $date,
        ];
    }
}
