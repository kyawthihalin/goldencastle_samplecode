<?php

namespace App\Http\Resources\Api\User;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class AccountResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {  
        return [
            'id' => $this->id,
            'icon' => url('/') . '/' . str_replace('\\', '/', $this->account_photo),
            'name' => $this->name,
            'account_name' => "{$this->account_name} ({$this->phone_number})",
            'qr_code' => url('/') . '/' . str_replace('\\', '/', $this->qr_code),
            'phone_number' => $this->phone_number
        ];
    }
}
