<?php

namespace App\Http\Resources\Api\User;

use App\Models\Notification;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\DB;

class NotificationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $data = json_decode($this->data);

        return [
            'id' => $this->id,
            // 'type' => $this->type,
            'title' => $data->title,
            'message' => $data->message,
            'read' => $this->read_at ? true : false,
            'time' => $this->created_at->format('g:i A')
        ];
    }
}
