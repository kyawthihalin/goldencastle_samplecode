<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BannerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (request()->has('images')) {
            $images = json_decode(request()->all()['images']);

            if (!is_array($images)) {
                throw \Illuminate\Validation\ValidationException::withMessages([
                    'images' => ['Banner should have at least 1 image.'],
                ]);
            }

            if (count($images) < 1) {
                throw \Illuminate\Validation\ValidationException::withMessages([
                    'images' => ['Banner should have at least 1 image.'],
                ]);
            }

            if (count($images) > 10) {
                throw \Illuminate\Validation\ValidationException::withMessages([
                    'images' => ['Banner should have only 10 images.'],
                ]);
            }
        }

        return [
            'images' => ['required', 'string']
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            //
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            //
        ];
    }
}
