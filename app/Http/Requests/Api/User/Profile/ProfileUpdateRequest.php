<?php

namespace App\Http\Requests\Api\User\Profile;

use Illuminate\Foundation\Http\FormRequest;

class ProfileUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string'],
            'phone_number' => ['string', 'max:20', 'min:5'],
            'nrc' => ['string', 'min:17', 'max:30'],
        ];
    }

    public function messages()
    {
        return [
            'phone_number.min' => 'Phone number must have at least 5 characters in length.',
            'phone_number.max' => 'Phone number can only contain maximum 20 characters.',
            'nrc.min' => 'Nrc must have at least 17 characters in length.',
            'nrc.max' => 'Nrc can only contain maximum 20 characters.',
        ];
    }
}
