<?php

namespace App\Http\Requests\Api\User\Auth;

use App\Actions\HiddenValidation;
use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        (new HiddenValidation())->handle([
            'device_name' => ['required', 'string', 'max:255'],
            'device_model' => ['required', 'string', 'max:255'],
            'os_version' => ['required', 'string', 'max:255'],
            'os_type' => ['required', 'string', 'max:255'],
            'app_version_id' => ['required', 'string', 'max:255'],
            'noti_token' => ['required', 'string', 'max:255'],
            'language' => ['required', 'string', 'in:en,mm,th'],
        ]);

        return [
            'user_name' => ['required', 'string', 'max:255', 'unique:users', 'alpha_dash'],
            'name' => ['required', 'string'],
            'password' => [
                'required', 
                'confirmed', 
                'string', 
                'min:6',             // must be at least 10 characters in length
                'regex:/[a-z]/',      // must contain at least one lowercase letter
                'regex:/[A-Z]/',      // must contain at least one uppercase letter
                'regex:/[0-9]/',      // must contain at least one digit
                // 'regex:/[@$!%*#?&]/', // must contain a special character
            ],
            'agent_code' => ['required', 'string'],
        ];
    }

    public function messages()
    {
        return [
            'user_name.alpha_dash' => trans('validation.user_name.alpha_dash'),
            'password.min' => trans('validation.password.regex'),
            'password.regex' => trans('validation.password.regex'),
        ];
    }
}
