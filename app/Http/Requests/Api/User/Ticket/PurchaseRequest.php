<?php

namespace App\Http\Requests\Api\User\Ticket;

use App\Actions\HiddenValidation;
use Illuminate\Foundation\Http\FormRequest;

class PurchaseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ticket_numbers' => ['required', 'array', 'min:1'],
            'ticket_numbers.*' => ['digits:6']
        ];
    }
}
