<?php

namespace App\Http\Requests\Api\User\Ticket;

use Illuminate\Foundation\Http\FormRequest;

class ResultCheckRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ticket_number' => ['digits:6', 'prohibits:ticket_numbers,from_ticket'],
            'ticket_numbers' => ['array', 'prohibits:ticket_number,from_ticket'],
            'ticket_numbers.*' => ['digits:6'],
            'from_ticket' => ['digits:6', 'prohibits:ticket_number,ticket_numbers', 'required_with:to_ticket'],
            'to_ticket' => ['digits:6', 'required_with:from_ticket'],
            'draw_id' => ['date', 'date_format:Y-m-d', 'before:tomorrow']
        ];
    }
}
