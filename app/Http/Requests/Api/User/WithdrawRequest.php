<?php

namespace App\Http\Requests\Api\User;

use App\Actions\HiddenValidation;
use App\Models\WithdrawOption;
use Illuminate\Foundation\Http\FormRequest;

class WithdrawRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        (new HiddenValidation())->handle([
            'password' => ['required', 'string'],
            'point' => ['required'],
            'payee' => ['required'],
            'account_number' => ['required'],
            'option' => ['required', 'string', 'in:' . implode(',', WithdrawOption::pluck('name')->toArray())]
        ]);

        return [
            'point' => ['integer', 'min:100'],
            'payee' => ['string', 'max:255'],
            'account_number' => ['required', 'string', 'max:255'],
            'qr_code' => ['image', 'mimes:png,jpg,jpeg,jfif'],
        ];
    }

    public function messages()
    {
        return [
            'point.min' => __('withdraw.validation.point.min',["point"=>100]),
            'point.integer' => __('withdraw.validation.point.integer'),
        ];
    }
}
