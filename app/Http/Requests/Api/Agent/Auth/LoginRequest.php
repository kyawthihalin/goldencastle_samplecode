<?php

namespace App\Http\Requests\Api\Agent\Auth;

use App\Actions\HiddenValidation;
use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        (new HiddenValidation())->handle([
            'device_name' => ['required', 'string'],
            'device_model' => ['required', 'string'],
            'os_version' => ['required', 'string'],
            'os_type' => ['required', 'string'],
            'app_version_id' => ['required', 'string'],
            'noti_token' => ['required', 'string'],
            'language' => ['required', 'string', 'in:en,mm,th'],
        ]);

        return [
            'code' => ['required', 'string'],
            'password' => ['required', 'string'],
        ];
    }
}
