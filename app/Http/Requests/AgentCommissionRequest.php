<?php

namespace App\Http\Requests;

use App\Models\TicketPrice;
use Illuminate\Foundation\Http\FormRequest;

class AgentCommissionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $ticket_price = TicketPrice::first();
        return [
            'point' => 'required|integer|min:1|max:' . $ticket_price->price,
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            //
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'point.max' => 'Agent commission amount must be below ticket price.',
        ];
    }
}
