<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AgentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:5|max:255',
            'password' => [
                'required',
                'confirmed',
                'string',
                'min:6',             // must be at least 10 characters in length
                'regex:/[a-z]/',      // must contain at least one lowercase letter
                'regex:/[A-Z]/',      // must contain at least one uppercase letter
                'regex:/[0-9]/',      // must contain at least one digit
            ],
            'password_confirmation' => 'required',
            'state_id' => 'required',
            'nrc' => [
                'required',
                'string',
                'min:17',
                'max:20',
                // 'regex:/([1-9]|1[0-5])\/[A-Z]{6,9}\(([N|E|P|T|C|AC|NC|V|M])\)[0-9]{6}/'
            ],
            'phone_number' => 'required|string',
            'nrc_front' => 'required|string',
            'nrc_back' => 'required|string',
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            //
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'password.regex' => 'Password must be 6 characters in length, contains at least one lowercase letter, one uppercase letter and one degit.'
        ];
    }
}
