<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AgentUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:5|max:255',
            'state_id' => 'required',
            'nrc' => [
                'required',
                'string',
                'min:17',
                'max:20',
                // 'regex:/([1-9]|1[0-5])\/[A-Z]{6,9}\(([N|E|P|T|C|AC|NC|V|M])\)[0-9]{6}/'
            ],
            'phone_number' => 'required|string',
            'nrc_front' => 'required|string',
            'nrc_back' => 'required|string',
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            //
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            //
        ];
    }
}
