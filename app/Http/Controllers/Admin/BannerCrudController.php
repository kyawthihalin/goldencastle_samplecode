<?php

namespace App\Http\Controllers\Admin;

use App\Actions\SetCrudPermission;
use App\Http\Requests\BannerRequest;
use App\Models\Banner;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class BannerCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class BannerCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Banner::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/banner');
        CRUD::setEntityNameStrings('banner', 'banners');
        CRUD::denyAccess(['create', 'show', 'update', 'delete']);
        (new SetCrudPermission())->execute('banner', $this->crud);
    }

    public function showDetailsRow($id)
    {
        $banner = Banner::find($id);
        return view('partials.bannerDetail', ['banner' => $banner]);
    }
    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::addColumn([   // Browse
            'name'  => 'images',
            'label' => 'images',
            'type'  => 'closure',
            'function' => function ($entry) {
                $paths = json_decode($entry->images);
                $images = '';
                foreach ($paths as $path) {
                    $modified_path = str_replace('\\', '/', $path);
                    $url = url('/') . '/' . $modified_path;
                    $images .= "<a target='blank' href='{$url}' ><img src='{$url}' alt='banner' width='50' height='50'/></a> ";
                }
                return $images;
            }
        ],);
        CRUD::column('type');
        CRUD::column('updated_at');

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(BannerRequest::class);
        CRUD::addField(
            [
                'name'          => 'images',
                'label'         => 'Files',
                'type'          => 'browse_multiple',
                'multiple'   => true, // enable/disable the multiple selection functionality
                'sortable'   => true, // enable/disable the reordering with drag&drop
            ],
        );
        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number'])); 
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
