<?php

namespace App\Http\Controllers\Admin;

use App\Actions\SetCrudPermission;
use App\Http\Requests\TicketResultRequest;
use App\Models\ResultType;
use App\Models\User;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

/**
 * Class TicketResultCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class TicketResultCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\TicketResult::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/ticket-result');
        CRUD::setEntityNameStrings('winner ticket', 'winner tickets');
        (new SetCrudPermission())->execute('ticket_price', $this->crud);
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->enableExportButtons();

        $this->crud->orderBy('draw_id', 'desc');
        $this->crud->addFilter(
            [
                'name'  => 'user',
                'type'  => 'select2_ajax',
                'label' => 'Winner'
            ],
            url('admin/options/user'),
            function ($value) { // if the filter is active
                $this->crud->addClause('whereHas', 'user_ticket', function ($query) use ($value) {
                    $query->where('user_id', $value);
                });
            }
        );

        $this->crud->addFilter(
            [
                'name'  => 'agent',
                'type'  => 'select2_ajax',
                'label' => 'Agent'
            ],
            url('admin/options/agent'),
            function ($value) { // if the filter is active
                $this->crud->addClause('whereHas', 'user_ticket', function ($query) use ($value) {
                    $query->whereHas('user', function($q) use($value){
                        $q->where('agent_id', $value);
                    });
                });
            }
        );

        $this->crud->addFilter([
            'name'  => 'app_type',
            'type'  => 'select2_multiple',
            'label' => 'Result Type'
        ], function () {
            return ResultType::all()->pluck('name', 'id')->toArray();
        }, function ($values) { // if the filter is active
            $this->crud->addClause('whereHas', 'result_type', function ($query) use ($values) {
                $query->whereIn('id', json_decode($values));
            });
        });

        $this->crud->addFilter([
            'name'  => 'draw_id',
            'type'  => 'select2',
            'label' => 'Draw Date'
        ], function () {
            return DB::table('results')
                ->orderBy('date', 'desc')
                ->select('date')->distinct()->limit(6)
                ->pluck('date', 'date')->toArray();
        }, function ($value) { // if the filter is active
            $this->crud->addClause('where', 'draw_id', Carbon::parse($value));
        });

        $this->crud->addFilter([
            'name'  => 'number',
            'type'  => 'text',
            'label' => 'Ticket Number'
        ], false, function ($value) { // if the filter is active
            $this->crud->addClause('whereHas', 'user_ticket', function ($query) use ($value) {
                $query->where('number', json_decode($value));
            });
        });

        CRUD::addColumn([
            'name' => 'ticket_number',
            'type' => 'closure',
            'label' => 'Ticket Number',
            'function' => function ($entry) {
                $user_ticket = $entry->user_ticket;
                return $user_ticket->number;
            }
        ]);

        CRUD::addColumn([
            'name' => 'user',
            'type' => 'closure',
            'label' => 'Winner',
            'function' => function ($entry) {
                $user = $entry->user_ticket->user;
                return $user->name;
            }
        ]);

        CRUD::addColumn([
            'name' => 'agent',
            'type' => 'closure',
            'label' => 'Agent',
            'function' => function ($entry) {
                $agent = $entry->user_ticket->user->agent;
                return $agent->name . " ($agent->code)";
            }
        ]);

        CRUD::addColumn([
            'name'     => 'result_type',
            'label'    => 'Result Type',
            'type'     => 'closure',
            'function' => function ($entry) {
                return ucfirst(str_replace('_', ' ', $entry->result_type->name));
            }
        ]);

        CRUD::addColumn([
            'name' => 'amount',
            'type' => 'text',
            'label' => 'Price',
        ]);

        CRUD::addColumn([
            'name' => 'draw_id',
            'label' => 'Draw Date',
            'type'     => 'closure',
            'function' => function ($entry) {
                return $entry->draw_id->format('Y-m-d');
            }
        ]);

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupShowOperation()
    {
        CRUD::addColumn([
            'name' => 'user',
            'type' => 'closure',
            'label' => 'User',
            'function' => function ($entry) {
                $user = $entry->user_ticket->user;
                return $user->name;
            }
        ]);

        CRUD::addColumn([
            'name' => 'agent',
            'type' => 'closure',
            'label' => 'Agent',
            'function' => function ($entry) {
                $agent = $entry->user_ticket->user->agent;
                return $agent->name . " ($agent->code)";
            }
        ]);

        CRUD::addColumn([
            'name' => 'ticket_number',
            'type' => 'closure',
            'label' => 'Ticket Number',
            'function' => function ($entry) {
                $user_ticket = $entry->user_ticket;
                return $user_ticket->number;
            }
        ]);

        CRUD::addColumn([
            'name'     => 'result_type',
            'label'    => 'Result Type',
            'type'     => 'closure',
            'function' => function ($entry) {
                return ucfirst(str_replace('_', ' ', $entry->result_type->name));
            }
        ]);

        CRUD::addColumn([
            'name' => 'amount',
            'type' => 'text',
            'label' => 'Price',
        ]);

        CRUD::addColumn([
            'name' => 'draw_id',
            'label' => 'Draw Date',
            'type'     => 'closure',
            'function' => function ($entry) {
                return $entry->draw_id->format('Y-m-d');
            }
        ]);
        
        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number'])); 
         */
    }
}
