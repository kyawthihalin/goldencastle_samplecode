<?php

namespace App\Http\Controllers\Admin;

use App\Actions\SetCrudPermission;
use App\Http\Requests\LotteryDateRequest;
use App\Models\LotteryDate;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Carbon\Carbon;

/**
 * Class LotteryDateCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class LotteryDateCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation {store as traitStore;}
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\LotteryDate::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/lottery-date');
        CRUD::setEntityNameStrings('lottery date', 'lottery dates');
        (new SetCrudPermission())->execute('income', $this->crud);
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::addColumn([
            'name' => 'date',
        ]);

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(LotteryDateRequest::class);

        CRUD::addField([
            'name' => 'date',
            'type' => 'date_picker',
            'date_picker_options' => [
                'format'   => 'mm-dd',
                'language' => 'en'
            ],
        ]);

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number'])); 
         */
    }

    public function store(LotteryDateRequest $request) 
    {
        $exist = LotteryDate::where('date', Carbon::parse($request->date)->format('m-d'))->first();

        if($exist){
            \Alert::add('error', 'This lottery date already exists.')->flash();
            return redirect(backpack_url('lottery-date'));
        }
        LotteryDate::create([
            'date' => Carbon::parse($request->date)->format('m-d'),
        ]);

        \Alert::add('success', 'Lottery date added successfully.')->flash();
        return redirect(backpack_url('lottery-date'));
    }
}
