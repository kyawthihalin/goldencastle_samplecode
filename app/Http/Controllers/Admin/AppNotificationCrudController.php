<?php

namespace App\Http\Controllers\Admin;

use App\Actions\GetTopic;
use App\Actions\SendNotification;
use App\Actions\SendNotificationWithTopic;
use App\Actions\SetCrudPermission;
use App\Http\Requests\AppNotificationRequest;
use App\Models\AppNotification;
use App\Models\Topic;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class AppNotificationCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class AppNotificationCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\AppNotification::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/app-notification');
        CRUD::setEntityNameStrings('app notification', 'app notifications');
        (new SetCrudPermission())->execute('app_notification', $this->crud);
        CRUD::denyAccess([
            "show",
            "update",
            "delete"
        ]);
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {

        $this->crud->removeAllButtonsFromStack('line');
        $this->crud->enableExportButtons();
        CRUD::addColumn([
            'name' => 'topic_id',
            'type' => 'relationship',
            'label' => 'Topic Name'
        ]);
        CRUD::column('title');
        CRUD::column('description');
        CRUD::column('created_at');
        CRUD::column('updated_at');

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(AppNotificationRequest::class);
        CRUD::addField([
            'name' => 'topic_id',
            'type' => 'select2_from_array',
            'allows_multiple' => false,
            'options'     => (new GetTopic())->execute(),
        ]);
        CRUD::field('title');
        CRUD::addField([
            'name' => 'description',
            'type' => 'textarea',
            'label' => 'Notification Body'
        ]);
        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number'])); 
         */
    }
    public function store(AppNotificationRequest $request)
    {
        $topic = Topic::find($request->topic_id);
        $title = $request->title;
        $desc  = $request->description;
        AppNotification::create([
            'topic_id' => $topic->id,
            'title' => $title,
            'description' => $desc,
        ]);
        $data = [
            "title" => $title,
            "description" => $desc
        ];
        (new SendNotificationWithTopic())->execute("/topics/$topic->topic_name", $data);
        \Alert::add('success', 'Successfully Sent Notification')->flash();
        return redirect('admin/app-notification');
    }
    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
