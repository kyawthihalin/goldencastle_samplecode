<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PasswordRequest;
use App\Models\User;
use App\Services\GetUser;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use App\Actions\SetCrudPermission;

/**
 * Class PasswordCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class PasswordCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Password::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/password');
        CRUD::setEntityNameStrings('password', 'change password');
        CRUD::denyAccess(["list","show","clone","delete"]);
        (new SetCrudPermission())->execute('user_password', $this->crud);

    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(PasswordRequest::class);
        CRUD::addField([
            'name' => 'user',
            'type' => 'select2_from_array',
            'allows_multiple' => false,
            'options'     => (new GetUser())->getUser(),
        ]);
        CRUD::addField([
            'name' => 'password',
            'type' => 'password',
            'label' => "New Password",
        ]);
        CRUD::addField([
            'name' => 'password_confirmation',
            'type' => 'password',
            'label' => "Confirm Password",
        ]);
        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number'])); 
         */
    }

    public function store(PasswordRequest $request)
    {
        $user = User::find($request->user);
        if($user)
        {
            $user->password = bcrypt($request->password);
            $user->save();
            $user->touch();
        }
        \Alert::add('success', 'Successfully Changed Password')->flash();
        return redirect('admin/password/create');
    }
    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
