<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\AppFileRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class AppFileCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class AppFileCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\AppFile::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/app-file');
        CRUD::setEntityNameStrings('app file', 'app files');
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::addColumn([
            'name' => 'file',
            'type' => 'closure',
            'function' => function ($entry) {
                $modified_path = str_replace('\\', '/', $entry->file);
                if($entry->type !== 'golden castle apk'){
                    return "<a target='blank' href='/{$modified_path}' ><img src='/{$modified_path}' alt='{$entry->type}' width='100'/></a> ";
                }
                return "<a href='/{$modified_path}' >Click to download</a> ";
            }
        ]);
        CRUD::column('type');
        CRUD::column('updated_at');

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        CRUD::setValidation(AppFileRequest::class);

        CRUD::addField([
            'name' => 'file',
            'type' => 'browse'
        ]);
        // CRUD::addField([
        //     'name' => 'type',
        //     'label' => 'File Type',
        //     'type' => 'select_from_array',
        //     'options' => [
        //         'first draw photo' => 'First draw photo',
        //         'last draw photo' => 'Last draw photo',
        //         'ticket background photo' => 'Ticket background photo',
        //         'golden castle apk' => 'Golden castle apk',
        //     ]
        // ]);
    }
}
