<?php

namespace App\Http\Controllers\Admin;

use App\Actions\SendNotification as ActionsSendNotification;
use App\Exceptions\InvalidWithdrawStatusException;
use App\Http\Requests\AgentWithdrawRequestRequest;
use App\Http\Requests\WithdrawConfirmRequest;
use App\Http\Requests\WithdrawRefundRequest;
use App\Models\Agent;
use App\Models\AgentWithdrawRequest;
use App\Models\WithdrawOption;
use App\Notifications\SendNotification;
use App\Services\Telegram;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

/**
 * Class WithdrawRequestCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class AgentWithdrawRequestCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\AgentWithdrawRequest::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/agent-withdraw-request');
        CRUD::setEntityNameStrings('agent withdraw request', 'agent withdraw requests');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->removeAllButtonsFromStack('line');
        $this->crud->disableResponsiveTable();
        $this->crud->addClause('orderBy', 'created_at', 'desc');

        $this->crud->addFilter(
            [
                'type'  => 'text',
                'name'  => 'reference_id',
                'label' => 'Reference Id'
            ],
            false,
            function ($value) { // if the filter is active
                $this->crud->addClause('where', 'reference_id', $value);
            }
        );

        // select2_multiple filter
        $this->crud->addFilter([
            'name'  => 'agent',
            'type'  => 'select2_multiple',
            'label' => 'Agent'
        ], function () {
            return Agent::all()->pluck('name', 'id')->toArray();
        }, function ($values) { // if the filter is active
            $this->crud->addClause('whereIn', 'agent_id', json_decode($values));
        });

        // select2_multiple filter
        $this->crud->addFilter([
            'name'  => 'option',
            'type'  => 'select2_multiple',
            'label' => 'Option'
        ], function () {
            return WithdrawOption::all()->pluck('name', 'id')->toArray();
        }, function ($values) { // if the filter is active
            $this->crud->addClause('whereIn', 'withdraw_option_id', json_decode($values));
        });

        $this->crud->addFilter(
            [
                'type'  => 'text',
                'name'  => 'payee',
                'label' => 'Payee'
            ],
            false,
            function ($value) { // if the filter is active
                $this->crud->addClause('where', 'payee', 'LIKE', "%$value%");
            }
        );

        $this->crud->addFilter(
            [
                'type'  => 'text',
                'name'  => 'account_number',
                'label' => 'Account Number'
            ],
            false,
            function ($value) { // if the filter is active
                $this->crud->addClause('where', 'account_number', $value);
            }
        );

        $this->crud->addFilter([
            'name'  => 'status',
            'type'  => 'dropdown',
            'label' => 'Status'
        ], [
            'Requested' => 'Requested',
            'Refunded' => 'Refunded',
            'Completed' => 'Completed',
        ], function ($value) { // if the filter is active
            $this->crud->addClause('where', 'status', $value);
        });

        // CRUD::addColumn([
        //     'name' => 'user',
        //     'type' => 'relationship'
        // ]);

        CRUD::addColumn([
            'name' => 'agent_name',
            'label' => 'Agent Name',
            'attribute' => 'name',
            'entity' => 'agent',
            'type' => 'relationship'
        ]);

        CRUD::addColumn([
            'name' => 'withdraw_option',
            'label' => 'Option',
            'type' => 'relationship'
        ]);

        CRUD::column('reference_id');
        CRUD::column('payee');
        CRUD::column('point');
        CRUD::column('account_number');
        CRUD::addColumn([
            'name' => 'qr_code',
            'type' => 'closure',
            'function' => function ($entry) {
                if (!$entry->qr_code) {
                    return '-';
                }
                $url = url('/') . '/' . 'storage/Withdraw/Requests/' . $entry->id . '/' . $entry->qr_code;
                return "<a href=$url target='__blank'><img src=$url style='width:50px;height:50px'></a>";
            }
        ]);

        CRUD::column('created_at');
        CRUD::addColumn([
            'name' => 'action_at',
            'label' => 'Action At',
            'type' => 'closure',
            'function' => function ($entry) {
                if ($entry->status === 'Requested') {
                    return '-';
                }
                if ($entry->status === 'Completed') {
                    return Carbon::parse($entry->completed_at)->format('Y-m-d') . " (Completed At)";
                }
                if ($entry->status === 'Refunded') {
                    return Carbon::parse($entry->completed_at)->format('Y-m-d') . " (Refunded At)";
                }
            }
        ]);

        CRUD::addColumn([
            'name' => 'status',
            'type' => 'closure',
            'function' => function ($entry) {
                $color = 'success';
                if ($entry->status === 'Requested') {
                    $color = 'warning';
                } elseif ($entry->status === 'Refunded') {
                    $color = 'secondary';
                }

                return "<span class='badge badge-{$color}'>{$entry->status}</span>";
            }
        ]);

        if (
            backpack_user()->can('complete withdraw_requests')
            || backpack_user()->can('complete refund withdraw_requests')
        ) {
            $this->crud->addButtonFromView('line', 'withdraw', 'withdraw', 'beginning');
        }
        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(AgentWithdrawRequestRequest::class);


        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function completeShow(AgentWithdrawRequest $withdraw_request)
    {
        return view('partials.agent-withdraw.complete', [
            'withdraw_request' => $withdraw_request
        ]);
    }

    public function complete(WithdrawConfirmRequest $request, AgentWithdrawRequest $withdraw_request)
    {
        $admin = backpack_user();
        if (!Hash::check($request->password, $admin->password)) {
            return response()->json([
                'message' => 'Your password is incorrect.'
            ], 401);
        }

        $screenshot = DB::transaction(function () use ($request, $withdraw_request) {
            $locked_request = AgentWithdrawRequest::lockForUpdate()->find($withdraw_request->id);

            if ($locked_request->status != 'Requested') {
                throw new InvalidWithdrawStatusException($locked_request->status);
            }

            $locked_request->update([
                'status' => 'Completed',
                'completed_at' => now(),
                'description' => $request->description,
                'withdrawn_amount' => $withdraw_request->point *  config('point.relationship_amount')
            ]);

            $screenshot = null;

            if ($request->hasFile('screenshot')) {
                $file = $request->file('screenshot');
                $file->storeAs(
                    'Withdraw/Requests/' . $locked_request->id,
                    'screenshot.' . $file->extension(),
                    'public'
                );

                $locked_request->update([
                    'screenshot' => 'screenshot.' . $file->extension(),
                ]);

                $screenshot = url('/') . '/' . 'storage/Withdraw/Requests/' . $locked_request->id . '/' .
                    'screenshot.' . $file->extension();
            }

            return $screenshot;
        }, 5);

        $user = $withdraw_request->agent;
        $title = __("withdraw.notification.title.completed");
        $message = __("withdraw.notification.message.completed");
        $user->notify(new SendNotification($title, $message . $withdraw_request->point . ' Points'));
        (new ActionsSendNotification())->execute([$user->noti_token], [
            'Type' => 'Notification',
            'title' => $title,
            'message' => $message,
        ]);

        $withdraw_request->refresh();

        (new Telegram(config('services.telegram.chat_ids.withdraw_agent')))->sendMessage(
            "Withdrawal Completed ( {$user->name} | @{$user->code} )
Reference Id - {$withdraw_request->reference_id}
Option - {$withdraw_request->withdraw_option->name}
Payee - {$withdraw_request->payee}
Account Number - {$withdraw_request->account_number}
Point - {$withdraw_request->point}
Withdrawn Amount - {$withdraw_request->withdrawn_amount}
Description - {$withdraw_request->description}
Date - {$withdraw_request->completed_at}
                ",
            $screenshot,
        );

        \Alert::add('success', 'Withdraw completed successfully.')->flash();
        return redirect('/admin/withdraw-request');
    }

    public function refundShow(AgentWithdrawRequest $withdraw_request)
    {
        return view('partials.agent-withdraw.refund', [
            'withdraw_request' => $withdraw_request
        ]);
    }

    public function refund(WithdrawRefundRequest $request, AgentWithdrawRequest $withdraw_request)
    {
        $admin = backpack_user();
        if (!Hash::check($request->password, $admin->password)) {
            return response()->json([
                'message' => 'Your password is incorrect.'
            ], 401);
        }

        $user = $withdraw_request->agent;

        $screenshot = DB::transaction(function () use ($request, $withdraw_request, $user) {
            $locked_request = AgentWithdrawRequest::lockForUpdate()->find($withdraw_request->id);
            $user_locked = Agent::lockForUpdate()->find($user->id);

            if ($locked_request->status != 'Requested') {
                throw new InvalidWithdrawStatusException($locked_request->status);
            }

            $locked_request->update([
                'status' => 'Refunded',
                'completed_at' => now(),
                'description' => $request->description,
            ]);

            $user_locked_before = $user_locked->point;
            $user_locked_after = $user_locked_before + $withdraw_request->point;
            $user_locked->point = $user_locked_after;
            $user_locked->save();

            $screenshot = null;

            if ($request->hasFile('screenshot')) {
                $file = $request->file('screenshot');
                $file->storeAs(
                    'Withdraw/Requests/' . $locked_request->id,
                    'screenshot.' . $file->extension(),
                    'public'
                );

                $locked_request->update([
                    'screenshot' => 'screenshot.' . $file->extension(),
                ]);

                $screenshot = url('/') . '/' . 'storage/Withdraw/Requests/' . $locked_request->id . '/' .
                    'screenshot.' . $file->extension();
            }

            return $screenshot;
        }, 5);


        $withdraw_request->refresh();
        (new Telegram(config('services.telegram.chat_ids.withdraw_agent')))->sendMessage(
            "Withdrawal Refunded ( {$user->name} | @{$user->code} )
Reference Id - {$withdraw_request->reference_id}
Option - {$withdraw_request->withdraw_option->name}
Payee - {$withdraw_request->payee}
Account Number - {$withdraw_request->account_number}
Refunded Point - {$withdraw_request->point}
Description - {$withdraw_request->description}
Date - {$withdraw_request->completed_at}
                ",
            $screenshot,
        );
        $title = __("withdraw.notification.title.refunded");
        $message = __("withdraw.notification.message.refunded");
        $user->notify(new SendNotification($title, $message));
        (new ActionsSendNotification())->execute([$user->noti_token], [
            'Type' => 'Notification',
            'title' => $title,
            'message' => $message,
        ]);
        \Alert::add('success', 'Withdraw refunded successfully.')->flash();
        return redirect('/admin/withdraw-request');
    }
}
