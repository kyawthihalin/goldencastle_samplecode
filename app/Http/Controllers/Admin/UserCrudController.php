<?php

namespace App\Http\Controllers\Admin;

use App\Actions\SetCrudPermission;
use App\Http\Requests\UserRequest;
use App\Models\Agent;
use App\Models\User;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Doctrine\DBAL\Schema\Column;

/**
 * Class UserCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class UserCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\User::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/user');
        CRUD::setEntityNameStrings('user', 'users');
        (new SetCrudPermission())->execute('user', $this->crud);
        CRUD::denyAccess("show");
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->enableExportButtons();

        $this->crud->addFilter(
            [
                'type'  => 'text',
                'name'  => 'name',
                'label' => 'Name'
            ],
            false,
            function ($value) { // if the filter is active
                $this->crud->addClause('where', 'name', 'LIKE',  "%$value%");
            }
        );

        $this->crud->addFilter(
            [
                'type'  => 'text',
                'name'  => 'user_name',
                'label' => 'User Name'
            ],
            false,
            function ($value) { // if the filter is active
                $this->crud->addClause('where', 'name', $value);
            }
        );
        // select2_multiple filter
        $this->crud->addFilter([
            'name'  => 'agent',
            'type'  => 'select2_multiple',
            'label' => 'Agent'
        ], function () {
            return Agent::all()->pluck('name', 'id')->toArray();
        }, function ($values) { // if the filter is active
            $this->crud->addClause('whereIn', 'agent_id', json_decode($values));
        });

        $this->crud->addFilter(
            [
                'name' => 'reference_id',
                'type' => 'text',
                'label' => 'Reference',
            ],
            false,
            function ($value) {
                $this->crud->addClause('where', 'reference_id', $value);
            }
        );

        // dropdown filter
        $this->crud->addFilter(
            [
                'type'  => 'dropdown',
                'name'  => 'account_status',
                'label' => 'Account Status'
            ],
            [
                'freeze' => 'Freeze',
                'active' => 'Active',
            ],
            function ($value) { // if the filter is active
                $value === 'active' ?
                    $this->crud->addClause('active') : $this->crud->addClause('freeze'); // apply the "active" eloquent scope 
            }
        );

        if (backpack_user()->can('freeze user')) {
            $this->crud->addButtonFromView('line', 'freeze', 'freeze', 'beginning');
        }
        if (backpack_user()->can('unfreeze user')) {
            $this->crud->addButtonFromView('line', 'unfreeze', 'unfreeze', 'beginning');
        }
        CRUD::column('name');
        CRUD::column('user_name');
        CRUD::addColumn([
            'name' => 'agent_id',
            'type' => 'relationship',
            'label' => 'Agent',
        ]);
        CRUD::column('point');
        CRUD::column('reference_id');
        CRUD::addColumn([
            'name' => 'created_at',
            'label' => 'Registered At',
        ]);

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */
    }


    public function freeze(User $user)
    {
        $user->frozen_at = now();
        $user->save();

        return response()->json([
            'message' => 'Successfully locked this user account.'
        ]);
    }

    public function unfreeze(User $user)
    {
        $user->frozen_at = null;
        $user->save();

        return response()->json([
            'message' => 'Successfully unlocked this user account.'
        ]);
    }
}
