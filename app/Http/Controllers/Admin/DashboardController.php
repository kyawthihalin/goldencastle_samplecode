<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\Agent;
use App\Models\TicketPrice;
use App\Models\TicketResult;
use App\Models\User;
use App\Models\UserTicket;
use App\Services\DrawIdService;

class DashboardController extends Controller
{
    public function index()
    {
        $admin_count = Admin::get()->count();
        $user_count = User::get()->count();
        $agent_count = Agent::get()->count();
        $next_draw = (new DrawIdService())->getCurrentDrawId();
        $current_draw_income = UserTicket::where("draw_id", $next_draw)->sum("amount");
        $ticket_price = TicketPrice::first();
        $total_win_ticket_count = TicketResult::where("draw_id", $next_draw)->count();
        return view("admin.dashboard")->with([
            "agent_count" => number_format($agent_count),
            "user_count" => number_format($user_count),
            "admin_count" => number_format($admin_count),
            "current_draw_income" => number_format($current_draw_income),
            "ticket_price" => number_format($ticket_price->price),
            "total_win_ticket_count" => number_format($total_win_ticket_count),
        ]);
    }
}
