<?php

namespace App\Http\Controllers\Admin\Option;

use App\Http\Controllers\Controller;
use App\Models\Agent;
use Illuminate\Http\Request;

class AgentController extends Controller
{
    public function index(Request $request)
    {
        $term = $request->input('term');
        $options = Agent::where('name', 'like', '%' . $term . '%')->get()->pluck('name', 'id');
        return $options;
    }
}
