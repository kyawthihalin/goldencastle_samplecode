<?php

namespace App\Http\Controllers\Admin\Option;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $term = $request->input('term');
        $options = User::where('name', 'like', '%' . $term . '%')->get()->pluck('name', 'id');
        return $options;
    }
}
