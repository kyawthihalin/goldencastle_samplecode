<?php

namespace App\Http\Controllers\Admin;

use App\Actions\SendNotification as SendNotificationAction;
use App\Actions\SetCrudPermission;
use App\Http\Requests\AgentNotificationRequest;
use App\Models\Agent;
use App\Models\Notification;
use App\Notifications\SendNotification;
use App\Services\GetUser;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class AgentNotificationCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class AgentNotificationCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\AgentNotification::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/agent-notification');
        CRUD::setEntityNameStrings('agent notification', 'agent notifications');
        (new SetCrudPermission())->execute('agent_notification', $this->crud);
        CRUD::denyAccess([
            "show",
            "update",
            "delete"
        ]);
        CRUD::addClause("where", "notifiable_type", "App\Models\Agent");
    }
    public function showDetailsRow($id)
    {
        $notification = Notification::find($id);
        return view('partials.notiDetail', ['notification' => $notification]);
    }
    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->enableExportButtons();
        $this->crud->enableDetailsRow();
        $this->crud->removeAllButtonsFromStack('line');
        CRUD::addColumn([
            'name' => 'type',
            'type'     => 'closure',
            'label' => "Type of Notification",
            'function' => function ($entry) {
                if ($entry->type == "App\Notifications\SendNotification") {
                    return "Sent Notification";
                }
            }
        ]);
        CRUD::addColumn([
            'name' => 'notifiable_type',
            'type'     => 'closure',
            'label' => "Type of User",
            'function' => function ($entry) {
                if ($entry->notifiable_type == "App\Models\Agent") {
                    return "Agent";
                }
            }
        ]);
        CRUD::addColumn([
            'name' => 'notifiable_id',
            'type'     => 'closure',
            'label' => "User ID",
            'function' => function ($entry) {
                return $entry->notifiable_id;
            }
        ]);
        CRUD::column('read_at');
        CRUD::column('created_at');
        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(AgentNotificationRequest::class);
        CRUD::addField([
            'name' => 'title',
            'type' => 'text',
            'label' => 'Title'
        ]);
        CRUD::addField([
            'name' => 'desc',
            'type' => 'textarea',
            'label' => 'Description'
        ]);
        CRUD::addField([
            'name' => 'agents',
            'type' => 'select2_from_array',
            'allows_multiple' => true,
            'options'     => (new GetUser())->getAgent(),
        ]);

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number'])); 
         */
    }
  
    public function store(AgentNotificationRequest $request)
    {
        $data = [
            "title" => $request->title,
            "message" => $request->desc,
            'Type' => "Notification"
        ];
        $tokens = [];

        foreach ($request->agents as $user) {
            $agent_obj = Agent::find($user);
            $agent_obj->notify(new SendNotification($request->title, $request->desc));
            $tokens[] = $agent_obj->noti_token;
        }
        $remove_nulls = array_filter($tokens, fn($value) => !is_null($value) && $value !== '');
        if(count($remove_nulls) > 0)
        {
            (new SendNotificationAction())->execute($remove_nulls,$data);
        }
        \Alert::add('success', 'Successfully Sent Notification')->flash();
        return redirect('admin/agent-notification');
    }
    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
