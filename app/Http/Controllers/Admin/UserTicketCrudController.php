<?php

namespace App\Http\Controllers\Admin;

use App\Actions\SetCrudPermission;
use App\Http\Requests\UserTicketRequest;
use App\Models\User;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

/**
 * Class UserTicketCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class UserTicketCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\UserTicket::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/user-ticket');
        CRUD::setEntityNameStrings('user ticket', 'user tickets');
        (new SetCrudPermission())->execute('user_ticket', $this->crud);
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->enableExportButtons();
        $this->crud->disableResponsiveTable();

        // select2_multiple filter
        $this->crud->addFilter([
            'name'  => 'user',
            'type'  => 'select2_multiple',
            'label' => 'User'
        ], function () {
            return User::all()->pluck('name', 'id')->toArray();
        }, function ($values) { // if the filter is active
            $this->crud->addClause('whereIn', 'user_id', json_decode($values));
        });

        $this->crud->addFilter(
            [
                'name' => 'reference_id',
                'type' => 'text',
                'label' => 'Reference',
            ],
            false,
            function ($value) {
                $this->crud->addClause('where', 'reference_id', $value);
            }
        );

        $this->crud->addFilter(
            [
                'name' => 'number',
                'type' => 'text',
                'label' => 'Number',
            ],
            false,
            function ($value) {
                $this->crud->addClause('where', 'number', $value);
            }
        );

        // daterange filter
        $this->crud->addFilter(
            [
                'type'  => 'date_range',
                'name'  => 'purchased_date',
                'label' => 'Purchased date'
            ],
            false,
            function ($value) { // if the filter is active, apply these constraints
                $dates = json_decode($value);
                $this->crud->addClause('where', 'purchased_date', '>=', $dates->from);
                $this->crud->addClause('where', 'purchased_date', '<=', $dates->to . ' 23:59:59');
            }
        );

        $this->crud->addFilter([
            'name'  => 'draw_id',
            'type'  => 'select2',
            'label' => 'Draw Date'
        ], function () {
            return DB::table('results')
                ->orderBy('date', 'desc')
                ->select('date')->distinct()->limit(6)
                ->pluck('date', 'date')->toArray();
        }, function ($value) { // if the filter is active
            $this->crud->addClause('where', 'draw_id', Carbon::parse($value));
        });

        CRUD::addColumn([
            'name' => 'user_id',
            'type' => 'relationship',
            'label' => 'User'
        ]);
        CRUD::column('reference_id');
        CRUD::addColumn([
            'name' => 'number',
            'label' => 'Ticket Number'
        ]);
        CRUD::column('purchased_date');
        CRUD::column('amount');
        CRUD::column('commission');
        CRUD::column('profit');
        CRUD::addColumn([
            'name' => 'draw_id',
            'label' => 'Draw Date',
            'type'     => 'closure',
            'function' => function ($entry) {
                return $entry->draw_id->format('Y-m-d');
            }
        ]);

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupShowOperation()
    {
        CRUD::addColumn([
            'name' => 'user_id',
            'type' => 'relationship',
            'label' => 'User'
        ]);
        CRUD::column('reference_id');
        CRUD::addColumn([
            'name' => 'number',
            'label' => 'Ticket Number'
        ]);
        CRUD::column('purchased_date');
        CRUD::addColumn([
            'name' => 'draw_id',
            'label' => 'Draw Date',
            'type'     => 'closure',
            'function' => function ($entry) {
                return $entry->draw_id->format('Y-m-d');
            }
        ]);
        CRUD::column('amount');
    }
}
