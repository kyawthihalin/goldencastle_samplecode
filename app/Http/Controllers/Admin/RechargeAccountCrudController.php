<?php

namespace App\Http\Controllers\Admin;

use App\Actions\SetCrudPermission;
use App\Http\Requests\RechargeAccountRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class RechargeAccountCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class RechargeAccountCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\RechargeAccount::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/recharge-account');
        CRUD::setEntityNameStrings('recharge account', 'recharge accounts');
        CRUD::denyAccess(["show"]);
        (new SetCrudPermission())->execute('recharge_accounts', $this->crud);
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::column('name');
        CRUD::column('account_name');
        CRUD::addColumn([
            "name" => "account_photo",
            "label" => "Account Photo",
            "type" => "image"
        ]);
        CRUD::addColumn([
            "name" => "phone_number",
            "label" => "Account Number/Phone Number",
            "type" => "text"
        ]);
        CRUD::addColumn([
            "name" => "qr_code",
            "label" => "QR Code",
            "type" => "image"
        ]);
        CRUD::addColumn([
            "name" => "status",
            "label" => "Status",
            "type" => "text"
        ]);

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(RechargeAccountRequest::class);

        CRUD::field('name');
        CRUD::field('account_name');
        CRUD::addField([   // Browse
            'name'  => 'account_photo',
            'label' => 'Select Account Photo',
            'type'  => 'browse'
        ]);
        CRUD::addField([   // Browse
            'name'  => 'phone_number',
            'label' => 'Account Number/Phone Number',
            'type'  => 'text'
        ]);
        CRUD::addField([   // Browse
            'name'  => 'qr_code',
            'label' => 'Select QR Code',
            'type'  => 'browse'
        ]);
        CRUD::addField([
            'name' => 'status',
            'type' => 'select_from_array',
            'label' => 'Status',
            'options'     => ['Active' => 'Active', 'Inactive' => 'Inactive'],
            'allows_null' => false,
        ]);

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number'])); 
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
