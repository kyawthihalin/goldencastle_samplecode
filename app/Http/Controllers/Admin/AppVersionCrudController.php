<?php

namespace App\Http\Controllers\Admin;

use App\Actions\SetCrudPermission;
use App\Http\Requests\AppVersionRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Http\Request;

/**
 * Class AppVersionCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class AppVersionCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\AppVersion::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/app-version');
        CRUD::setEntityNameStrings('app version', 'app versions');
        (new SetCrudPermission())->execute('app_version', $this->crud);
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->enableExportButtons();
        CRUD::addColumn([
            "name" => "admin",
            "label" => "Admin Name",
            'type'  => 'relationship',
        ]);
        CRUD::addColumn([
            'name'         => 'app_type', // name of relationship method in the model
            'type'         => 'relationship',
            'label'        => 'App Type', // Table column heading
        ]);
        CRUD::column('version');
        CRUD::column('features');
        CRUD::column('created_at');
        CRUD::column('updated_at');

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(AppVersionRequest::class);
        CRUD::addField([
            'label'     => "App Type",
            'type'      => 'select2',
            'name'      => 'app_type_id', // the db column for the foreign key
            // optional
            'entity'    => 'app_type', // the method that defines the relationship in your Model
            'model'     => "App\Models\AppType", // foreign key model
            'attribute' => 'name', // foreign key attribute that is shown to user
        ]);
        CRUD::field('version');
        CRUD::addField([
            'name'  => 'features',
            'label' => 'Description',
            'type'  => 'textarea',
        ]);


        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number'])); 
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
