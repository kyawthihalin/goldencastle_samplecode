<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\FillPointRequest;
use App\Models\FillPoint;
use App\Models\User;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Support\Facades\DB;
use App\Actions\SendNotification as SendNotificationAction;
use App\Actions\SetCrudPermission;
use App\Models\Admin;
use App\Notifications\SendNotification;
use Illuminate\Http\Request;

/**
 * Class FillPointCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class FillPointCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\FillPoint::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/fill-point');
        CRUD::setEntityNameStrings('fill point', 'fill points');
        CRUD::denyAccess(["show", "update", "delete", "clone"]);
        (new SetCrudPermission())->execute('fill_points', $this->crud);
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        // select2_multiple filter
        $this->crud->addFilter([
            'name'  => 'user',
            'type'  => 'select2_multiple',
            'label' => 'User'
        ], function () {
            return User::all()->pluck('name', 'id')->toArray();
        }, function ($values) { // if the filter is active
            $this->crud->addClause('whereIn', 'user_id', json_decode($values));
        });
        // select2_multiple filter
        $this->crud->addFilter([
            'name'  => 'admin',
            'type'  => 'select2_multiple',
            'label' => 'Admin'
        ], function () {
            return Admin::all()->pluck('name', 'id')->toArray();
        }, function ($values) { // if the filter is active
            $this->crud->addClause('whereIn', 'admin_id', json_decode($values));
        });
        $this->crud->removeAllButtonsFromStack('line');
        $this->crud->enableExportButtons();
        CRUD::column('user_id');
        CRUD::column('admin_id');
        CRUD::column('amount');
        CRUD::column('created_at');
        CRUD::column('updated_at');

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(FillPointRequest::class);
        // CRUD::field('user_id');
        CRUD::addField([
            'label'       => "Choose User", // Table column heading
            'type'        => "select2_from_ajax",
            'name'        => 'user_id', // the column that contains the ID of that connected entity
            'entity'      => 'user', // the method that defines the relationship in your Model
            'attribute'   => "name",
            'model'       => '\App\Models\AgentWithdrawRequest',
            'data_source' => url("admin/fill-point/get-user-name"),
            'placeholder'             => "Choose User to Fill Point",
            'minimum_input_length'    => 0,
            'maximum_input_length'    => 0,
        ]);

        CRUD::field('amount');



        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }
    public function store(FillPointRequest $request)
    {
        $data = [
            "title" => "Point Filled",
            "message" => "$request->amount points filled to your account!",
            'Type' => "Notification"
        ];
        DB::transaction(function () use ($request, $data) {
            $amount = $request->amount;
            $filled_by = backpack_user()->id;
            $user_locked = User::lockForUpdate()->find($request->user_id);
            $user_locked->update([
                'point' => $user_locked->point + $amount / config('point.relationship_amount')
            ]);
            FillPoint::create([
                'amount' => $amount,
                'admin_id' => $filled_by,
                'user_id' => $request->user_id
            ]);
            $user_locked->notify(new SendNotification($data['title'], $data['message']));

            (new SendNotificationAction())->execute([$user_locked->noti_token], $data);
        }, 5);

        \Alert::add('success', 'Successfully Filled Point')->flash();
        return redirect('admin/fill-point');
    }
    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function getUserName(Request $request)
    {
        $search_term = $request->input('q');
        $data = $this->SelectFromArrayUser($search_term);

        return response()->json([
            'data' => $data['data'],
            'current_page' => $data['current_page'],
            'last_page' => $data['last_page']
        ], 200);
    }

    public function SelectFromArrayUser($request)
    {
        if ($request) {
            $results = User::where('name', 'LIKE', '%' . $request . '%')->orWhere('phone_number', 'LIKE', '%' . $request . '%')->orWhere('user_name', 'LIKE', '%' . $request . '%')->paginate(10);
        } else {
            $results = User::paginate(10);
        }

        $data = [
            'data'          => $this->formatData($results),
            'current_page'  => $results->currentPage(),
            'last_page'     => $results->lastPage()
        ];
        return $data;
    }

    public function formatData($datas)
    {
        $arr = [];
        $count = 0;
        foreach ($datas as $data) {
            $nick_name = $data->user_name . '-' . $data->phone_number;
            $arr[$count]['id'] = $data->id;
            $arr[$count]['name'] = $data->name . " (" . $nick_name . ")";
            $count++;
        }

        return $arr;
    }
}
