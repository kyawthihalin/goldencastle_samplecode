<?php

namespace App\Http\Controllers\Admin;

use App\Actions\CreateResult;
use App\Actions\SetCrudPermission;
use App\Http\Requests\ResultRequest;
use App\Models\LotteryDate;
use App\Models\ResultType;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Carbon\Carbon;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;

/**
 * Class ResultCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ResultCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation {
        store as traitStore;
    }
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Result::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/result');
        CRUD::setEntityNameStrings('result', 'results');
        (new SetCrudPermission())->execute('result', $this->crud);
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->enableExportButtons();
        $this->crud->disableResponsiveTable();
        $this->crud->orderBy('date', 'desc');
        $this->crud->addFilter([
            'name'  => 'app_type',
            'type'  => 'select2_multiple',
            'label' => 'Result Type'
        ], function () {
            return ResultType::all()->pluck('name', 'id')->toArray();
        }, function ($values) { // if the filter is active
            $this->crud->addClause('whereHas', 'result_type', function ($query) use ($values) {
                $query->whereIn('id', json_decode($values));
            });
        });

        $this->crud->addFilter([
            'name'  => 'date',
            'type'  => 'select2',
            'label' => 'Draw Date'
        ], function () {
            return DB::table('results')
                ->orderBy('date', 'desc')
                ->select('date')->distinct()->limit(6)
                ->pluck('date', 'date')->toArray();
        }, function ($value) { // if the filter is active
            $this->crud->addClause('where', 'date', Carbon::parse($value));
        });

        $this->crud->addFilter([
            'name'  => 'number',
            'type'  => 'text',
            'label' => 'Result Number'
        ], false, function ($value) { // if the filter is active


            $this->crud->addClause('where', 'result', 'LIKE', "%$value%");
        });

        CRUD::addColumn([
            'name'     => 'result_type',
            'label'    => 'Result Type',
            'type'     => 'closure',
            'function' => function ($entry) {
                return ucfirst(str_replace('_', ' ', $entry->result_type->name));
            }
        ]);

        CRUD::addColumn([
            'name'     => 'result',
            'label'    => 'Result Numbers',
            'type'     => 'closure',
            'function' => function ($entry) {
                $string = '';
                foreach ($entry->result as $key => $result) {
                    $break = "";
                    if (($key + 1) % 10 === 0) {
                        $break = "<br>";
                    }
                    $string .= $result;
                    if ($key != count($entry->result) - 1) {
                        $string .= ', ' . $break;
                    }
                }

                return $string;
            },
            'wrapperAttributes' => [
                'class' => 'form-group custom-width'
            ]
        ]);
        CRUD::addColumn([
            'name' => 'date',
            'label' => 'Draw Date',
            'type'     => 'closure',
            'function' => function ($entry) {
                return $entry->date->format('Y-m-d');
            }
        ]);

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupShowOperation()
    {
        $this->crud->set('show.setFromDb', false);

        CRUD::addColumn([
            'name'     => 'result_type',
            'label'    => 'Result Type',
            'type'     => 'closure',
            'function' => function ($entry) {
                return ucfirst(str_replace('_', ' ', $entry->result_type->name));
            }
        ]);

        CRUD::addColumn([
            'name'     => 'result',
            'label'    => 'Result Ticket',
            'type'     => 'closure',
            'function' => function ($entry) {
                $string = '';
                foreach ($entry->result as $key => $result) {
                    $break = "";
                    if (($key + 1) % 10 === 0) {
                        $break = "<br>";
                    }
                    $string .= $result . ', ' . $break;
                }

                return $string;
            }
        ]);

        CRUD::addColumn([
            'name' => 'date',
            'label' => 'Draw Date',
            'type'     => 'closure',
            'function' => function ($entry) {
                return $entry->date->format('Y-m-d');
            }
        ]);

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number'])); 
         */
    }

    protected function setupCreateOperation()
    {
        CRUD::setValidation(ResultRequest::class);

        CRUD::addField([
            'name' => 'date',
            'type' => 'select_from_array',
            'options' => LotteryDate::all()->pluck('date', 'date')->toArray(),
        ]);
    }


    public function store(ResultRequest $request)
    {
        $message = (new CreateResult())->execute(now()->year . '-' . $request->date);

        if ($message === 'success') {
            \Alert::add('success', 'Result created successfully.')->flash();
        } else {
            \Alert::add('error', $message)->flash();
        }

        return redirect(backpack_url('result'));
    }
}
