<?php

namespace App\Http\Controllers\Admin;

use App\Actions\GenerateReference;
use App\Actions\SetCrudPermission;
use App\Http\Requests\AgentRequest;
use App\Models\Admin;
use App\Models\Agent;
use App\Models\State;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use App\Actions\GenerateReferenceId;
use App\Http\Requests\AgentUpdateRequest;
use Illuminate\Support\Str;

/**
 * Class AgentCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class AgentCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Agent::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/agent');
        CRUD::setEntityNameStrings('agent', 'agents');
        (new SetCrudPermission())->execute('agent', $this->crud);
        CRUD::denyAccess("show");
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->enableExportButtons();
        // select2_multiple filter
        $this->crud->addFilter([
            'name'  => 'agent',
            'type'  => 'select2_multiple',
            'label' => 'Agent'
        ], function () {
            return Agent::all()->pluck('name', 'id')->toArray();
        }, function ($values) { // if the filter is active
            $this->crud->addClause('whereIn', 'id', json_decode($values));
        });

        // select2_multiple filter
        $this->crud->addFilter([
            'name'  => 'state',
            'type'  => 'select2_multiple',
            'label' => 'State'
        ], function () {
            return State::all()->pluck('name_en', 'id')->toArray();
        }, function ($values) { // if the filter is active
            $this->crud->addClause('whereIn', 'state_id', json_decode($values));
        });

        $this->crud->addFilter(
            [
                'name' => 'phone_number',
                'type' => 'text',
                'label' => 'Phone Number',
            ],
            false,
            function ($value) {
                $this->crud->addClause('where', 'phone_number', 'like', "%$value%");
            }
        );
        $this->crud->addFilter(
            [
                'name' => 'nrc',
                'type' => 'text',
                'label' => 'NRC',
            ],
            false,
            function ($value) {
                $this->crud->addClause('where', 'nrc', 'like', "%$value%");
            }
        );

        $this->crud->addFilter([
            'name'  => 'admin',
            'type'  => 'select2_multiple',
            'label' => 'Created by'
        ], function () {
            return Admin::all()->pluck('name', 'id')->toArray();
        }, function ($values) { // if the filter is active
            $this->crud->addClause('whereIn', 'admin_id', json_decode($values));
        });

        if (backpack_user()->can('freeze agent')) {
            $this->crud->addButtonFromView('line', 'freeze', 'freeze', 'beginning');
        }
        if (backpack_user()->can('unfreeze agent')) {
            $this->crud->addButtonFromView('line', 'unfreeze', 'unfreeze', 'beginning');
        }

        CRUD::column('name');
        CRUD::column('code');
        CRUD::addColumn([
            'name' => 'state_id',
            'type' => 'relationship',
            'label' => 'State',
            'attribute' => 'name_en'
        ]);
        // CRUD::column('reference_id');
        CRUD::column('nrc');
        CRUD::column('point');
        CRUD::addColumn([
            'name' => 'admin_id',
            'type' => 'relationship',
            'label' => 'Created by',
        ]);
        CRUD::column('phone_number');
        CRUD::addColumn([
            'name' => 'nrc_front',
            'type' => 'image',
            'label' => 'NRC FRONT',
        ]);
        CRUD::addColumn([
            'name' => 'nrc_back',
            'type' => 'image',
            'label' => 'NRC BACK',
        ]);
        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        // $generated = (new GenerateReferenceId())->execute("AG","001");
        CRUD::setValidation(AgentRequest::class);

        CRUD::addField([  // Select2
            'label'     => "State",
            'type'      => 'select2',
            'name'      => 'state_id', // the db column for the foreign key
            // optional
            'entity'    => 'state', // the method that defines the relationship in your Model
            'model'     => "App\Models\State", // foreign key model
            'attribute' => 'name_en', // foreign key attribute that is shown to 
            'wrapper'   => [
                'class'      => 'form-group col-md-6'
            ], // change the HTML attributes for the field wrapper - mostly for resizing fields 
        ],);

        CRUD::addField([  // Select2
            'label'     => "Agent Name",
            'type'      => 'text',
            'name'      => 'name', // the db column for the foreign key
            'attributes' => [
                'placeholder' => 'Enter agent"s name here...',
            ],
            'wrapper'   => [
                'class'      => 'form-group col-md-6'
            ], // change the HTML attributes for the field wrapper - mostly for resizing fields 
        ],);
        CRUD::addField([  // Select2
            'label'     => "NRC",
            'type'      => 'text',
            'name'      => 'nrc', // the db column for the foreign key
            'attributes' => [
                'placeholder' => 'Enter NRC here...',
            ],
            'wrapper'   => [
                'class'      => 'form-group col-md-6'
            ], // change the HTML attributes for the field wrapper - mostly for resizing fields 
        ],);
        CRUD::addField([  // Select2
            'label'     => "Password",
            'type'      => 'password',
            'name'      => 'password', // the db column for the foreign key
            'attributes' => [
                'placeholder' => 'Enter Password here...',
            ],
            'wrapper'   => [
                'class'      => 'form-group col-md-6'
            ], // change the HTML attributes for the field wrapper - mostly for resizing fields 
        ],);

        CRUD::addField([  // Select2
            'label'     => "Confirm Password",
            'type'      => 'password',
            'name'      => 'password_confirmation', // the db column for the foreign key
            'attributes' => [
                'placeholder' => 'Enter Password here...',
            ],
            'wrapper'   => [
                'class'      => 'form-group col-md-6'
            ], // change the HTML attributes for the field wrapper - mostly for resizing fields 
        ],);
        CRUD::addField([  // Select2
            'label'     => "Phone Number",
            'type'      => 'text',
            'name'      => 'phone_number', // the db column for the foreign key
            'attributes' => [
                'placeholder' => 'Enter Phone Number here...',
            ],
            'wrapper'   => [
                'class'      => 'form-group col-md-6'
            ], // change the HTML attributes for the field wrapper - mostly for resizing fields 
        ],);


        CRUD::addField([   // Browse
            'name'  => 'nrc_front',
            'label' => 'Nrc Front Photo',
            'type'  => 'browse',
            'wrapper'   => [
                'class'      => 'form-group col-md-6'
            ],
        ],);
        CRUD::addField([   // Browse
            'name'  => 'nrc_back',
            'label' => 'Nrc Back Photo',
            'type'  => 'browse',
            'wrapper'   => [
                'class'      => 'form-group col-md-6'
            ],
        ],);

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number'])); 
         */
    }
    public function store(AgentRequest $request)
    {
        $state = State::find($request->state_id);
        $agent = Agent::create([
            'state_id' => $state->id,
            'name' => $request->name,
            'password' => bcrypt($request->password),
            'nrc' => $request->nrc,
            'code' => Str::uuid(),
            'admin_id' => backpack_user()->id,
            'phone_number' => $request->phone_number,
            'nrc_front' => $request->nrc_front,
            'nrc_back' => $request->nrc_back
        ]);

        $agent->refresh();
        $agent->update([
            'code' => (new GenerateReference())->execute($state->code, $agent->sequence, 4, 0)
        ]);
        \Alert::add('success', 'Successfully Created Agent')->flash();
        return redirect('admin/agent');
    }
    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        CRUD::setValidation(AgentUpdateRequest::class);
        CRUD::addField([  // Select2
            'label'     => "State",
            'type'      => 'select2',
            'name'      => 'state_id', // the db column for the foreign key
            // optional
            'entity'    => 'state', // the method that defines the relationship in your Model
            'model'     => "App\Models\State", // foreign key model
            'attribute' => 'name_en', // foreign key attribute that is shown to 
            'wrapper'   => [
                'class'      => 'form-group col-md-6'
            ], // change the HTML attributes for the field wrapper - mostly for resizing fields 
        ],);

        CRUD::addField([  // Select2
            'label'     => "Agent Name",
            'type'      => 'text',
            'name'      => 'name', // the db column for the foreign key
            'attributes' => [
                'placeholder' => 'Enter agent"s name here...',
            ],
            'wrapper'   => [
                'class'      => 'form-group col-md-6'
            ], // change the HTML attributes for the field wrapper - mostly for resizing fields 
        ],);
        CRUD::addField([  // Select2
            'label'     => "NRC",
            'type'      => 'text',
            'name'      => 'nrc', // the db column for the foreign key
            'attributes' => [
                'placeholder' => 'Enter NRC here...',
            ],
            'wrapper'   => [
                'class'      => 'form-group col-md-6'
            ], // change the HTML attributes for the field wrapper - mostly for resizing fields 
        ],);
        CRUD::addField([  // Select2
            'label'     => "Phone Number",
            'type'      => 'text',
            'name'      => 'phone_number', // the db column for the foreign key
            'attributes' => [
                'placeholder' => 'Enter Phone Number here...',
            ],
            'wrapper'   => [
                'class'      => 'form-group col-md-6'
            ], // change the HTML attributes for the field wrapper - mostly for resizing fields 
        ],);


        CRUD::addField([   // Browse
            'name'  => 'nrc_front',
            'label' => 'Nrc Front Photo',
            'type'  => 'browse',
            'wrapper'   => [
                'class'      => 'form-group col-md-6'
            ],
        ],);
        CRUD::addField([   // Browse
            'name'  => 'nrc_back',
            'label' => 'Nrc Back Photo',
            'type'  => 'browse',
            'wrapper'   => [
                'class'      => 'form-group col-md-6'
            ],
        ],);
    }

    public function freeze(Agent $agent)
    {
        $agent->frozen_at = now();
        $agent->save();

        return response()->json([
            'message' => 'Successfully locked this agent account.'
        ]);
    }

    public function unfreeze(Agent $agent)
    {
        $agent->frozen_at = null;
        $agent->save();

        return response()->json([
            'message' => 'Successfully unlocked this agent account.'
        ]);
    }
}
