<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\RechargeRequestRequest;
use App\Models\Admin;
use App\Models\RechargeAccount;
use App\Models\RechargeRequest;
use App\Models\User;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Support\Facades\DB;
use App\Actions\SendNotification as SendNotificationAction;
use App\Actions\SetCrudPermission;
use Illuminate\Support\Facades\Storage;
use App\Notifications\SendNotification;

/**
 * Class RechargeRequestCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class RechargeRequestCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\RechargeRequest::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/recharge-request');
        CRUD::setEntityNameStrings('recharge request', 'recharge requests');
        CRUD::denyAccess(['create', 'show', 'delete', 'clone']);
        (new SetCrudPermission())->execute('recharge_requests', $this->crud);
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->addClause('orderBy', 'created_at', 'desc');
        // select2_multiple filter
        $this->crud->addFilter([
            'name'  => 'user',
            'type'  => 'select2_multiple',
            'label' => 'User'
        ], function () {
            return User::all()->pluck('name', 'id')->toArray();
        }, function ($values) { // if the filter is active
            $this->crud->addClause('whereIn', 'user_id', json_decode($values));
        });

        // select2_multiple filter
        $this->crud->addFilter([
            'name'  => 'admin',
            'type'  => 'select2_multiple',
            'label' => 'Confirmed By'
        ], function () {
            return Admin::all()->pluck('name', 'id')->toArray();
        }, function ($values) { // if the filter is active
            $this->crud->addClause('whereIn', 'confirmed_by', json_decode($values));
        });

        // select2_multiple filter
        $this->crud->addFilter([
            'name'  => 'recharge',
            'type'  => 'select2_multiple',
            'label' => 'Recharge Accounts'
        ], function () {
            return RechargeAccount::all()->pluck('name', 'id')->toArray();
        }, function ($values) { // if the filter is active
            $this->crud->addClause('whereIn', 'recharge_account_id', json_decode($values));
        });

        $this->crud->addFilter([
            'name'  => 'status',
            'type'  => 'dropdown',
            'label' => 'Status'
        ], [
            "Completed" => 'Completed',
            "Requested" => 'Requested',
            "Rejected" => 'Rejected',
        ], function ($value) { // if the filter is active
            $this->crud->addClause('where', 'status', $value);
        });

        $this->crud->removeAllButtonsFromStack('line');
        CRUD::column('reference_id');
        CRUD::column('recharge_account_id');
        CRUD::column('user_id');
        CRUD::column('confirmed_amount');
        CRUD::addColumn([
            'name' => 'confirmed_by',
            'type' => 'relationship',
            'label' => 'Action By',
            'attribute' => 'name',
            'entity' => 'confirm_user',
        ]);
        CRUD::addColumn([
            'label' => 'Screenshot',
            'type' => 'closure',
            'function' => function ($entry) {
                // $url = url('/') . '/' . 'storage/Recharge/Requests/' . $entry->id . '/' . $entry->screenshot;
                $url =  Storage::url("Recharge/Requests/$entry->id/$entry->screenshot");
                return    "<a href=$url target='__blank'><img src=$url style='width:50px;height:60px'></a>";
            }
        ]);
        CRUD::addColumn([
            'label' => 'Status',
            'type' => 'closure',
            'function' => function ($entry) {
                if ($entry->status == "Requested") {
                    return '<span class="badge badge-pill badge-warning text-dark">Requested</span>';
                }
                if ($entry->status == "Completed") {
                    return '<span class="badge badge-pill badge-success text-white">Completed</span>';
                }
                if ($entry->status == "Rejected") {
                    return '<span class="badge badge-pill badge-danger text-white">Rejected</span>';
                }
            }
        ]);
        if (backpack_user()->can('reject recharge_requests')) {
            $this->crud->addButtonFromView('line', 'reject', 'reject', 'beginning');
        }
        if (backpack_user()->can('confirm recharge_requests')) {
            $this->crud->addButtonFromView('line', 'confirm', 'confirm', 'beginning');
        }
        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(RechargeRequestRequest::class);

        CRUD::addField([  // Select2
            'label'     => "Account Type",
            'type'      => 'select2',
            'name'      => 'recharge_account_id', // the db column for the foreign key
            'entity'    => 'recharge_account', // the method that defines the relationship in your Model
            'model'     => "App\Models\RechargeAccount", // foreign key model
            'attribute' => 'name', // foreign key attribute that is shown to
            'attributes' => [
                'readonly' => 'readonly',
                'disabled' => 'disabled'
            ],
        ]);


        CRUD::addField([  // Select2
            'label'     => "User",
            'type'      => 'select2',
            'name'      => 'user_id', // the db column for the foreign key
            'entity'    => 'user', // the method that defines the relationship in your Model
            'model'     => "App\Models\User", // foreign key model
            'attribute' => 'name', // foreign key attribute that is shown to
            'attributes' => [
                'readonly' => 'readonly',
                'disabled' => 'disabled'
            ],
        ]);
        CRUD::addField([
            'name' => 'confirmed_amount',
            'type' => 'number',
            'label' => 'Amount'
        ]);

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }
    public function update(RechargeRequestRequest $request)
    {
        $data = [
            "title" => "Recharge Request Completed",
            "message" => "Your Recharge Request has been confirmed!",
            'Type' => "Notification"
        ];
        $recharge = RechargeRequest::find($request->id);
        $user_locked = DB::transaction(function () use ($request, $recharge, $data) {
            $confirmed_amount = $request->confirmed_amount;
            $confirmed_by = backpack_user()->id;
            $status = "Completed";
            $user_locked = User::lockForUpdate()->find($recharge->user_id);
            $user_locked->update([
                'point' => $user_locked->point + ($confirmed_amount / (config('point.relationship_amount') ?? 1))
            ]);
            $recharge->update([
                "confirmed_amount" => $confirmed_amount,
                "confirmed_by" => $confirmed_by,
                "status" => $status
            ]);
            $user_locked->notify(new SendNotification($data['title'], $confirmed_amount / (config('point.relationship_amount') ?? 1) . " points has been added to your account."));
            return $user_locked;
        }, 5);
        (new SendNotificationAction())->execute([$user_locked->noti_token], $data);
        \Alert::add('success', 'Successfully Recharged')->flash();
        return redirect('admin/recharge-request');
    }
    public function reject(RechargeRequest $recharge_request)
    {
        $data = [
            "title" => "Recharge Request Rejected.",
            "message" => "Your Recharge Request has been rejected!",
            'Type' => "Notification"
        ];
        $recharge_request->status = "Rejected";
        $recharge_request->save();
        $user = User::find($recharge_request->user_id);
        $user->notify(new SendNotification($data['title'], $data['message']));
        (new SendNotificationAction())->execute([$user->noti_token], $data);
        return response()->json([
            'message' => 'Successfully rejected.'
        ]);
    }
    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
