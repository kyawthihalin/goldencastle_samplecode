<?php

namespace App\Http\Controllers\Admin\Charts;

use App\Models\Agent;
use App\Models\State;
use App\Models\User;
use Backpack\CRUD\app\Http\Controllers\ChartController;
use ConsoleTVs\Charts\Classes\Chartjs\Chart;

/**
 * Class UserByStateChartController
 * @package App\Http\Controllers\Admin\Charts
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class UserByStateChartController extends ChartController
{
    public function setup()
    {
        $this->chart = new Chart();
        $labels = [];
        $states = State::all();
        foreach ($states as $state) {
            $labels[] = $state->name_en;
        }
        // MANDATORY. Set the labels for the dataset points
        $this->chart->labels($labels);
        // RECOMMENDED. Set URL that the ChartJS library should call, to get its data using AJAX.
        $this->chart->load(backpack_url('charts/user-by-state'));
        // OPTIONAL
        $this->chart->minimalist(false);
        $this->chart->displayLegend(true);
    }

    /**
     * Respond to AJAX calls with all the chart data points.
     *
     * @return json
     */
    public function data()
    {
        $states = State::all();
        $users = [];
        foreach ($states as $state) {
            $agents = Agent::where('state_id', $state->id)->pluck("id")->toArray();
            $users[] = User::whereIn("agent_id", $agents)->get()->count();
        }
        $color_one = get_random_color_set(39);
        $color_two = get_random_color_set(90);
        $this->chart->dataset('Users', 'line', $users)
            ->color('rgb(' . $color_one[0] . ',' . $color_one[1] . ',' . $color_one[2] . ')')
            ->backgroundColor('rgba(' . $color_two[0] . ',' . $color_two[1] . ',' . $color_two[2] . ',0.4)');
    }
}
