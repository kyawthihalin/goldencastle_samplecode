<?php

namespace App\Http\Controllers\Admin\Charts;

use App\Models\UserTicket;
use Backpack\CRUD\app\Http\Controllers\ChartController;
use ConsoleTVs\Charts\Classes\Chartjs\Chart;
use Illuminate\Support\Facades\DB;

/**
 * Class MonthlyIncomeChartController
 * @package App\Http\Controllers\Admin\Charts
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class MonthlyIncomeChartController extends ChartController
{
    public function setup()
    {
        $this->chart = new Chart();
        $labels = [];
        $months = ["December", "November", "October", "September", "August", "July", "June", "May", "April", "March", "February", "January"];

        foreach ($months as $month) {
            $labels[] = $month;
        }
        $this->chart->labels($labels);

        // RECOMMENDED. Set URL that the ChartJS library should call, to get its data using AJAX.
        $this->chart->load(backpack_url('charts/monthly-income'));

        // OPTIONAL
        // $this->chart->minimalist(false);
        // $this->chart->displayLegend(true);
    }

    /**
     * Respond to AJAX calls with all the chart data points.
     *
     * @return json
     */
    public function data()
    {
        $monthly_data     = $this->getData();
        $monthly_income   = $this->format($monthly_data);
        $this->chart->dataset("Monthly Income", 'line', array_reverse($monthly_income))
            ->color('rgba(8, 168, 193, 0.91)')
            ->backgroundColor('rgba(113, 219, 235, 0.91)');
    }

    public function getData()
    {
        $query = DB::table('user_tickets')->whereYear('purchased_date', now()->format('Y'))->select(DB::raw("date_format(purchased_date, '%m') as month,sum(profit) as amount"));
        return $query->groupBy('month')->get();
    }

    public function format($montly_data)
    {
        $montly_datas     = [];
        if (count($montly_data) != 0) {
            foreach ($montly_data as $data) {
                $month = ltrim($data->month, '0');
                $montly_datas[$month - 1] = $data->amount;
            }
        }

        for ($i = 0; $i < 12; $i++) {
            if (array_key_exists($i, $montly_datas)) {
                $monthly_all_data[$i] = $montly_datas[$i];
            } else {
                $monthly_all_data[$i] = 0;
            }
        }

        return $monthly_all_data;
    }
}
