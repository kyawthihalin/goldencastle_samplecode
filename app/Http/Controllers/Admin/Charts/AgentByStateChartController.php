<?php

namespace App\Http\Controllers\Admin\Charts;

use App\Models\Agent;
use App\Models\State;
use Backpack\CRUD\app\Http\Controllers\ChartController;
use ConsoleTVs\Charts\Classes\Chartjs\Chart;

/**
 * Class AgentByStateChartController
 * @package App\Http\Controllers\Admin\Charts
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class AgentByStateChartController extends ChartController
{
    public function setup()
    {
        $this->chart = new Chart();
        $labels = [];
        $states = State::all();
        foreach ($states as $state) {
            $labels[] = $state->name_en;
        }
        // MANDATORY. Set the labels for the dataset points
        $this->chart->labels($labels);

        // RECOMMENDED. Set URL that the ChartJS library should call, to get its data using AJAX.
        $this->chart->load(backpack_url('charts/agent-by-state'));

        // OPTIONAL
        $this->chart->minimalist(false);
        $this->chart->displayLegend(true);
    }

    /**
     * Respond to AJAX calls with all the chart data points.
     *
     * @return json
     */
    public function data()
    {

        $states = State::all();
        foreach ($states as $state) {
            $agents[] = Agent::where('state_id', $state->id)->get()->count();
        }

        $this->chart->dataset('Agents', 'line', $agents)
            ->color('rgb(96, 92, 168)')
            ->backgroundColor('rgba(96, 92, 168, 0.4)');
    }
}
