<?php

namespace App\Http\Controllers\Admin\Charts;

use App\Models\UserTicket;
use Backpack\CRUD\app\Http\Controllers\ChartController;
use Carbon\Carbon;
use ConsoleTVs\Charts\Classes\Chartjs\Chart;

/**
 * Class IncomeByCurrentDrawChartController
 * @package App\Http\Controllers\Admin\Charts
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class IncomeByCurrentDrawChartController extends ChartController
{
    public function setup()
    {
        $this->chart = new Chart();

        $this->chart->labels($this->getAllDatesForThisMonth());
        $this->chart->load(backpack_url('charts/income-by-current-draw'));
        $this->chart->minimalist(false);
        $this->chart->displayLegend(true);
    }

    /**
     * Respond to AJAX calls with all the chart data points.
     *
     * @return json
     */
    public function data()
    {
        $incomes = [];
        foreach ($this->getAllDatesForThisMonth() as $date) {
            // $incomes[] = UserTicket::where("date", $date)->sum("profit");
            $incomes[] = UserTicket::whereDate("purchased_date", $date)->sum("profit");
        }

        $this->chart->dataset('Income', 'line', $incomes)
            ->color('rgb(77, 189, 116)')
            ->backgroundColor('rgba(77, 189, 116, 0.4)');
    }

    private function getAllDatesForThisMonth(): array
    {
        $dates = [];
        $currentMonth = now()->month;
        $currentYear = now()->year;
        $daysInMonth = now()->daysInMonth;

        for ($i = 1; $i <= $daysInMonth; $i++) {
            $dates[] = $currentYear . '-' . $currentMonth . '-' . $i;
        }

        return $dates;
    }
}
