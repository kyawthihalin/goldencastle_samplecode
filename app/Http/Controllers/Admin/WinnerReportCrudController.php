<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\WinnerReportRequest;
use App\Models\WinnerReport;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Http\Request;
use App\Actions\SetCrudPermission;
/**
 * Class WinnerReportCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class WinnerReportCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\WinnerReport::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/winner-report');
        CRUD::setEntityNameStrings('winner report', 'winner reports');
        (new SetCrudPermission())->execute('winner', $this->crud);
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {


        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(WinnerReportRequest::class);

        $this->crud->addField(
            [   // select_from_array
                'name'        => 'draw_id',
                'label'       => "Draw Id",
                'type'        => 'select_from_array',
                'options'     => get_draw_id(),
                'allows_null' => false,
                'wrapperAttributes' => [
                    'class' => 'form-group col-md-12 Drawid'
                ]
            ],
        );
        $this->crud->addField([
            'name' => 'custom-ajax-button',
            'type' => 'view',
            'view' => 'partials/reports/winner'
        ]);

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number'])); 
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
    public function report(Request $request)
    {
        $draw_id   = $request->draw_id;
        $winners  = WinnerReport::where('draw_id', '=', $draw_id)->get();
        return view('partials.reports.showwinner', compact('winners'))->render();
    }
}
