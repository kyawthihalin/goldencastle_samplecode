<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\IncomeReportRequest;
use App\Models\IncomeReport;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Http\Request;
use App\Actions\SetCrudPermission;
use Illuminate\Support\Facades\DB;

/**
 * Class IncomeReportCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class IncomeReportCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\IncomeReport::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/income-report');
        CRUD::setEntityNameStrings('income report', 'income reports');
        (new SetCrudPermission())->execute('income', $this->crud);
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {


        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(IncomeReportRequest::class);
        $this->crud->addField(
            [   // date_range
                'name'  => 'Sdate',
                'type'  => 'date_picker',
                'label' => 'Start Date',
                // optional:
                'date_picker_options' => [
                    'todayBtn' => 'linked',
                    'format'   => 'dd-mm-yyyy',
                    'language' => 'mm'
                ],
                'wrapperAttributes' => [
                    'class' => 'form-group col-md-6 SDate'
                ]
            ],
        );
        $this->crud->addField(
            [   // date_range
                'name'  => 'Edate',
                'type'  => 'date_picker',
                'label' => 'End Date',
                // optional:
                'date_picker_options' => [
                    'todayBtn' => 'linked',
                    'format'   => 'dd-mm-yyyy',
                    'language' => 'mm'
                ],
                'wrapperAttributes' => [
                    'class' => 'form-group col-md-6 EDate'
                ]
            ],
        );
        $this->crud->addField([
            'name' => 'custom-ajax-button',
            'type' => 'view',
            'view' => 'partials/reports/income'
        ]);


        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number'])); 
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function report(Request $request)
    {
        $startDate = $request->start_date;
        $endDate   = $request->end_date;
        $incomes  = IncomeReport::where('purchased_date', '>=', $startDate)->where('purchased_date', '<=', $endDate)->get();
        $reports = IncomeReport::where('purchased_date', '>=', $startDate)->where('purchased_date', '<=', $endDate)->select(DB::raw('sum(amount) as total_price'), DB::raw('sum(commission) as total_commission'), DB::raw('sum(profit) as total_profit'))->first();

        $total_price = $reports->total_price;
        $total_commission = $reports->total_commission;
        $total_profit = $reports->total_profit;
        return view('partials.reports.showincome', compact('incomes', 'total_profit', 'total_commission', 'total_price'))->render();
    }
}
