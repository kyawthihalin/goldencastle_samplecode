<?php

namespace App\Http\Controllers\Admin;

use App\Actions\SetCrudPermission;
use App\Http\Requests\ResultTypeRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class ResultTypeCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ResultTypeCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\ResultType::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/result-type');
        CRUD::setEntityNameStrings('result type', 'result types');
        CRUD::denyAccess(["create","delete","show"]);
        (new SetCrudPermission())->execute('result_type', $this->crud);
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->enableExportButtons();
        CRUD::column('name');
        CRUD::column('amount');
        CRUD::addColumn([
            'name' => 'admin',
            'type' => 'relationship',
            'label' => 'Updated by',
        ]);

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(ResultTypeRequest::class);
        CRUD::addField([
            "name" => "name",
            "label" => "Result Name",
            "type" => "text",
            'attributes' => [
                'placeholder' => 'Result Type Name',
                'class'       => 'form-control',
                'readonly'    => 'readonly',
            ], // change the HTML attributes of your input
        ]);
        CRUD::addField([
            "name" => "amount",
            "label" => "Result Amount",
            "type" => "number",
        ]);

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number'])); 
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

}
