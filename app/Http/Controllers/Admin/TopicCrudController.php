<?php

namespace App\Http\Controllers\Admin;

use App\Actions\CreateTopic;
use App\Actions\RemoveTopic;
use App\Http\Requests\TopicRequest;
use App\Models\Agent;
use App\Models\Topic;
use App\Models\User;
use App\Services\GetUser;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class TopicCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class TopicCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Topic::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/topic');
        CRUD::setEntityNameStrings('topic', 'topics');
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::denyAccess([
            "update",
            "show"
        ]);
        CRUD::addColumn([
            'name' => 'topic_name',
            'type' => 'closure',
            'label' => "Topic Name",
            'function' => function ($entry) {
                if ($entry->topic_name == "User" || $entry->topic_name == "Agent") {
                    CRUD::denyAccess([
                        "update",
                        "delete",
                        "show"
                    ]);
                }
                return $entry->topic_name;
            }
        ]);
        CRUD::column('created_at');
        CRUD::column('updated_at');
        $this->crud->enableExportButtons();

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(TopicRequest::class);

        CRUD::field('topic_name');
        CRUD::addField([
            'name' => 'agents',
            'type' => 'select2_from_array',
            'allows_multiple' => true,
            'options'     => (new GetUser())->getAgent(),
        ]);

        CRUD::addField([
            'name' => 'users',
            'type' => 'select2_from_array',
            'allows_multiple' => true,
            'options'     => (new GetUser())->getUser(),
        ]);
        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number'])); 
         */
    }

    public function store(TopicRequest $request)
    {
        $topic_name = $request->topic_name;
        $agents = [];
        $users = [];
        if (count($request->agents) > 0) {
            $agents = $request->agents;
        }
        if (count($request->users) > 0) {
            $users = $request->users;
        }
        Topic::create([
            "topic_name" => $topic_name
        ]);
        $user_tokens = User::whereIn('id', $users)->pluck('noti_token')->toArray();
        $agent_tokens = Agent::whereIn('id', $agents)->pluck('noti_token')->toArray();
        $merge_tokens = array_merge($user_tokens, $agent_tokens);

        if (count($merge_tokens) > 0) {
            $remove_nulls = array_filter($merge_tokens, fn ($value) => !is_null($value) && $value !== '');
            (new CreateTopic())->execute($remove_nulls, "/topics/$topic_name");
        }
        \Alert::add('success', 'Successfully Topic Created')->flash();
        return redirect('admin/topic');
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
