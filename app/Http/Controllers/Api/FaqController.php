<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\FaqRequest;
use App\Http\Resources\Api\FaqCollection;
use App\Models\Faq;
use Illuminate\Http\Request;

class FaqController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(FaqRequest $request)
    {
        $faqs = Faq::where('language', $request->language)->get();

        return $this->responseWithSuccess('Success', [
            'faqs' => FaqCollection::make($faqs)
        ]);
    }
}
