<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\ChangeLanguageRequest;
use App\Models\User;
use Illuminate\Http\Request;

class ChangeLanguageController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(ChangeLanguageRequest $request)
    {
        $user = auth()->user();
        $user->language = $request->language;
        $user->save();
        return $this->responseWithSuccess();
    }
}
