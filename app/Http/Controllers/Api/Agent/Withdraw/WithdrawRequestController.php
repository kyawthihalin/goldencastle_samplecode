<?php

namespace App\Http\Controllers\Api\Agent\Withdraw;

use App\Http\Controllers\Controller;
use App\Models\WithdrawOption;
use App\Http\Resources\Api\User\WithdrawOptionCollection;
use App\Models\Agent;
use App\Http\Resources\Api\User\WithdrawRequestCollection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Exceptions\PointNotEnoughException;
use Illuminate\Support\Str;
use App\Actions\GenerateReference;
use App\Http\Requests\Api\User\WithdrawRequest;
use App\Services\Telegram;
use App\Actions\SendNotification as SendNotificationAction;
use App\Notifications\SendNotification;

class WithdrawRequestController extends Controller
{
    public function options()
    {
        $options = WithdrawOption::all();

        return $this->responseWithSuccess('Success', [
            'options' => new WithdrawOptionCollection($options)
        ]);
    }

    public function withdraw(WithdrawRequest $request)
    {
        $user = auth()->user();
        if (!Hash::check($request->password, $user->password)) {
            return $this->responseWithSuccess(__('auth.failed_api.password'), [], 401);
        }


        DB::transaction(function () use ($request) {
            $user_locked = Agent::lockForUpdate()->find(auth()->id());
            if ($user_locked->point < $request->point) {
                throw new PointNotEnoughException();
            }

            $point_before = $user_locked->point;
            $user_locked->point = $point_before - $request->point;
            $user_locked->save();

            $withdraw_option = WithdrawOption::whereName($request->option)->first();

            $created_request = $user_locked->agent_withdraw_requests()->create([
                'withdraw_option_id' => $withdraw_option->id,
                'reference_id' => Str::uuid(),
                'payee' => $request->payee,
                'account_number' => $request->account_number,
                'point' => $request->point,
            ]);

            $created_request->refresh();
            $url = null;
            if ($request->has('qr_code')) {
                $file = $request->file('qr_code');
                $file->storeAs(
                    'Withdraw/Requests/' . $created_request->id,
                    'qr_code.' . $file->extension(),
                    'public'
                );

                $created_request->update([
                    'qr_code' => 'qr_code.' . $file->extension(),
                ]);

                $url = url('/') . '/' . 'storage/Withdraw/Requests/' . $created_request->id . '/' .
                    'qr_code.' . $file->extension();
            }

            $created_request->update([
                'reference_id' => (new GenerateReference())->execute('AGWR', $created_request->sequence),
            ]);
            $data = [
                "title" => "Withdraw Request",
                "message" => "Your Withdraw Request has been requested!",
                'Type' => "Notification"
            ];
            $user_locked->notify(new SendNotification($data['title'], $data['message']));
            (new SendNotificationAction())->execute([$user_locked->noti_token], $data);

            (new Telegram(config('services.telegram.chat_ids.withdraw_agent')))->sendMessage(
                "Withdrawal For {$user_locked->name} | @{$user_locked->user_name}
Option - {$withdraw_option->name}
Payee - {$created_request->payee}
Account Number - {$created_request->account_number}
                    ",
                $url,
            );
        }, 5);

        return $this->responseWithSuccess(__('withdraw.requested'));
    }

    public function records()
    {
        $user = Agent::find(auth()->id());

        return $this->responseWithSuccess('Success', [
            'records' => new WithdrawRequestCollection($user->agent_withdraw_requests()->orderBy('created_at', 'desc')->paginate(10)),
        ]);
    }
}
