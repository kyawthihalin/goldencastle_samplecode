<?php

namespace App\Http\Controllers\Api\Agent\Auth;

use App\Actions\Auth\SucceedLogin;
use App\Actions\PasswordGrant\AccessToken;
use App\Exceptions\AccountLockedException;
use App\Exceptions\AgentPasswordInvalidException;
use App\Exceptions\PasswordInvalidException;
use App\Exceptions\UserNotExistException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Agent\Auth\LoginRequest;
use App\Http\Resources\Api\Agent\AgentResource;
use App\Models\Agent;
use App\Services\Crypto;

class LoginController extends Controller
{
    public function __invoke(LoginRequest $request)
    {
        $agent_with_users = Agent::whereCode($request->code)->with('users')->first();
        $agent = Agent::whereCode($request->code)->first();
        if (!$agent_with_users) {
            throw new UserNotExistException(trans('auth.failed_api.agent'));
        }

        if ($agent_with_users->frozen_at){
            throw new AccountLockedException();
        }

        $response = (new AccessToken($agent_with_users))->execute(
            $request->code,
            $request->password
        );

        if ($response->failed()) {

            $error_arr = $response->json();

            if ($error_arr['error'] === 'invalid_grant') {
                throw new AgentPasswordInvalidException();
            }
            abort(500);
        }

        $results = $response->json();
        $results['agent'] = new AgentResource($agent_with_users);
        $results['secret_key'] = (new Crypto())->generateKey($agent_with_users);
        // dd($tokens);
        (new SucceedLogin())->handle($agent, $request);
        return $this->responseWithSuccess('Success', $results);
    }
}
