<?php

namespace App\Http\Controllers\Api\Agent;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Exceptions\AccountLockedException;
use App\Http\Resources\Api\User\UserResource;

class UserListByAgentController extends Controller
{
    public function __invoke()
    {
        $users = User::whereAgentId(auth()->user()->id)->paginate(20);
        if (auth()->user()->frozen_at){
            throw new AccountLockedException();
        }
        
        return $this->responseWithSuccess('Success', [
            'users' => UserResource::collection($users),
            'current_page' => $users->currentPage(),
            'has_more_page' => $users->hasMorePages(),
            'last_page' => $users->lastPage(),
            'next_page' => $users->nextPageUrl(),
            'prev_page' => $users->previousPageUrl(),
            'total_count' => $users->total()
        ]);
    }
}
