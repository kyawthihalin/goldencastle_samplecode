<?php

namespace App\Http\Controllers\Api\Agent\Ticket;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\UserTicket;
use App\Http\Requests\Api\User\Ticket\PurchasedTicketRequest;
use App\Models\ExchangeRate;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\UserTicketsListCollection;
use App\Services\DrawIdService;

class UserTicketsListByAgentController extends Controller
{
    public function __invoke(PurchasedTicketRequest $request)
    {
        $paginate = 2;
        $page = $request->page ?? 1;
        $next = $page + 1;
        
        $users = User::whereAgentId(auth()->user()->id)->pluck('id')->toArray();

        $draw_ids = DB::table('results')
        ->orderBy('date', 'desc')
        ->select('date')->distinct()
        ->pluck('date')->toArray();

        array_unshift($draw_ids, (new DrawIdService())->getCurrentDrawId());
        
        $paginated_draw_ids = array_splice($draw_ids, $page * $paginate - 2, $paginate);
        $next_paginated_draw_ids = array_splice($draw_ids, $next * $paginate - 2, $paginate);
        
        $purchased_tickets = [];
        
        foreach ($paginated_draw_ids as $draw_id) {
            $tickets = UserTicket::whereIn('user_id',$users)->where('draw_id', $draw_id)->get();
            $collection = new UserTicketsListCollection($tickets);
            if($collection->count()){
                $purchased_tickets[$draw_id] = $collection;
            }
        }
        $exchange_rate = ExchangeRate::latest()->first();

        return $this->responseWithSuccess('Success', [
            'purchased_tickets' => count($purchased_tickets) ? $purchased_tickets : null,
            'next' => count($next_paginated_draw_ids) ? $next : null,
            'exchange_rate' => $exchange_rate->rate,
        ]);
    }
}
