<?php

namespace App\Http\Controllers\Api\Agent;

use App\Http\Controllers\Controller;
use App\Models\Agent;
use App\Http\Resources\Api\Agent\AgentResource;
use App\Http\Requests\Api\User\Profile\ProfileUpdateRequest;

class ProfileController extends Controller
{
    public function index()
    {
        $agent = Agent::find(auth()->id());

        return $this->responseWithSuccess('Success', [
            'agent' => new AgentResource($agent)
        ]);
    }

    public function update(ProfileUpdateRequest $request)
    {
        $agent = Agent::find(auth()->id());
        $phone = $agent->phone;
        if($request->has('phone')){
            $phone = $request->phone;
        }
        $agent->update([
            'name' => $request->name,
            'phone_number' => $phone
        ]);

        return $this->responseWithSuccess('Success', [
            'agent' => new AgentResource($agent)
        ]);
    }
}
