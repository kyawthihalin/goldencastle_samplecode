<?php

namespace App\Http\Controllers\Api\Agent;

use App\Http\Controllers\Controller;
use App\Models\TicketResult;
use App\Models\UserTicket;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class ResultController extends Controller
{
    public function winners()
    {
        $last_two_draw_ids = DB::table('results')
            ->orderBy('date', 'desc')
            ->select('date')->distinct()->limit(2)
            ->pluck('date')->toArray();

        $results = [];
        $users = auth()->user()->users()->pluck('id')->toArray();
        foreach ($last_two_draw_ids as $draw_id) {
            $user_tickets = UserTicket::whereIn("user_id",$users)->where('draw_id', Carbon::parse($draw_id))->pluck('id')->toArray();
            $ticket_results = TicketResult::whereIn('user_ticket_id',$user_tickets)->where('draw_id', Carbon::parse($draw_id))->get();
            $draw_results = [];
            foreach ($ticket_results as $ticket_result) {
                $user_ticket = $ticket_result->user_ticket;
                $result_type = $ticket_result->result_type->name;
                $user = $user_ticket->user;

                if (!isset($draw_results[$result_type])) {
                    $draw_results[$result_type][0] = [
                        'ticket_number' => $user_ticket->number,
                        'amount' => $ticket_result->amount,
                        'name' => $user->name,
                    ];
                    continue;
                }

                array_push($draw_results[$result_type], [
                    'ticket_number' => $user_ticket->number,
                    'amount' => $ticket_result->amount,
                    'name' => $user->name,
                ]);
            }
            $results[$draw_id] = $draw_results;
        }

        return $this->responseWithSuccess('Success', [
            'results' => $results
        ]);
    }
}
