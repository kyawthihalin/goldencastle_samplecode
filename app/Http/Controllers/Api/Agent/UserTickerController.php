<?php

namespace App\Http\Controllers\Api\Agent;

use App\Actions\GetTicketPhotoUrl;
use App\Http\Controllers\Controller;
use App\Models\AppFile;
use App\Models\UserTicket;
use App\Services\DrawIdService;

class UserTickerController extends Controller
{
    public function __invoke()
    {
        $draw_id = (new DrawIdService())->getCurrentDrawId();
        $user_ids = auth()->user()->users()->pluck('id')->toArray();

        $tickets = UserTicket::whereIn('user_id', $user_ids)->where('draw_id', $draw_id)->pluck('number')->toArray();
        $ticket_background_photo = AppFile::whereType('ticket background mobile')->pluck('file')->first();

        return $this->responseWithSuccess('Success', [
            'photo' => (new GetTicketPhotoUrl())->execute(),
            'ticket_background' => url('/') . '/' . $ticket_background_photo,
            'user_tickets' => $tickets,
        ]);
    }
}
