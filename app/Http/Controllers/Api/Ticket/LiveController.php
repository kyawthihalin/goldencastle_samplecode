<?php

namespace App\Http\Controllers\Api\Ticket;

use App\Actions\GetTicketPhotoUrl;
use App\Http\Controllers\Controller;
use App\Models\AppFile;
use App\Models\User;
use App\Models\UserTicket;
use App\Services\DrawIdService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PDO;

class LiveController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $user = User::find(auth()->id());

        $draw_id = (new DrawIdService())->getCurrentDrawId();
        $ticket_background_photo = AppFile::whereType('ticket background mobile')->pluck('file')->first();

        if ($draw_id === now()->format('Y-m-d')) {
            return $this->responseWithSuccess('Success', [
                'photo' => (new GetTicketPhotoUrl())->execute(),
                'ticket_background' => url('/') . '/' . $ticket_background_photo,
                'live_tickets' => [],
            ]);
        }

        $sold_tickets = DB::table('user_tickets')
            ->where("draw_id", Carbon::parse($draw_id))
            ->select('number', DB::raw('COUNT(*) as `count`'))
            ->groupBy('number')
            ->havingRaw('COUNT(*) > 1')
            ->where(function ($query) use ($user) {
                if ($user) {
                    $query->where('user_id', '!=', $user->id);
                }
            })
            ->pluck('number')->toArray();

        $live_tickets = UserTicket::where('draw_id', Carbon::parse($draw_id))->whereNotIn('number', $sold_tickets)->latest()->limit(5)->pluck('number')->toArray();

        if (count($live_tickets) < 5) { // if there is no live sale tickets
            $generate_count = 5 - count($live_tickets);

            while ($generate_count > 0) {
                $new_ticket = $this->generateNewTicket(array_merge($sold_tickets, $live_tickets));

                array_push($live_tickets, (string)$new_ticket);
                $generate_count--;
            }
        }

        return $this->responseWithSuccess('Success', [
            'photo' => (new GetTicketPhotoUrl())->execute(),
            'ticket_background' => url('/') . '/' . $ticket_background_photo,
            'live_tickets' => $live_tickets,
        ]);
    }

    private function generateNewTicket($except_tickets)
    {
        $new_ticket = rand(pow(10, 6 - 1), pow(10, 6) - 1);

        if (in_array($new_ticket, $except_tickets)) {
            $this->generateNewTicket($except_tickets);
        }

        return $new_ticket;
    }
}
