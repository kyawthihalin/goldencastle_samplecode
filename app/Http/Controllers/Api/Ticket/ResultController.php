<?php

namespace App\Http\Controllers\Api\Ticket;

use App\Actions\FindResult;
use App\Actions\GetTicketPhotoUrl;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\User\ResultWinnerRequest;
use App\Http\Requests\Api\User\Ticket\ResultCheckRequest;
use App\Http\Requests\Api\User\Ticket\ResultIndexRequest;
use App\Http\Resources\Api\ResultResource;
use App\Models\AppFile;
use App\Models\Result;
use App\Models\ResultType;
use App\Models\TicketResult;
use App\Services\DrawIdService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ResultController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ResultIndexRequest $request)
    {
        $result_types = ResultType::all();

        $results = [];
        foreach ($result_types as $result_type) {
            $result = $result_type->results()->where('date', Carbon::parse($request->draw_id))->first();

            if (!$result) {
                continue;
            }

            $results[$result_type->name] = new ResultResource($result_type->results()->where('date', Carbon::parse($request->draw_id))->first());
        }

        return $this->responseWithSuccess('Success', [
            'results' => $results
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function check(ResultCheckRequest $request)
    {
        if ($request->has('from_ticket')) {
            $from = ltrim($request->from_ticket, "0");
            $to = ltrim($request->to_ticket, "0");

            if ($from >= $to) {
                return $this->responseWithSuccess('To ticket number must be greater than from ticket number', [], 422);
            }
        }

        $results = Result::whereDate('date', $request->draw_id)->get();
        $result_types = ResultType::pluck('name', 'name')->toArray();
        $filtered_results = array_map(function ($result_type) {
            return [
                'type' => $result_type,
                'amount' => null,
                'tickets' => [],
            ];
        }, $result_types);

        // dd($filtered_results);

        foreach ($results as $result) {
            $filtered_results[$result->result_type->name]['amount'] = $result->amount;
            if ($request->has('ticket_number')) {
                $won_tickets = (new FindResult($result))->execute([$request->ticket_number]);

                if ($won_tickets) {
                    array_push($filtered_results[$result->result_type->name]['tickets'], ...$won_tickets);
                }
            }

            if ($request->has('ticket_numbers')) {
                $won_tickets = (new FindResult($result))->execute($request->ticket_numbers);

                if (count($won_tickets)) {
                    array_push($filtered_results[$result->result_type->name]['tickets'], ...$won_tickets);
                }
            }

            if ($request->has('from_ticket')) {
                $from = (int) ltrim($request->from_ticket, "0");
                $to = (int) ltrim($request->to_ticket, "0");

                while ($from < $to) {
                    $ticket_number = str_pad($from, 6, "0", STR_PAD_LEFT);

                    $won_tickets = (new FindResult($result))->execute([$ticket_number]);

                    if ($won_tickets) {
                        array_push($filtered_results[$result->result_type->name]['tickets'], ...$won_tickets);
                    }

                    $from++;
                }
            }

            $filtered_results[$result->result_type->name]['tickets'] = array_unique($filtered_results[$result->result_type->name]['tickets']);
        }

        foreach ($filtered_results as $key => $filtered_result) {
            if (!count($filtered_result['tickets'])) {
                unset($filtered_results[$key]);
            }
        }

        return $this->responseWithSuccess('Success', [
            'results' => array_values($filtered_results)
        ]);
    }

    public function winners(ResultWinnerRequest $request)
    {
        $last_two_draw_ids = DB::table('results')
            ->orderBy('date', 'desc')
            ->select('date')->distinct()->limit(2)
            ->pluck('date')->toArray();

        $results = [];

        foreach ($last_two_draw_ids as $draw_id) {
            $ticket_results = TicketResult::where('draw_id', Carbon::parse($draw_id))->get();
            $draw_results = [];
            foreach ($ticket_results as $ticket_result) {
                $user_ticket = $ticket_result->user_ticket;
                $result_type = $ticket_result->result_type->name;
                $user = $user_ticket->user;

                if (!isset($draw_results[$result_type])) {
                    $draw_results[$result_type][0] = [
                        'ticket_number' => $user_ticket->number,
                        'amount' => $ticket_result->amount,
                        'name' => $user->name,
                    ];
                    continue;
                }

                array_push($draw_results[$result_type], [
                    'ticket_number' => $user_ticket->number,
                    'amount' => $ticket_result->amount,
                    'name' => $user->name,
                ]);
            }
            $results[$draw_id] = $draw_results;
        }

        return $this->responseWithSuccess('Success', [
            'results' => $results
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function numbers(Request $request)
    {
        $last_draw = (new DrawIdService())->getPreviousDrawId();

        $results = Result::where('date', Carbon::parse($last_draw))->get();
        $numbers = [];

        foreach ($results as $result) {
            $result_type = ResultType::find($result->result_type_id);
            $prefix = '';
            $suffix = '';

            switch ($result_type->name) {
                case "first_three":
                    $prefix = '---';
                    break;
                case "last_three":
                    $suffix = '---';
                    break;
                case "last_two":
                    $prefix = '----';
                    break;
                default:
                    break;
            }
            $modifiedArray = array_map(function ($element) use ($prefix, $suffix) {
                return $prefix . $element . $suffix;
            }, $result->result);
            $numbers = [...$numbers, ...$modifiedArray];
        }
        $ticket_background_photo = AppFile::whereType('ticket background mobile')->pluck('file')->first();

        return $this->responseWithSuccess('Success', [
            'photo' => (new GetTicketPhotoUrl())->execute(),
            'ticket_background' => url('/') . '/' . $ticket_background_photo,
            'last_draw_tickets' => $numbers
        ]);
    }
}
