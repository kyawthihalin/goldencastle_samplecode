<?php

namespace App\Http\Controllers\Api\Ticket;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class DrawIdController extends Controller
{
    public function __invoke()
    {
        $results = DB::table('results')
            ->orderBy('date', 'desc')
            ->select('date')->distinct()->limit(6)
            ->pluck('date')->toArray();

        return $this->responseWithSuccess('Success', [
            'draw_ids' => $results
        ]);
    }
}
