<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\User\FeedbackRequest;
use App\Models\User;

class FeedbackController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(FeedbackRequest $request)
    {
        $user = User::find(auth()->id());

        $user->feedbacks()->create([
            'title' => $request->title,
            'detail' => $request->detail
        ]);

        return $this->responseWithSuccess(__('global.feedback.submitted'));
    }
}
