<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Models\Banner;
use App\Models\ExchangeRate;
use Illuminate\Http\Request;

class BannerController extends Controller
{
    public function __invoke(Request $request)
    {
        $banner = Banner::whereType('app')->first();

        $paths = json_decode($banner->images);

        if (!count($paths)) {
            return $this->responseWithSuccess('No banner photos.', [], 404);
        }

        $urls = [];

        foreach ($paths as $path) {
            $modified_path = str_replace('\\', '/', $path);
            array_push($urls, url('/') . '/' . $modified_path);
        }
        
        $exchange_rate = ExchangeRate::latest()->first();

        return $this->responseWithSuccess('Success', [
            'exchange_rate' => optional($exchange_rate)->rate,
            'banner_urls' => $urls
        ]);
    }
}
