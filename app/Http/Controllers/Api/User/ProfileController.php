<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\User\Profile\AccountDeletion as ProfileAccountDeletion;
use App\Http\Requests\Api\User\Profile\ProfileIndexRequest;
use App\Http\Requests\Api\User\Profile\ProfileUpdateRequest;
use App\Http\Resources\Api\User\UserResource;
use App\Models\AccountDeletion;
use App\Models\User;

class ProfileController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(ProfileIndexRequest $request)
    {
        $user = User::find(auth()->id());

        return $this->responseWithSuccess('Success', [
            'user' => new UserResource($user)
        ]);
    }

    public function update(ProfileUpdateRequest $request)
    {
        // dd($request->all());
        $user = User::find(auth()->id());
        $user->update([
            'name' => $request->name,
            'phone_number' => $request->phone_number,
            'nrc' => $request->nrc,
        ]);

        return $this->responseWithSuccess('Success', [
            'user' => new UserResource($user)
        ]);
    }

    public function deleteRequest(ProfileAccountDeletion $request)
    {
        $account_deletion = new AccountDeletion();
        $account_deletion->create([
            'user_id' => auth()->id(),
            'reason' => $request->reason,
        ]);
        return $this->responseWithSuccess('Successfully Requested');
    }
}
