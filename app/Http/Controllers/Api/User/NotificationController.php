<?php

namespace App\Http\Controllers\Api\User;

use App\Actions\FindResult;
use App\Http\Controllers\Controller;
use App\Http\Resources\Api\User\NotificationResource;
use App\Models\Notification;
use App\Models\Result;
use App\Models\ResultType;
use App\Models\User;
use App\Services\DrawIdService;

class NotificationController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();
        // $page

        $notifications = $user->user_notifications()->orderBy('created_at', 'desc')->paginate(20);
        $ordered_notifications = [];

        foreach ($notifications as $notification) {
            $date = $notification->created_at->format('Y-m-d');

            if ($date === now()->format('Y-m-d')) {
                $date = 'Today';
            } else if ($date === now()->subDay()->format('Y-m-d')) {
                $date = 'Yesterday';
            }
            $data = new NotificationResource($notification);

            if (!count($ordered_notifications)) {
                array_push($ordered_notifications, [
                    'date' => $date,
                    'data' => [
                        $data
                    ]
                ]);
                continue;
            }

            $has = false;
            foreach ($ordered_notifications as $key => $ordered_notification) {
                if ($ordered_notification['date'] === $date) {
                    array_push($ordered_notifications[$key]['data'], $data);
                    $has = true;
                    break;
                }
            }

            if (!$has) {
                array_push($ordered_notifications, [
                    'date' => $date,
                    'data' => [$data]
                ]);
            }
        }

        // dd(array_unique($ordered_notificaitons));

        return $this->responseWithSuccess('Success', [
            'notifications' => $ordered_notifications
        ]);
    }

    public function read(Notification $notification)
    {
        $notification->update([
            'read_at' => now(),
        ]);

        return $this->responseWithSuccess();
    }

    public function readAll()
    {
        auth()->user()->user_notifications()->update([
            'read_at' => now()
        ]);

        return $this->responseWithSuccess();
    }

    public function won()
    {
        $user = User::find(auth()->id());
        // $page
        $has_won = $user->user_notifications()
            ->whereJsonContains('data', ['title' => 'Congratulations'])->whereNull('read_at')->first();

        if ($has_won) {
            $last_draw_id = (new DrawIdService())->getPreviousDrawId();
            $ticket_numbers = $user->tickets()->whereHas('ticket_results')->where('draw_id', $last_draw_id)->pluck('number')->toArray();

            $results = Result::where('date', $last_draw_id)->get();
            $result_types = ResultType::pluck('name', 'name')->toArray();

            $filtered_results = array_map(function ($result_type) {
                return [
                    'type' => $result_type,
                    'amount' => null,
                    'tickets' => [],
                ];
            }, $result_types);

            foreach ($results as $result) {
                $filtered_results[$result->result_type->name]['amount'] = $result->amount;
                $won_tickets = (new FindResult($result))->execute($ticket_numbers);

                if (count($won_tickets)) {
                    array_push($filtered_results[$result->result_type->name]['tickets'], ...$won_tickets);
                }

                $filtered_results[$result->result_type->name]['tickets'] = array_unique($filtered_results[$result->result_type->name]['tickets']);
            }

            foreach ($filtered_results as $key => $filtered_result) {
                if (!count($filtered_result['tickets'])) {
                    unset($filtered_results[$key]);
                }
            }

            return $this->responseWithSuccess('Success', [
                'results' => array_values($filtered_results)
            ]);
        }

        return $this->responseWithSuccess('Success', [
            'results' => null,
        ]);
    }

    public function readWon()
    {
        $user = User::find(auth()->id());
        // $page
        $has_won = $user->user_notifications()
            ->whereJsonContains('data', ['title' => 'Congratulations'])->whereNull('read_at')->first();

        if ($has_won) {
            $has_won->update([
                'read_at' => now(),
            ]);
        }

        return $this->responseWithSuccess();
    }
}
