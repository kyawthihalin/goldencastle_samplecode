<?php

namespace App\Http\Controllers\Api\User\Auth;

use App\Actions\Auth\SucceedLogin;
use App\Actions\PasswordGrant\AccessToken;
use App\Exceptions\AccountLockedException;
use App\Exceptions\PasswordInvalidException;
use App\Exceptions\UserNotExistException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\User\Auth\LoginRequest;
use App\Http\Resources\Api\User\UserResource;
use App\Models\ExchangeRate;
use App\Models\User;
use App\Services\Crypto;

class LoginController extends Controller
{
    public function __invoke(LoginRequest $request)
    {
        $user = User::whereUserName($request->user_name)->first();

        if (!$user) {
            throw new UserNotExistException(trans('auth.failed_api.user'));
        }

        if ($user->frozen_at) {
            throw new AccountLockedException();
        }

        $response = (new AccessToken($user))->execute(
            $request->user_name,
            $request->password
        );

        if ($response->failed()) {

            $error_arr = $response->json();

            if ($error_arr['error'] === 'invalid_grant') {
                throw new PasswordInvalidException();
            }
            abort(500);
        }
        $exchange_rate = ExchangeRate::latest()->first();

        $results = $response->json();
        $results['user'] = new UserResource($user);
        $results['exchange_rate'] = optional($exchange_rate)->rate;
        $results['secret_key'] = (new Crypto())->generateKey($user);
        // dd($tokens);
        (new SucceedLogin())->handle($user, $request);

        return $this->responseWithSuccess('Success', $results);
    }
}
