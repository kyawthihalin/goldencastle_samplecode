<?php

namespace App\Http\Controllers\Api\User\Auth;

use App\Actions\Auth\SucceedLogin;
use App\Actions\PasswordGrant\AccessToken;
use App\Exceptions\AgentCodeNotExistException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\User\Auth\RegisterRequest;
use App\Http\Resources\Api\User\UserResource;
use App\Models\Agent;
use App\Models\ExchangeRate;
use App\Models\User;
use App\Services\Crypto;

class RegisterController extends Controller
{
    public function __invoke(RegisterRequest $request)
    {
        $agent = Agent::whereCode($request->agent_code)->first();

        if (!$agent) {
            throw new AgentCodeNotExistException();
        }

        $agent->users()->create([
            'name' => $request->name,
            'user_name' => $request->user_name,
            'password' => bcrypt($request->password),
            'reference_id' => $request->user_name,
            'device_name' => $request->device_name,
            'device_model' => $request->device_model,
            'os_version' => $request->os_version,
            'os_type' => $request->os_type,
            'app_version_id' => $request->app_version_id,
            'noti_token' => $request->noti_token,
            'language' => $request->language,
        ]);

        $user = User::whereUserName($request->user_name)->first();
        $response = (new AccessToken($user))->execute(
            $request->user_name,
            $request->password
        );

        if ($response->failed()) {
            abort(500);
        }
        $exchange_rate = ExchangeRate::latest()->first();

        $results = $response->json();
        $results['user'] = new UserResource($user);
        $results['exchange_rate'] = optional($exchange_rate)->rate;
        $results['secret_key'] = (new Crypto())->generateKey($user);
        (new SucceedLogin())->handle($user, $request);

        return $this->responseWithSuccess('Success', $results);
    }
}
