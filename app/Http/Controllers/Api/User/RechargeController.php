<?php

namespace App\Http\Controllers\Api\User;

use App\Actions\GenerateReference;
use App\Actions\SendNotification as ActionsSendNotification;
use App\Exceptions\RechargeAlreadyExistException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\User\RechargeRequest;
use App\Http\Resources\Api\User\AccountCollection;
use App\Http\Resources\Api\User\RechargeRequestCollection;
use App\Models\RechargeAccount;
use App\Models\User;
use App\Services\Telegram;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use App\Notifications\SendNotification;

class RechargeController extends Controller
{
    public function accounts()
    {
        $accounts = RechargeAccount::where('status', 'Active')->get();

        return $this->responseWithSuccess('Success', [
            'accounts' => new AccountCollection($accounts),
        ]);
    }

    public function records()
    {
        $user = User::find(auth()->id());

        return $this->responseWithSuccess('Success', [
            'point' => $user->point,
            'records' => new RechargeRequestCollection($user->recharge_requests()->orderBy('created_at', 'desc')->paginate(10)),
        ]);
    }

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function recharge(RechargeRequest $request)
    {
        $user = User::find(auth()->id());

        $exist = $user->recharge_requests()->where('status', 'Requested')->first();

        $account = RechargeAccount::find($request->account_id);

        if (!$account) {
            abort(404);
        }

        if ($exist) {
            throw new RechargeAlreadyExistException();
        }

        $recharge_request = DB::transaction(function () use ($user, $account, $request) {
            $recharge_request = $user->recharge_requests()->create([
                'reference_id' => Str::uuid(),
                'recharge_account_id' => $account->id,
            ]);

            $recharge_request->refresh();
            $recharge_request->update([
                'reference_id' => (new GenerateReference())->execute('RC', $recharge_request->sequence),
            ]);

            $file = $request->file('screenshot');
            $file->storeAs(
                'Recharge/Requests/' . $recharge_request->id,
                'screenshot.' . $file->extension(),
                'public'
            );

            $recharge_request->update([
                'screenshot' => 'screenshot.' . $file->extension(),
            ]);
            $data = [
                "title" => "Recharge Request",
                "message" => "Your Recharge Request has been requested!",
                'Type' => "Notification"
            ];
            (new Telegram(config('services.telegram.chat_ids.recharge')))->sendMessage(
                "Recharge For {$user->name} | @{$user->user_name}
Reference Id - {$recharge_request->reference_id}
Recharge Account - {$recharge_request->account->name}
Date - {$recharge_request->created_at}",
                url('/') . '/' . 'storage/Recharge/Requests/' . $recharge_request->id . '/' .
                    $recharge_request->screenshot,
            );

            (new ActionsSendNotification())->execute([$user->noti_token], [
                'Type' => 'Notification',
                'title' => $data['title'],
                'message' => $data['message'],
            ]);

            $user->notify(new SendNotification($data['title'], $data['message']));

            return $recharge_request;
        }, 5);

        return $this->responseWithSuccess(__('recharge.requested'));
    }
}
