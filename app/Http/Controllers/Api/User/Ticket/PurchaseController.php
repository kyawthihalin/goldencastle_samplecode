<?php

namespace App\Http\Controllers\Api\User\Ticket;

use App\Actions\GenerateReference;
use App\Actions\SendNotification as ActionsSendNotification;
use App\Exceptions\AccountLockedException;
use App\Exceptions\PointNotEnoughException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\User\Ticket\PurchaseRequest;
use App\Models\Agent;
use App\Models\AgentCommission;
use App\Models\TicketPrice;
use App\Models\User;
use App\Models\UserTicket;
use App\Notifications\SendNotification;
use App\Services\DrawIdService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class PurchaseController extends Controller
{
    public function __invoke(PurchaseRequest $request)
    {
        $user = User::find(auth()->id());
        if ($user->frozen_at) {
            throw new AccountLockedException();
        }

        $ticket_numbers = $request->ticket_numbers;

        $draw_id = (new DrawIdService())->getCurrentDrawId();

        if ($draw_id === now()->format('Y-m-d')) {
            return $this->responseWithError(__('global.purchase.lottery_day'), [], 400);
        }

        $purchased_tickets = [];
        foreach ($ticket_numbers as $ticket_number) {
            $user_tickets = UserTicket::where("number", $ticket_number)->where("draw_id", $draw_id)->get()->count();
            if ($user_tickets >= 2) {
                array_push($purchased_tickets, $ticket_number);
            }
        }

        if (count($purchased_tickets)) {
            return $this->responseWithError(__('ticket.already_purchased'), [
                'purchased_tickets' => $purchased_tickets
            ], 422);
        }

        DB::transaction(function () use ($request, $user, $draw_id) {
            $ticket = TicketPrice::first();
            $user_locked = User::lockForUpdate()->find($user->id);

            if ($user_locked->point < ($ticket->price * count($request->ticket_numbers))) {
                throw new PointNotEnoughException();
            }

            $agent_commission = AgentCommission::first();

            foreach ($request->ticket_numbers as $ticket_number) {

                $created_ticket = $user_locked->tickets()->create([
                    'reference_id' => Str::uuid(),
                    'number' => $ticket_number,
                    'purchased_date' => now(),
                    'amount' => $ticket->price,
                    'profit' => (float) bcsub($ticket->price, $agent_commission->point, 4),
                    'commission' => $agent_commission->point,
                    'draw_id' => $draw_id
                ]);

                $created_ticket->refresh();
                $created_ticket->update([
                    'reference_id' => (new GenerateReference())->execute('PT', $created_ticket->sequence),
                ]);
            }

            $before_point = $user_locked->point;
            $user_locked->update([
                'point' => $before_point - ($ticket->price * count($request->ticket_numbers)),
            ]);

            $commission_amount = $agent_commission->point * count($request->ticket_numbers);

            $agent_locked = Agent::lockForUpdate()->find($user_locked->agent_id);

            $before_amount_agent = $agent_locked->point;
            $after_amount_agent = $before_amount_agent + $commission_amount;

            $agent_locked->update([
                'point' => $after_amount_agent
            ]);
        }, 5);

        $tickets_string = implode(', ', $request->ticket_numbers);

        $title = __("ticket.notification.title.purchased");
        $message = __("ticket.notification.message.purchased", [
            'tickets' => $tickets_string
        ]);

        (new ActionsSendNotification())->execute([$user->noti_token], [
            'Type' => 'Notification',
            'title' => $title,
            'message' => $message,
        ]);

        $user->notify(new SendNotification($title, $message));

        $message = __("ticket.notification.message.purchased_from", [
            'name' => $user->name,
            'tickets' => $tickets_string,
        ]);
        $user->agent->notify(new SendNotification($title, $message));

        (new ActionsSendNotification())->execute([$user->agent->noti_token], [
            'Type' => 'Notification',
            'title' => $title,
            'message' => $message,
        ]);

        return $this->responseWithSuccess(__('ticket.purchased'));
    }
}
