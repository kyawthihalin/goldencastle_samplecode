<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\AppContact;
use Illuminate\Http\Request;

class ContactUsController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $phone_number = AppContact::where('status', 1)->whereType('Phone Number')->pluck('link')->first();
        $email = AppContact::where('status', 1)->whereType('Email')->pluck('link')->first();

        return $this->responseWithSuccess('Success', [
            'phone' => $phone_number,
            'email' => $email,
        ]);
    }
}
