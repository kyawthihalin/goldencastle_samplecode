<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\DrawIdService;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CountDownController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $current_draw = Carbon::parse((new DrawIdService())->getCurrentDrawId());
        $now = now();
        $new_noti_count = 0;

        return $this->responseWithSuccess('Success', [
            'day_diff' => $current_draw->diffInDays($now),
            'current_draw' => $current_draw->format('Y-m-d'),
            'new_noti_count' => $new_noti_count,
        ]);
    }
}
