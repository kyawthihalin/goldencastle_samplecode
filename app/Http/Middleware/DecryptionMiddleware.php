<?php

namespace App\Http\Middleware;

use App\Actions\HiddenValidation;
use App\Services\Crypto;
use Closure;
use Illuminate\Http\Request;

class DecryptionMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if (!config('app.crypto')) {
            return $next($request);
        }

        (new HiddenValidation())->handle([
            'data' => ['string']
        ]);

        if (!$request->has('data')) {
            $request->replace([]);
            return $next($request);
        }

        $data = (new Crypto())->decrypt($request->data);
        $request->replace($data);

        return $next($request);
    }
}
