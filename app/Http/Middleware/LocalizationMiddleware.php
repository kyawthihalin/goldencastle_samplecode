<?php

namespace App\Http\Middleware;

use App\Actions\HiddenValidation;
use Closure;
use Illuminate\Http\Request;

class LocalizationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $language = 'en';

        if(auth()->user()){
            $language = auth()->user()->language;
        }
        else {
            (new HiddenValidation)->handle([
                'language' => ['required', 'string', "in:en,mm,th"]
            ]);
            $language = $request->language;
        }

        app()->setLocale($language);
        return $next($request);
    }
}
