<?php

namespace App\Models;

use App\Traits\Uuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RechargeAccount extends Model
{
    use \Backpack\CRUD\app\Models\Traits\CrudTrait;
    use HasFactory, Uuid;

    protected $guarded = ['id'];
    protected $table = "recharge_accounts";
    
    public function requests()
    {
        return $this->hasMany(RechargeRequest::class, 'recharge_account_id');
    }
}
