<?php

namespace App\Models;

use App\Traits\Uuid;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;

use function PHPSTORM_META\map;

class Agent extends Authenticatable
{
    use CrudTrait, HasApiTokens, Uuid, HasFactory, Notifiable;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'agents';
    protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'code',
        'password',
        'nrc',
        'device_name',
        'device_model',
        'os_version',
        'os_type',
        'app_version_id',
        'noti_token',
        'language',
        'state_id',
        'admin_id',
        'nrc_front',
        'nrc_back',
        'phone_number',
        'frozen_at',
        'point'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
        'device_name',
        'device_model',
        'os_version',
        'os_type',
        'app_version_id',
        'noti_token',
        'language',
        'point',
        'frozen_at',
    ];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function findForPassport($username)
    {
        return $this->where('code', $username)->first();
    }
    // public static function boot()
    // {
    //     parent::boot();
    //     static::created(function($row){
    //         $agent = Agent::find($row->id);
    //         $state = State::find($row->state_id);
    //         $state_name = $state->name_en;
    //         $date = now();
    //         $agent_code = $state_name.$date->year;
    //         $agent->code = $agent_code;
    //         $agent->save();
    //     });
    // }


    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function users()
    {
        return $this->hasMany(User::class, 'agent_id');
    }

    public function admin()
    {
        return $this->belongsTo(Admin::class, 'admin_id');
    }

    public function state()
    {
        return $this->belongsTo(State::class, 'state_id');
    }
    public function user_notifications()
    {
        return $this->morphMany(Notification::class, 'notifiable');
    }
    public function agent_withdraw_requests()
    {
        return $this->hasMany(AgentWithdrawRequest::class, 'agent_id');
    }
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
