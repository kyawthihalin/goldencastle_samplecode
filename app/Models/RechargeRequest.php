<?php

namespace App\Models;

use App\Traits\Uuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RechargeRequest extends Model
{
    use \Backpack\CRUD\app\Models\Traits\CrudTrait;
    use HasFactory, Uuid;

    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function confirm_user()
    {
        return $this->belongsTo(Admin::class, 'confirmed_by');
    }

    public function account()
    {
        return $this->belongsTo(RechargeAccount::class, 'recharge_account_id');
    }

    public function recharge_account()
    {
        return $this->belongsTo(RechargeAccount::class, 'recharge_account_id');
    }
}
