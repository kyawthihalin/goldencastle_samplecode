<?php

namespace App\Models;

use App\Traits\Uuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Backpack\CRUD\app\Models\Traits\CrudTrait;

class User extends Authenticatable
{
    use CrudTrait, HasApiTokens, HasFactory, Notifiable, Uuid;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'user_name',
        'password',
        'reference_id',
        'device_name',
        'device_model',
        'os_version',
        'os_type',
        'app_version_id',
        'noti_token',
        'language',
        'phone_number',
        'nrc',
        'point'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
        'device_name',
        'device_model',
        'os_version',
        'os_type',
        'app_version_id',
        'noti_token',
        'language',
        'frozen_at',
        'password_mistake_count',
        'password_mistook_at'
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function findForPassport($username)
    {
        return $this->where('user_name', $username)->first();
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function agent()
    {
        return $this->belongsTo(Agent::class, 'agent_id');
    }

    public function tickets()
    {
        return $this->hasMany(UserTicket::class, 'user_id');
    }

    public function feedbacks()
    {
        return $this->hasMany(Feedback::class);
    }

    public function user_notifications()
    {
        return $this->morphMany(Notification::class, 'notifiable');
    }

    public function recharge_requests()
    {
        return $this->hasMany(RechargeRequest::class, 'user_id');
    }

    public function withdraw_requests()
    {
        return $this->hasMany(WithdrawRequest::class, 'user_id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    public function scopeFreeze($query)
    {
        return $query->whereNotNull('frozen_at');
    }

    public function scopeActive($query)
    {
        return $query->whereNull('frozen_at');
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */
    public function routeNotificationForFcm()
    {
        return $this->noti_token;
    }
    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
