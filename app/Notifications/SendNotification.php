<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notification;
use NotificationChannels\Fcm\FcmChannel;
use NotificationChannels\Fcm\FcmMessage;
use NotificationChannels\Fcm\Resources\AndroidConfig;
use NotificationChannels\Fcm\Resources\AndroidFcmOptions;
use NotificationChannels\Fcm\Resources\AndroidNotification;
use NotificationChannels\Fcm\Resources\ApnsConfig;
use NotificationChannels\Fcm\Resources\ApnsFcmOptions;

class SendNotification extends Notification
{
    protected $title;
    protected $message;

    public function __construct($title, $message)
    {
        $this->title = $title;
        $this->message = $message;
    }

    public function via($notifiable)
    {
        return ['database'];
    }

    // public function toFcm($notifiable)
    // {
    //     return FcmMessage::create()
    //         ->setNotification(\NotificationChannels\Fcm\Resources\Notification::create()
    //             ->setTitle($this->title)
    //             ->setBody($this->desc))
    //         ->setAndroid(
    //             AndroidConfig::create()
    //                 ->setFcmOptions(AndroidFcmOptions::create()->setAnalyticsLabel('analytics'))
    //                 ->setNotification(AndroidNotification::create()->setColor('#0A0A0A'))
    //         )->setApns(
    //             ApnsConfig::create()
    //                 ->setFcmOptions(ApnsFcmOptions::create()->setAnalyticsLabel('analytics_ios'))
    //         );
    // }

    // public function fcmProject($notifiable, $message)
    // {
    //     return 'pushno'; // name of the firebase project to use
    // }

    public function toDatabase($notifiable)
    {
        return [
            "title" => $this->title,
            "message" => $this->message,
        ];
    }
}
