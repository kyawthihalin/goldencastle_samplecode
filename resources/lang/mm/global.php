<?php
return [
    'banner' => [
        'empty' => 'There is no banner photo.'
    ],
    'feedback' => [
        'submitted' => 'Thanks for your feedback.'
    ],
    'point' => [
        'not_enough' => 'Your point not enought to purchase tickets. Please recharge and try again.',
    ],
    'purchase' => [
        'lottery_day' => 'Tickets cannot be purchased on lottery day.'
    ]
];
