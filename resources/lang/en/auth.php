<?php

use App\Constants\Lottery;

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed_api' => [
        'password' => 'Password is incorrect. Please try again.',
        'user' => 'Username or password is incorrect. Please try again.',
        'agent' => 'Agent code or password is incorrect. Please try again.',
    ],
    'failed' => 'Email or password is incorrect.',
    'invalid' => [
        'agent_code' => 'This agent code is invalid. Please try again.',
    ],
    'locked' => 'This account is locked. Please contact to call center, (' . Lottery::CALL_CENTER . ').',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

];
