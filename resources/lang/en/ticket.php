<?php
return [
    'notification' => [
        'title' => [
            'purchased' => 'Ticket Purchased',
        ],
        'message' => [
            'purchased' => 'You just bought tickets - :tickets',
            'purchased_from' => ':name just bought tickets - :tickets',
        ]
    ],
    'already_purchased' => 'Ticket already purchased, Please choose another number.',
    'purchased' => 'Ticket purchased successfully.',
    'and' => 'and',
    'first' => '1st',
    'second' => '2nd',
    'third' => '3rd',
    'forth' => '4th',
    'fifth' => '5th',
    'closest_first' => 'closet first',
    'first_three' => 'first three digits',
    'last_three' => 'last three digits',
    'last_two' => 'last two digits',
    'won' => [
        'title' => 'Congratulations',
        'message' => 'You have won'
    ]
];
