<?php
return [
    'banner' => [
        'empty' => 'There is no banner photo.'
    ],
    'feedback' => [
        'submitted' => 'Thanks for your feedback.'
    ],
    'point' => [
        'not_enough' => 'Your point not enought.Please try again later.',
    ],
    'purchase' => [
        'lottery_day' => 'Tickets cannot be purchased on lottery day.'
    ]
];
