<?php

return [
    'failed' => [
        'already_exist' => 'Recharge request already exist for this account. Please try again after confirmed by customer support.'
    ],
    'requested' => 'Recharge requested, please wait for confirmation by customer support.'
];
