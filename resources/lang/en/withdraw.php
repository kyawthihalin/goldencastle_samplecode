<?php

return [
    'requested' => 'Withdraw requested, please wait for confirmation by customer support.',
    'validation' => [
        'point' => [
            'min' => 'You can only withdraw over :point point.',
            'integer' => 'Point must be in number.'
        ],
    ],
    'notification' => [
        'title' => [
            'completed' => 'Withdraw Completed',
            'refunded' => 'Withdraw Refunded'
        ],
        'message' => [
            'completed' => 'Your withdraw request is completed : ',
            'refunded' => 'Your withdraw request is refunded.'
        ]
    ],
];
