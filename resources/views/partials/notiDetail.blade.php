@if($notification != null)
<div class="col-md-12 p-0 m-0">
    <div class="col-md-12 mb-1 bg-secondary">Notification Detail</div>
    <div class="col-md-12">
        <div class="row col-md-12">
            <table class="table-responsive">
            <tr>
                <th>Name</th>
                <th>Title</th>
                <th>Description</th>
            </tr>
            <tr>
            <td>{{$notification->notifiable->name}}</td>
            @foreach(json_decode($notification->data) as $data)
                <td>{{$data}}</td>
            @endforeach
            </tr>
            </table>
        </div>
    </div>
</div>
@endif