@if($banner != null)
<div class="col-md-12 p-0 m-0">
    <div class="col-md-12 mb-1 bg-secondary">Banner Gallery</div>
    <div class="col-md-12">
        <div class="row">
            @foreach(json_decode($banner->images) as $image)

            <div class="col-md-3 mb-5 col-sm-10">
                <a href="{{asset($image)}}" target="_blank" title="product image">
                    <img src="{{asset($image)}}" alt="product image" height="150px">
                </a>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endif