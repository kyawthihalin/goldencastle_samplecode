<div class="modal-header">
    <h4 class="modal-title" id="exampleModalLongTitle">Refund Withdraw</h4>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body mx-2">
    <div class="row">
        <div class="col-6 font-weight-bold">
            Requested Point
        </div>
        <div class="col-6">
            {{$withdraw_request->point}}
        </div>
        <div class="col-6 font-weight-bold">
            Amount To Transfer
        </div>
        <div class="col-6">
            {{$withdraw_request->point * config('point.relationship_amount')}}
        </div>
        <div class="col-6 font-weight-bold">
            Account Number
        </div>
        <div class="col-6">
            {{$withdraw_request->account_number}}
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-12 my-2">
            <label for="screenshot">Screenshot (Optional)</label>
            <input type="file" class="form-control" id="screenshot" name="screenshot">
            <span class="error error-screenshot text-danger"></span>
        </div>
        <div class="col-12 my-2">
            <label for="description">Description <span class="text-danger">*</span></label>
            <textarea class="form-control" name="description" id="description">Provided information are incorrect.</textarea>
            <span class="error error-description text-danger"></span>
        </div>
        <div class="col-12 my-2">
            <label for="password">Enter your password <span class="text-danger">*</span></label>
            <input type="password" class="form-control" name="password" id="password">
            <span class="error error-password text-danger"></span>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="closeModal()">Cancel</button>
    <button type="button" class="btn btn-primary" onclick="refundWithdraw('{{$withdraw_request->id}}')">Refund</button>
</div>

<script>
    function refundWithdraw(id) {
        var formData = new FormData();
        formData.append('description', $('textarea[name="description"]').val());
        formData.append('password', $('input[name="password"]').val());

        if ($('input[name="screenshot"]')[0].files[0]) {
            formData.append('screenshot', $('input[name="screenshot"]')[0].files[0]);
        }

        $.ajax({
            url: `agent-withdraw-request/${id}/refund`,
            method: 'POST',
            data: formData,
            processData: false,
            contentType: false,
            success: function(data) {
                Swal.fire({
                    icon: 'success',
                    title: 'Success',
                    text: 'Withdraw refund completed.',
                    showCloseButton: false,
                    showCancelButton: false,
                    width: 800,
                    showConfirmButton: false,
                    didClose: () => {
                        window.location.reload();
                    }
                })
            },
            error: function(error) {
                if (error.status === 422) {
                    $.each(error.responseJSON.errors, function(key, value) {
                        $(`.error-${key}`).text(value[0]);
                    })
                }
                if (error.status === 401) {
                    $(`.error-password`).text(error.responseJSON.message);
                }
                if (error.status === 400) {
                    closeModal();
                    Toast.fire({
                        icon: 'error',
                        title: error.responseJSON.message
                    });
                }
            }
        })
    }
</script>