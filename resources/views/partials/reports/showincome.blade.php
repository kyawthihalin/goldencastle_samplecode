<style>
    table {
        width: 100%;
    }

    table,
    th,
    td {
        border-collapse: collapse;
        border: 1px solid #a8a8a8;
    }

    th {
        text-align: center;
        padding: 5px;
    }

    td {
        padding: 5px;
    }
</style>
@if(count($incomes) > 0)
<table>
    <thead>
        <th>No</th>
        <th>Reference No</th>
        <th>User ID</th>
        <th>User Name</th>
        <th>Ticket Number</th>
        <th>Purchased Date</th>
        <th>Draw ID</th>
        <th>Ticket Price</th>
        <th>Agent Commission</th>
        <th>Profit</th>
    </thead>

    <tbody>
        @php
        $count = 1;
        @endphp
        @foreach($incomes as $income)
        <tr>
            <td>{{$count++}}</td>
            <td>{{$income->reference_id}}</td>
            <td>{{$income->user->reference_id}}</td>
            <td>{{$income->user->name}}</td>
            <td>{{$income->number}}</td>
            <td>{{$income->purchased_date}}</td>
            <td>{{$income->draw_id}}</td>
            <td>{{number_format($income->amount)}} MMK</td>
            <td>{{number_format($income->commission)}} MMK</td>
            <td>{{number_format($income->profit)}} MMK</td>
        </tr>
        @endforeach
        <tr>
            <td colspan="7" style="text-align: right;font-weight:bold">Total</td>
            <td style="font-weight:bold">{{number_format($total_price,0)}}</td>
            <td style="font-weight:bold">{{number_format($total_commission,0)}}</td>
            <td style="font-weight:bold">{{number_format($total_profit,0)}}</td>
        </tr>
    </tbody>

</table>
<br><br>
@else
<h3 style="text-align: center;font-weight: 500;opacity: 0.4;text-shadow: 1px 1px;padding: 30px;font-size: 25px">No Data Found !!!</h3>
@endif