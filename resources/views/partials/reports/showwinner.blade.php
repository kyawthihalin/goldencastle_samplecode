<style>
    table {
        width: 100%;
    }

    table,
    th,
    td {
        border-collapse: collapse;
        border: 1px solid #a8a8a8;
    }

    th {
        text-align: center;
        padding: 5px;
    }

    td {
        padding: 5px;
    }
</style>
@if(count($winners) > 0)
<table>
    <thead>
        <th>No</th>
        <th>Ticket No</th>
        <th>Winner Name</th>
        <th>Agent Name</th>
        <th>Result Type</th>
        <th>Prize</th>
        <th>Draw ID</th>
    </thead>

    <tbody>
        @php
        $count = 1;
        @endphp
        @foreach($winners as $winner)
        <tr>
            <td>{{$count++}}</td>
            <td>{{$winner->user_ticket->number}}</td>
            <td>{{$winner->user_ticket->user->name}}</td>
            <td>{{$winner->user_ticket->user->agent->name}}</td>
            <td>{{ucfirst(str_replace('_', ' ', $winner->result_type->name))}}</td>
            <td>{{number_format($winner->amount,0)}}</td>
            <td>{{$winner->draw_id}}</td>
        </tr>
        @endforeach
    </tbody>

</table>
<br><br>
@else
<h3 style="text-align: center;font-weight: 500;opacity: 0.4;text-shadow: 1px 1px;padding: 30px;font-size: 25px">No Data Found !!!</h3>
@endif