@extends(backpack_view('blank'))
@section('content')
<script type="text/javascript">
  function display_c() {
    var refresh = 1000; // Refresh rate in milli seconds
    mytime = setTimeout('display_ct()', refresh)
  }

  function display_ct() {
    var x = new Date();
    var h = x.getHours(),
      m = x.getMinutes() + ":" + x.getSeconds();
    var ime = (h > 12) ? (h - 12 + ':' + m + ' PM') : (h + ':' + m + ' AM');
    document.getElementById('ct').innerHTML = ime;
    display_c();
  }
</script>
<section class="content-header" style="padding-top: 5px">
  <strong>
    <h1 class="container-fluid" style="font-family:Times New Roman; font-size:35px; font-weight: bold !important; color: #404e67;">
      Welcome Back, {{backpack_user()->name}} !
    </h1>
  </strong>
  <h3 class="container-fluid mb-5" style="padding-top: 0px;margin-top: 10px;">
    <small style="font-family: Centaur; font-size: 20px; font-weight: bold; color: #60099e;">
      Today is <?php
                $date = Carbon\Carbon::now();
                echo date('l, d F Y', strtotime($date)); //June, 2017
                ?>

      <body onload=display_ct();>
        <span id='ct'></span>
      </body>
    </small>
  </h3>
</section>
@endsection

@push('after_styles')
<link href="{{ asset('/css/style.css') }}" rel="stylesheet" />
<link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300&display=swap" rel="stylesheet">
@endpush
<?php
$widgets['after_content'][] = [
    'type' => 'div',
    'class' => 'row pt-2 customwidth',
    'content' => [
        [
            'type'       => 'card',
            'wrapper' => ['class' => 'col-sm-6 col-md-3'], // optional
            'class'   => 'card text-white customcard1', // optional
            'content'    => [
                'body'   => '<p class="icon"><i class="las la-user-lock" style="font-size:23px;padding-top:4px;"></i>&nbsp;&nbsp;&nbsp;<span class="title">Total Admin</span></p><p class="total_amount">' . $admin_count . ' Users </p>
                    <div class="view_detail"><a href="/admin/admin" style="color:white;font-weight:bold">View Detail</a> </div>'
            ]
        ],
        [
            'type'       => 'card',
            'wrapper' => ['class' => 'col-sm-6 col-md-3'], // optional
            'class'   => 'card text-white customcard3', // optional
            'content'    => [
                'body'   => '<p class="icon"><i class="las la-store" style="font-size:23px;padding-top:4px;"></i>&nbsp;&nbsp;&nbsp;<span class="title">Total Agent</span></p><p class="total_amount">' . $agent_count . ' Users </p>
                    <div class="view_detail"><a href="/admin/agent" style="color:white;font-weight:bold">View Detail</a> </div>'
            ]
        ],
        [
            'type'       => 'card',
            'wrapper' => ['class' => 'col-sm-6 col-md-3'], // optional
            'class'   => 'card text-white customcard2', // optional
            'content'    => [
                'body'   => '<p class="icon"><i class="las la-users" style="font-size:23px;padding-top:4px;"></i>&nbsp;&nbsp;&nbsp;<span class="title">Total User</span></p><p class="total_amount">' . $user_count . ' Users</p>
                <div class="view_detail"><a href="/admin/user" style="color:white;font-weight:bold">View Detail</a> </div>'
            ]
        ],
        [
            'type'       => 'card',
            'wrapper' => ['class' => 'col-sm-6 col-md-3'], // optional
            'class'   => 'card text-white customcard3', // optional
            'content'    => [
                'body'   => '<p class="icon"><i class="las la-wallet" style="font-size:23px;padding-top:4px;"></i>&nbsp;&nbsp;&nbsp;<span class="title">Income (Current Draw) </span></p><p class="total_amount">' . $current_draw_income . ' MMK</p>
                <div class="view_detail"><a href="/admin/user-ticket" style="color:white;font-weight:bold">View Detail</a> </div>'
            ]
        ],
        [
            'type'       => 'card',
            'wrapper' => ['class' => 'col-sm-6 col-md-6'], // optional
            'class'   => 'card text-white customcard4', // optional
            'content'    => [
                'body'   => '<p class="icon"><i class="las la-wallet" style="font-size:23px;padding-top:4px;"></i>&nbsp;&nbsp;&nbsp;<span class="title">Total Win Tickets(Current Draw) </span></p><p class="total_amount">' . $total_win_ticket_count . ' MMK</p>
                <div class="view_detail"><a href="/admin/ticket-result" style="color:white;font-weight:bold">View Detail</a> </div>'
            ]
        ],

        [
            'type'       => 'card',
            'wrapper' => ['class' => 'col-sm-6 col-md-6'], // optional
            'class'   => 'card text-white customcard1', // optional
            'content'    => [
                'body'   => '<p class="icon"><i class="las la-wallet" style="font-size:23px;padding-top:4px;"></i>&nbsp;&nbsp;&nbsp;<span class="title">Ticket Price</span></p><p class="total_amount">' . $ticket_price . ' Points</p>
                <div class="view_detail"><a href="/admin/ticket-price" style="color:white;font-weight:bold">Change Price</a> </div>'
            ]
        ],
        [
            'type' => 'chart',
            'wrapperClass' => 'col-md-6',
            // 'class' => 'bg-white text-center',
            'controller' => \App\Http\Controllers\Admin\Charts\AgentByStateChartController::class,
            'content' => [
                'header' => "Agent by State", // optional
            ]
        ],
        [
            'type' => 'chart',
            'wrapperClass' => 'col-md-6',
            // 'class' => 'bg-white text-center',
            'controller' => \App\Http\Controllers\Admin\Charts\UserByStateChartController::class,
            'content' => [
                'header' => "User by State", // optional
            ]
        ],
        [
            'type' => 'chart',
            'wrapperClass' => 'col-md-12',
            // 'class' => 'bg-white text-center',
            'controller' => \App\Http\Controllers\Admin\Charts\MonthlyIncomeChartController::class,
            'content' => [
                'header' => "Monthly Income (". now()->format('Y') . ")", // optional
            ]
        ],
        [
            'type' => 'chart',
            'wrapperClass' => 'col-md-12',
            // 'class' => 'bg-white text-center',
            'controller' => \App\Http\Controllers\Admin\Charts\IncomeByCurrentDrawChartController::class,
            'content' => [
                'header' => "Daily Income (Current Month)", // optional
            ]
        ],

    ]
]
?>
