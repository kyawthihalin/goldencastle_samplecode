@if($entry->status == "Completed")
<span>-</span>
@elseif($entry->status == "Rejected")
<span>-</span>
@elseif($entry->status!="Rejected")
<?php
$url = '/' . $crud->route . '/' . $entry->getKey() . "/reject";
?>
<button onclick="confirmBox('{{$url}}', 'Reject Recharge Transaction', 'Reject')" class="btn btn-sm btn-danger"><i class="las la-trash"></i></button>
@endif