@if($entry->status === 'Requested')
<button title="Complete" class="btn btn-sm btn-success rounded-circle" onclick="openModal('agent-withdraw-request/withdraw_request/{{$entry->id}}/complete/show')"><span class="las la-check"></span></button>

<button title="Refund" class="btn btn-sm btn-secondary rounded-circle" onclick="openModal('agent-withdraw-request/withdraw_request/{{$entry->id}}/refund/show')"><span class="las la-sync"></span></button>
@endif