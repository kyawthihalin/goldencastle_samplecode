@if($entry->status == "Completed")
<span>-</span>
@elseif($entry->status == "Rejected")
<span>-</span>
@elseif($entry->status!="Rejected")
<?php
$url = '/' . $crud->route . '/' . $entry->getKey() . "/edit";
?>
<button onclick="confirmRecharge('{{$url}}', 'Confirm?', 'confirm')" class="btn btn-sm btn-success"><i class="las la-check"></i></button>
@endif