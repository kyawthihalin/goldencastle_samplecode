@if(!$entry->frozen_at)
<?php
$url = '/' . $crud->route . '/' . $entry->getKey() . "/freeze";
?>
<button onclick="confirmBox('{{$url}}', 'lock this account?', 'freeze')" class="btn btn-sm rounded-circle btn-success"><i class="las la-lock-open"></i></button>
@endif