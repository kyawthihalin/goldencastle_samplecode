@if($entry->frozen_at)
<?php
$url = '/' . $crud->route . '/' . $entry->getKey() . "/unfreeze";
?>
<button onclick="confirmBox('{{$url}}', 'unlock this account?', 'Unfreeze')" class="btn btn-sm rounded-circle btn-danger"><i class="las la-lock"></i></button>

@endif