@extends(backpack_view('blank'))
@push('after_styles')
<style>
    .card-1 {
        height: 140px;
        box-shadow: 0 0 2px 0 rgb(145 158 171 / 24%),
            0 16px 32px -4px rgb(145 158 171 / 24%);
        background: linear-gradient(to right, #047edf, #90caf9) !important;
    }

    .card-content {
        display: flex;
        flex-direction: column;
        padding-bottom: 4px;
    }

    .card-title {
        font-size: 19px;
    }

    .link-detail {
        text-decoration: none !important;
        align-self: end;
    }

    .badge {
        padding: .35em .5em;
    }

    .la {
        font-size: 1.45em;
    }

    .la-award{
        color: gold;
    }
</style>

@endpush
<?php
$widgets['before_content'][] = [
    'type' => 'div',
    'class' => 'container-fluid row',
    'content' => [
        [
            'type'       => 'card',
            'wrapper' => ['class' => 'col-sm-6 col-md-3'], // optional
            'class'   => 'card text-white card-1', // optional
            'content'    => [
                'body'   => '
                <div class="card-content">
                    <span class="card-title">
                        <i class="la la-users"></i>
                        Registered User
                    </span>
                    <p>' . $user_count . '</p>
                    <a  href="" class="badge badge-secondary link-detail">
                        View Detail
                    </a>
                </div>'
            ]
        ],
        [
            'type'       => 'card',
            'wrapper' => ['class' => 'col-sm-6 col-md-3'], // optional
            'class'   => 'card text-white card-1', // optional
            'content'    => [
                'body'   => '
                <div class="card-content">
                    <span class="card-title">
                        <i class="la la-user-secret"></i>
                        Agents
                    </span>
                    <p>' . $agent_count . '</p>
                    <a  href="" class="badge badge-secondary link-detail">
                        View Detail
                    </a>
                </div>'
            ]
        ],
        [
            'type'       => 'card',
            'wrapper' => ['class' => 'col-sm-6 col-md-3'], // optional
            'class'   => 'card text-white card-1', // optional
            'content'    => [
                'body'   => '
                <div class="card-content">
                    <span class="card-title">
                        <i class="la la-award"></i>
                        Last Winner Tickets
                    </span>
                    <p>' . $last_winner_ticket_count . '</p>
                    <a  href="" class="badge badge-secondary link-detail">
                        View Detail
                    </a>
                </div>'
            ]
        ],
        [
            'type'       => 'card',
            'wrapper' => ['class' => 'col-sm-6 col-md-3'], // optional
            'class'   => 'card text-white card-1', // optional
            'content'    => [
                'body'   => '
                <div class="card-content">
                    <span class="card-title">
                        <i class="la la-tag"></i>
                        Current Ticket Price
                    </span>
                    <p>' . $ticket_price . ' Ks</p>
                    <a  href="" class="badge badge-secondary link-detail">
                        View Detail
                    </a>
                </div>'
            ]
        ],
    ],
];
?>

@section('content')
@endsection