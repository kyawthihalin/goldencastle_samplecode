{{-- This file is used to store sidebar items, inside the Backpack admin panel --}}
@if(backpack_user()->can('dashboard'))
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('dashboard') }}"><i class="la la-home nav-icon"></i> {{ trans('backpack::base.dashboard') }}</a></li>
@endif

<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-files-o"></i> File Management</a>
    <ul class="nav-dropdown-items">
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('elfinder') }}"><i class="nav-icon la la-files-o"></i> <span>Upload File</span></a></li>
        @if(backpack_user()->can('list banner'))
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('banner') }}'><i class='nav-icon las la-file-image'></i> Banners</a></li>
        @endif
        @if(backpack_user()->can('list app file'))
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('app-file') }}'><i class='nav-icon la la-question'></i> App files</a></li>
        @endif

    </ul>
</li>

@if(backpack_user()->can('list user_ticket') || backpack_user()->can('list result') || backpack_user()->can('list ticket_result'))
<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon las la-ticket-alt"></i>Manage Tickets</a>
    <ul class="nav-dropdown-items">
        @if(backpack_user()->can('list user_ticket'))
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('user-ticket') }}'><i class='nav-icon la la-ticket-alt'></i>User tickets</a></li>
        @endif
        @if(backpack_user()->can('list result'))
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('result') }}'><i class='nav-icon la la-check-square'></i>Results</a></li>
        @endif
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('result-type') }}'><i class='nav-icon la la-check-square'></i> Result Types</a></li>
        @if(backpack_user()->can('list ticket_result'))
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('ticket-result') }}'><i class='nav-icon la la-award'></i>Winner Tickets</a></li>
        @endif
    </ul>
</li>
@endif
@if(backpack_user()->can('list recharge_accounts') || backpack_user()->can('list recharge_requests') || backpack_user()->can('list fill_points'))
<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon las la-landmark"></i> Recharge Section</a>
    <ul class="nav-dropdown-items">
        @if(backpack_user()->can('list recharge_accounts'))
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('recharge-account') }}'><i class='nav-icon las la-landmark'></i> Recharge Accounts</a></li>
        @endif
        @if(backpack_user()->can('list recharge_requests'))
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('recharge-request') }}'><i class='nav-icon las la-landmark'></i> Recharge requests</a></li>
        @endif
        @if(backpack_user()->can('list fill_points'))
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('fill-point') }}'><i class='nav-icon las la-wallet'></i> Fill points</a></li>
        @endif
    </ul>
</li>
@endif


@if(backpack_user()->can('list withdraw_requests') || backpack_user()->can('list withdraw_options'))
<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon las la-landmark"></i> Withdraw Section</a>
    <ul class="nav-dropdown-items">
        @if(backpack_user()->can('list withdraw_requests'))
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('withdraw-request') }}'><i class='nav-icon las la-landmark'></i>User Requests</a></li>
        @endif
        @if(backpack_user()->can('list withdraw_requests'))
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('agent-withdraw-request') }}'><i class='nav-icon la la-landmark'></i> Agent requests</a></li>
        @endif
        @if(backpack_user()->can('list withdraw_options'))
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('withdraw-option') }}'><i class='nav-icon las la-landmark'></i> Withdraw Options</a></li>
        @endif
    </ul>
</li>
@endif


@if(backpack_user()->can('list ticket_price') || backpack_user()->can('list app_version') || backpack_user()->can('list faq'))
<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon las la-cogs"></i> Project Setting</a>
    <ul class="nav-dropdown-items">
        @if(backpack_user()->can('list lottery date'))
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('lottery-date') }}'><i class='nav-icon la la-question'></i> Lottery dates</a></li>
        @endif
        @if(backpack_user()->can('list ticket_price'))
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('ticket-price') }}'><i class='nav-icon la la-tag'></i>Ticket price</a></li>
        @endif
        @if(backpack_user()->can('list app_version'))
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('app-version') }}'><i class='nav-icon las la-chart-line'></i> App versions</a></li>
        @endif
        @if(backpack_user()->can('list faq'))
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('faq') }}'><i class='nav-icon lab la-rocketchat'></i> Faqs</a></li>
        @endif
        @if(backpack_user()->can('list feedback'))
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('feedback') }}'><i class='nav-icon lab la-wpforms'></i> Feedback</a></li>
        @endif
        @if(backpack_user()->can('list exchange'))
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('exchange-rate') }}'><i class='nav-icon las la-dollar-sign'></i> Exchange rates</a></li>
        @endif
        @if(backpack_user()->can('list live_opening'))
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('live-opening') }}'><i class='nav-icon lab la-youtube'></i> Live Opening</a></li>
        @endif
        @if(backpack_user()->can('update agent_commission'))
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('agent-commission') }}'><i class='nav-icon la la-question'></i> Agent commissions</a></li>
        @endif
        @if(backpack_user()->can('update app contact'))
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('app-contact') }}'><i class='nav-icon la la-question'></i> App contacts</a></li>
        @endif
        @if(backpack_user()->can('list account deletion'))
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('account-deletion') }}'><i class='nav-icon las la-trash'></i>Deletion Requests</a></li>
        @endif
    </ul>
</li>
@endif
@if(backpack_user()->can('list user_notification') || backpack_user()->can('list agent_notification') || backpack_user()->can('list app_notification') || backpack_user()->can('list topic_notification'))
<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon las la-paper-plane"></i>Notifications</a>
    <ul class="nav-dropdown-items">
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('topic') }}'><i class='nav-icon las la-comment'></i> Topics</a></li>
        @if(backpack_user()->can('list user_notification'))
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('notification') }}'><i class='nav-icon las la-comment'></i>User Notifications</a></li>
        @endif
        @if(backpack_user()->can('list agent_notification'))
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('agent-notification') }}'><i class='nav-icon las la-comment'></i> Agent notifications</a></li>
        @endif
        @if(backpack_user()->can('list app_notification'))
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('app-notification') }}'><i class='nav-icon las la-comment'></i> App notifications</a></li>
        @endif
    </ul>
</li>
@endif
@if(backpack_user()->can('list income') || backpack_user()->can('list winner'))
<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon las la-print"></i> Reports Section</a>
    <ul class="nav-dropdown-items">
        @if(backpack_user()->can('list income'))
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('income-report/create') }}'><i class='nav-icon las la-print'></i> Income reports</a></li>
        @endif
        @if(backpack_user()->can('list winner'))
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('winner-report/create') }}'><i class='nav-icon las la-print'></i> Winner reports</a></li>
        @endif
    </ul>
</li>
@endif

<!-- Users, Roles, Permissions -->
@if(backpack_user()->can('list user') || backpack_user()->can('list agent') || backpack_user()->can('list contact message'))
<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-users"></i> User Management</a>
    <ul class="nav-dropdown-items">
        @if(backpack_user()->can('list user'))
        <li class='nav-item'><a class='nav-link' href="{{ backpack_url('user') }}"><i class='nav-icon la la-users'></i>Users</a></li>
        @endif
        @if(backpack_user()->can('list agent'))
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('agent') }}"><i class="nav-icon la la-user-secret"></i>Agents</a></li>
        @endif
        @if(backpack_user()->can('list contact message'))
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('contact-message') }}'><i class='nav-icon la la-question'></i> Contact Messages</a></li>
        @endif

    </ul>
</li>
@endif

@if(backpack_user()->can('create user_password') || backpack_user()->can('create agent_password'))
<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-key"></i> Change Password</a>
    <ul class="nav-dropdown-items">
        @if(backpack_user()->can('create user_password'))
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('password/create') }}'><i class='nav-icon las la-key'></i>User</a></li>
        @endif
        @if(backpack_user()->can('create agent_password'))
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('agent-password/create') }}'><i class='nav-icon las la-key'></i> Agent</a></li>
        @endif
    </ul>
</li>
@endif

@if(backpack_user()->can('list admin') || backpack_user()->can('list role'))
<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-users"></i> Authentication</a>
    <ul class="nav-dropdown-items">
        @if(backpack_user()->can('list admin'))
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('admin') }}"><i class="nav-icon la la-user"></i> <span>Admins</span></a></li>
        @endif
        @if(backpack_user()->can('list role'))
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('role') }}"><i class="nav-icon la la-id-badge"></i> <span>Roles</span></a></li>
        @endif
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('edit-account-info') }}"><i class="nav-icon la la-id-badge"></i> <span>Account Info</span></a></li>
    </ul>
</li>
@endif