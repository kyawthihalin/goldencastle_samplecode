<?php

use App\Actions\GetCurrentDrawId;
use App\Interfaces\LotteryApiInterface;
use App\Services\DrawIdService;
use App\Services\LotteryApi\LotteryService;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
  // dd((new DrawIdService())->getPreviousDrawId());
  // Set the locale to Thai
  // Carbon::setLocale('th');

  // // Thai month names to English month names mapping
  // $thaiToEnglishMonths = [
  //   'มกราคม' => 'January',
  //   'กุมภาพันธ์' => 'February',
  //   'มีนาคม' => 'March',
  //   'เมษายน' => 'April',
  //   'พฤษภาคม' => 'May',
  //   'มิถุนายน' => 'June',
  //   'กรกฎาคม' => 'July',
  //   'สิงหาคม' => 'August',
  //   'กันยายน' => 'September',
  //   'ตุลาคม' => 'October',
  //   'พฤศจิกายน' => 'November',
  //   'ธันวาคม' => 'December',
  // ];

  // // Create a Carbon instance from the Thai date
  // $thaiDate = '16 พฤษภาคม 2566';
  // $parts = explode(' ', $thaiDate);
  // $englishMonth = $thaiToEnglishMonths[$parts[1]];
  // $englishYear = $parts[2] - 543; // Convert Thai year to Western year
  // $englishDate = $parts[0] . ' ' . $englishMonth . ' ' . $englishYear;

  // $date = Carbon::createFromFormat('d F Y', $englishDate);

  // // Format the date in English
  // $englishDateFormatted = $date->format('Y-m-d'); // or any other desired format

  // echo $englishDateFormatted;
});

Route::get('/results', function () {
  dd((new LotteryService())->getResults('2023-05-16'));
});
