<?php

use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\Artisan;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->purpose('Display an inspiring quote');

Artisan::command('basedecode', function () {
    $array = ['sdlaf', 'ddd', 'dasd'];

    $new_array = array_shift($array);

    dd($array);
});

Artisan::command('logs:clean', function() {
    exec('rm -f ' . storage_path('logs/dev.log'));
    $this->comment('Dev Logs cleaned!');
    
})->describe('Clean the development log file');