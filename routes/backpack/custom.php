<?php

use App\Http\Controllers\Admin\AgentWithdrawRequestCrudController;
use App\Http\Controllers\Admin\WithdrawRequestCrudController;
use Illuminate\Support\Facades\Route;

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => array_merge(
        (array) config('backpack.base.web_middleware', 'web'),
        (array) config('backpack.base.middleware_key', 'admin')
    ),
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes
    Route::get('dashboard', 'DashboardController@index');

    Route::crud('agent', 'AgentCrudController');
    Route::crud('user', 'UserCrudController');
    Route::get('user/{user}/freeze', 'UserCrudController@freeze');
    Route::get('user/{user}/unfreeze', 'UserCrudController@unfreeze');

    Route::get('recharge-request/{recharge_request}/confirm', 'RechargeRequestCrudController@confirm');
    Route::get('recharge-request/{recharge_request}/reject', 'RechargeRequestCrudController@reject');
    Route::crud('admin', 'AdminCrudController');
    Route::get('admin/{admin}/freeze', 'AdminCrudController@freeze');
    Route::get('admin/{admin}/unfreeze', 'AdminCrudController@unfreeze');

    Route::get('agent/{agent}/freeze', 'AgentCrudController@freeze');
    Route::get('agent/{agent}/unfreeze', 'AgentCrudController@unfreeze');

    Route::crud('app-type', 'AppTypeCrudController');
    Route::crud('user-ticket', 'UserTicketCrudController');
    Route::crud('ticket-price', 'TicketPriceCrudController');
    Route::crud('result', 'ResultCrudController');
    Route::crud('ticket-result', 'TicketResultCrudController');
    Route::crud('app-version', 'AppVersionCrudController');
    Route::crud('faq', 'FaqCrudController');

    Route::prefix('options')->group(function () {
        Route::get('user', 'Option\UserController@index');
        Route::get('agent', 'Option\AgentController@index');
    });
    Route::get('charts/agent-by-state', 'Charts\AgentByStateChartController@response')->name('charts.agent-by-state.index');
    Route::get('charts/user-by-state', 'Charts\UserByStateChartController@response')->name('charts.user-by-state.index');
    Route::get('charts/income-by-current-draw', 'Charts\IncomeByCurrentDrawChartController@response')->name('charts.income-by-current-draw.index');
    Route::get('charts/monthly-income', 'Charts\MonthlyIncomeChartController@response')->name('charts.monthly-income.index');
    Route::crud('banner', 'BannerCrudController');
    Route::crud('ticket-photo', 'TicketPhotoCrudController');
    Route::crud('income-report', 'IncomeReportCrudController');
    Route::post('income/report', 'IncomeReportCrudController@report')->name('income_report');
    Route::crud('winner-report', 'WinnerReportCrudController');
    Route::post('winner/report', 'WinnerReportCrudController@report')->name('winner_report');
    Route::crud('notification', 'NotificationCrudController');
    Route::crud('feedback', 'FeedbackCrudController');
    Route::crud('exchange-rate', 'ExchangeRateCrudController');
    Route::crud('agent-notification', 'AgentNotificationCrudController');
    Route::crud('result-type', 'ResultTypeCrudController');
    Route::crud('topic', 'TopicCrudController');
    Route::crud('app-notification', 'AppNotificationCrudController');
    Route::crud('password', 'PasswordCrudController');
    Route::crud('agent-password', 'AgentPasswordCrudController');
    Route::crud('recharge-account', 'RechargeAccountCrudController');
    Route::crud('recharge-request', 'RechargeRequestCrudController');
    Route::crud('fill-point', 'FillPointCrudController');
    Route::get('fill-point/get-user-name', 'FillPointCrudController@getUserName');

    Route::crud('live-opening', 'LiveOpeningCrudController');
    Route::crud('withdraw-request', 'WithdrawRequestCrudController');
    Route::crud('withdraw-option', 'WithdrawOptionCrudController');

    Route::get('withdraw-request/{withdraw_request}/complete/show', [WithdrawRequestCrudController::class, 'completeShow']);
    Route::post('withdraw-request/{withdraw_request}/complete', [WithdrawRequestCrudController::class, 'complete']);

    Route::get('withdraw-request/{withdraw_request}/refund/show', [WithdrawRequestCrudController::class, 'refundShow']);
    Route::post('withdraw-request/{withdraw_request}/refund', [WithdrawRequestCrudController::class, 'refund']);


    Route::crud('agent-withdraw-request', 'AgentWithdrawRequestCrudController');

    Route::get('agent-withdraw-request/withdraw_request/{withdraw_request}/complete/show', [AgentWithdrawRequestCrudController::class, 'completeShow']);
    Route::post('agent-withdraw-request/{withdraw_request}/complete', [AgentWithdrawRequestCrudController::class, 'complete']);

    Route::get('agent-withdraw-request/withdraw_request/{withdraw_request}/refund/show', [AgentWithdrawRequestCrudController::class, 'refundShow']);
    Route::post('agent-withdraw-request/{withdraw_request}/refund', [AgentWithdrawRequestCrudController::class, 'refund']);
    Route::crud('agent-commission', 'AgentCommissionCrudController');
    Route::crud('app-contact', 'AppContactCrudController');
    Route::crud('contact-message', 'ContactMessageCrudController');
    Route::crud('app-file', 'AppFileCrudController');
    Route::crud('lottery-date', 'LotteryDateCrudController');
    Route::crud('account-deletion', 'AccountDeletionCrudController');
}); // this should be the absolute last line of this file