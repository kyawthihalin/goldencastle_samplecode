<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\Agent\UserListByAgentController;
use App\Http\Controllers\Api\Agent\Ticket\UserTicketsListByAgentController;
use App\Http\Controllers\Api\ChangeLanguageController;
use App\Http\Controllers\Api\User\Auth\ChangePasswordController;
use App\Http\Controllers\Api\Agent\ProfileController;
use App\Http\Controllers\Api\Agent\ResultController;
use App\Http\Controllers\Api\Agent\UserTickerController;
use App\Http\Controllers\Api\User\NotificationController;
use App\Http\Controllers\Api\Agent\Withdraw\WithdrawRequestController;
use App\Http\Controllers\Api\Ticket\ResultController as ResultAuthController;
use App\Http\Controllers\Api\User\BannerController;
use App\Http\Controllers\Api\Ticket\DrawIdController;
use App\Http\Controllers\Api\FaqController;
use App\Http\Controllers\Api\ContactUsController;
use App\Http\Controllers\Api\CountDownController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('get/users/list', [UserListByAgentController::class, '__invoke']);
Route::get('get/user-tickets', [UserTicketsListByAgentController::class, '__invoke']);
Route::post('language/update', [ChangeLanguageController::class, '__invoke']); //ok
Route::post('password/update', [ChangePasswordController::class, '__invoke']); //ok
Route::get('profile', [ProfileController::class, 'index']); 
Route::post('profile/update', [ProfileController::class, 'update']);
Route::get('notifications', [NotificationController::class, 'index']);
Route::post('notifications/{notification}/read', [NotificationController::class, 'read']);
Route::post('notifications/read-all', [NotificationController::class, 'readAll']);


Route::prefix('withdraw')->group(function () {
    Route::get('options', [WithdrawRequestController::class, 'options']);
    Route::post('/', [WithdrawRequestController::class, 'withdraw']);
    Route::get('records', [WithdrawRequestController::class, 'records']);
});

Route::get('results/winners', [ResultController::class, 'winners']);
Route::post('count-down', [CountDownController::class, '__invoke']);


Route::post('banners', [BannerController::class, '__invoke']);
Route::post('results', [ResultAuthController::class, 'index']);
Route::post('results/check', [ResultAuthController::class, 'check']);
Route::post('draw-ids', [DrawIdController::class, '__invoke']);
Route::post('results/winners', [ResultAuthController::class, 'winners']);
Route::post('tickets/users', [UserTickerController::class, '__invoke']);
Route::post('results/numbers', [ResultAuthController::class, 'numbers']);
Route::post('faqs', [FaqController::class, '__invoke']);
Route::post('helps', [ContactUsController::class, '__invoke']);