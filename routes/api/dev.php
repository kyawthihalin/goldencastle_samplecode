<?php

use App\Actions\GetLastDrawsDate;
use App\Actions\SendNotification;
use App\Services\Crypto;
use App\Services\DrawIdService;
use App\Services\LotteryApi\RayRiffyService;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('encrypt', function () {
    $encrypted = (new Crypto())->encrypt(request()->all());

    return response()->json([
        'encrypted' => $encrypted
    ]);
});

Route::post('decrypt', function () {
    $decrypted = (new Crypto())->decrypt(request()->data);

    return response()->json($decrypted);
});

Route::get('lottery-results', function () {
    Artisan::call('create:result');
});

Route::get('/test', function () {
    request()->validate([
        'token' => ['required', 'string'],
        'data' => ['required', 'array'],
    ]);
    (new SendNotification())->execute([request()->token], request()->data);
});

Route::prefix('get')->group(function () {
    Route::post('last-draw', function () {
        request()->validate([
            'date' => ['date', 'date_format:Y-m-d']
        ]);

        dd((new DrawIdService())->getPreviousDrawId(request()->date));
    });

    Route::post('current-draw', function () {
        request()->validate([
            'date' => ['date', 'date_format:Y-m-d']
        ]);

        dd((new DrawIdService())->getCurrentDrawId(request()->date));
    });

    Route::post('last-draw-dates', function () {
        request()->validate([
            'count' => ['integer'],
            'date' => ['date', 'date_format:Y-m-d']
        ]);

        dd((new GetLastDrawsDate())->execute(request()->count, request()->date));
    });
});

Route::get('/lottery-results', function (RayRiffyService $apiService) {
    // $bot_token = config('services.telegram.bot_token');
    // Http::post("https://api.telegram.org/bot{$bot_token}/" . 'sendPhoto', [
    //     'chat_id' => "-1001911886196",
    //     'caption' => 'Bot test',
    //     'photo' => 'https://media.npr.org/assets/img/2017/09/12/macaca_nigra_self-portrait-3e0070aa19a7fe36e802253048411a38f14a79f8-s900-c85.webp'
    // ]);
    // $apiService->getResults('2023-05-16');
});
