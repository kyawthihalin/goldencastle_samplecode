<?php

use App\Http\Controllers\Api\ChangeLanguageController;
use App\Http\Controllers\Api\User\Auth\ChangePasswordController;
use App\Http\Controllers\Api\User\FeedbackController;
use App\Http\Controllers\Api\User\NotificationController;
use App\Http\Controllers\Api\User\ProfileController;
use App\Http\Controllers\Api\User\RechargeController;
use App\Http\Controllers\Api\User\Ticket\PurchaseController;
use App\Http\Controllers\Api\User\Ticket\PurchasedTicketController;
use App\Http\Controllers\Api\User\WithdrawController;
use App\Models\User;
use App\Services\Crypto;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

if (!app()->environment('production')) {
    Route::post('encrypt', function () {
        $encrypted = (new Crypto())->encrypt(request()->all());

        return response()->json([
            'encrypted' => $encrypted
        ]);
    })->withoutMiddleware(['decrypted', 'locale']);

    Route::post('decrypt', function () {
        $decrypted = (new Crypto())->decrypt(request()->data);

        return response()->json($decrypted);
    })->withoutMiddleware(['decrypted', 'locale']);
}
Route::post('tickets/purchase', [PurchaseController::class, '__invoke']);
Route::post('tickets/purchased', [PurchasedTicketController::class, '__invoke']);

// profile routs
Route::post('language/update', [ChangeLanguageController::class, '__invoke']); //ok
Route::post('password/update', [ChangePasswordController::class, '__invoke']); //ok
Route::post('profile', [ProfileController::class, 'index']);
Route::post('profile/update', [ProfileController::class, 'update']);
Route::post('feedback', [FeedbackController::class, '__invoke']);
Route::post('notifications', [NotificationController::class, 'index']);
Route::post('notifications/{notification}/read', [NotificationController::class, 'read']);
Route::post('notifications/read-all', [NotificationController::class, 'readAll']);
Route::post('notifications/won', [NotificationController::class, 'won']);
Route::post('notifications/read/won', [NotificationController::class, 'readWon']);
Route::post('request/account-deletion', [ProfileController::class, 'deleteRequest']);
Route::prefix('recharge')->group(function () {
    Route::post('/', [RechargeController::class, 'recharge']);
    Route::post('accounts', [RechargeController::class, 'accounts']);
    Route::post('records', [RechargeController::class, 'records']);

    // comment the code when production stage
    Route::post('cancel', function () {
        $requested = auth()->user()->recharge_requests()->where('status', 'Requested')->first();

        if (!$requested) {
            return response()->json([
                'message' => 'Not Requested'
            ], 422);
        }

        $requested->delete();
        return response()->json([
            'message' => 'Request Cancelled'
        ]);
    });
});

Route::prefix('withdraw')->group(function () {
    Route::post('options', [WithdrawController::class, 'options']);
    Route::post('/', [WithdrawController::class, 'withdraw']);
    Route::post('records', [WithdrawController::class, 'records']);
});
