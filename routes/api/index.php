<?php

use App\Http\Controllers\Api\Agent\Auth\LoginController as AgentLoginController;
use App\Http\Controllers\Api\ContactUsController;
use App\Http\Controllers\Api\CountDownController;
use App\Http\Controllers\Api\FaqController;
use App\Http\Controllers\Api\Ticket\DrawIdController;
use App\Http\Controllers\Api\Ticket\LiveController;
use App\Http\Controllers\Api\Ticket\ResultController;
use App\Http\Controllers\Api\User\Auth\LoginController as UserLoginController;
use App\Http\Controllers\Api\User\Auth\RegisterController;
use App\Http\Controllers\Api\User\BannerController;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('user/login', [UserLoginController::class, '__invoke']);
Route::post('user/register', [RegisterController::class, '__invoke']);
Route::post('agent/login', [AgentLoginController::class, '__invoke']);

Route::post('count-down', [CountDownController::class, '__invoke']);

Route::post('banners', [BannerController::class, '__invoke']);
Route::post('results', [ResultController::class, 'index']);
Route::post('results/check', [ResultController::class, 'check']);
Route::post('draw-ids', [DrawIdController::class, '__invoke']);
Route::post('results/winners', [ResultController::class, 'winners']);
Route::post('tickets/live', [LiveController::class, '__invoke']);
Route::post('results/numbers', [ResultController::class, 'numbers']);
Route::post('faqs', [FaqController::class, '__invoke']);
Route::post('helps', [ContactUsController::class, '__invoke']);