<?php

namespace Database\Factories;

use App\Actions\GenerateReference;
use App\Models\Admin;
use App\Models\State;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class AgentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'admin_id' => Admin::all()->random(),
            'state_id' => State::all()->random(),
            'code' => Str::uuid(),
            'password' => bcrypt('password'),
            // 'reference_id' => $this->faker->unique()->randomNumber(8),
            'name' =>  $this->faker->name(),
            'nrc' => '2/JaMaLa(N)696969',
            'device_name' =>  $this->faker->word(),
            'device_model' =>  $this->faker->word(),
            'os_version' => '1.1l1',
            'os_type' => 'iOS',
            'app_version_id' => '1.2.3',
            'point' => 10000,
        ];
    }
}
