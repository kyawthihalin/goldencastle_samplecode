<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name(),
            'user_name' => $this->faker->unique()->userName(),
            'reference_id' => $this->faker->unique()->randomNumber(8),
            'device_name' => $this->faker->word(),
            'device_model' => $this->faker->word(),
            'os_version' => '1.1l1',
            'os_type' => 'iOS',
            'app_version_id' => '1.2.3',
            'noti_token' => Str::random(80),
            'password' => bcrypt('password'), // password
        ];
    }
}
