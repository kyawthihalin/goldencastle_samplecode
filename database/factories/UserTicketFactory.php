<?php

namespace Database\Factories;

use App\Actions\GenerateReferenceId;
use App\Actions\GetLastDrawsDate;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserTicketFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $reference_id = (new GenerateReferenceId())->execute('UT', '09' . rand(pow(10, 10 - 1), pow(10, 10) - 1));
        return [
            'user_id' => User::all()->random(),
            'reference_id' => $reference_id,
            'number' => rand(pow(10, 6 - 1), pow(10, 6) - 1),
            'purchased_date' => now()->subDays(random_int(1, 60)),
            'draw_id' => $this->faker->randomElement((new GetLastDrawsDate())->execute(2)),
            'amount' => 7000,
            'profit' => 6000,
            'commission' => 1000,
        ];
    }
}
