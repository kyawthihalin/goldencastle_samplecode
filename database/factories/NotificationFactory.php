<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class NotificationFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'type' => 'Notification',
            'notifiable_type' => get_class(User::first()),
            'notifiable_id' => User::all()->random(),
            'data' => json_encode([
                'title' => $this->faker->word(),
                'message' => $this->faker->sentence(),
            ]),
        ];
    }
}
