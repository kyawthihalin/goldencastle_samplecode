<?php

namespace Database\Seeders;

use App\Actions\GetLastDrawsDate;
use App\Constants\Lottery;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Artisan;

class ResultSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (app()->environment('production')) {
            return;
        }

        $prev_draw_ids = (new GetLastDrawsDate())->execute(2);

        foreach ($prev_draw_ids as $prev_draw_id) {
            Artisan::call('result:create', [
                'draw_id' => $prev_draw_id
            ]);
        }
    }
}
