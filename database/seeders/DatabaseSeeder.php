<?php

namespace Database\Seeders;

use App\Actions\GenerateReference;
use App\Actions\GetLastDrawsDate;
use App\Models\Agent;
use App\Models\Faq;
use App\Models\User;
use App\Models\UserTicket;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Artisan;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ExchangeRateSeeder::class);
        $this->call(AppTypeSeeder::class);
        $this->call(StateSeeder::class);
        $this->call(RechargeAccountSeeder::class);

        $this->call(PermissionGroupSeeder::class);
        $this->call(ResultTypeSeeder::class);
        // $this->call(ResultSeeder::class);
        $this->call(PermissionSeeder::class);
        $this->call(RoleSeeder::class);
        $this->call(AgentCommissionSeeder::class);
        $this->call(AdminSeeder::class);
        $this->call(TicketPriceSeeder::class);
        $this->call(AppFileSeeder::class);
        $this->call(BannerSeeder::class);
        $this->call(WithdrawOptionSeeder::class);

        if (!app()->environment('production')) {
            if (!Faq::first()) {
                Faq::factory(10)->create();
            }

            if (!Agent::first()) {
                $this->command->info('Seeding agents and users...');
                Agent::factory(5)
                    ->has(User::factory()->count(rand(4, 10)))
                    ->create();

                foreach (Agent::all() as $agent) {
                    $agent->update([
                        'code' => (new GenerateReference())->execute($agent->state->code, $agent->sequence, 4, 0)
                    ]);
                }
                $this->call(UserSeeder::class);
            }
            if (!UserTicket::first()) {
                $this->command->info('Seeding user tickets..');
                UserTicket::factory(5000)->create();
            }

            $this->command->info('Calculating result..');

            foreach ((new GetLastDrawsDate())->execute(2) as $draw_id) {
                Artisan::call('result:calculate', [
                    'draw_id' => $draw_id
                ]);
            }
        }
        $this->call(PasswordGrantClientSeeder::class);
        $this->call(LiveOpeningSeeder::class);
        $this->call(AppContactSeeder::class);
        $this->call(LotteryDateSeeder::class);
        // Notification::factory(1000)->create();
    }
}
