<?php

namespace Database\Seeders;

use App\Models\LotteryDate;
use App\Services\DrawIdService;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LotteryDateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $lottery_dates = [
            '01-17',
            '02-01',
            '02-16',
            '03-01',
            '03-16',
            '04-01',
            '04-16',
            '05-02',
            '05-16',
            '06-01',
            '06-16',
            '07-01',
            '07-16',
            '07-31',
            '08-16',
            '09-01',
            '09-16',
            '10-01',
            '10-16',
            '11-01',
            '11-16',
            '12-01',
            '12-16',
        ];

        foreach ($lottery_dates as $lottery_date) {
            $exist = LotteryDate::where('date', $lottery_date)->first();

            if (!$exist) {
                LotteryDate::create([
                    'date' => $lottery_date
                ]);
            }
        }
    }
}
