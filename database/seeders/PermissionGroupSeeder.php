<?php

namespace Database\Seeders;

use App\Models\PermissionGroup;
use Illuminate\Database\Seeder;

class PermissionGroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $groups = [
            'General',
            'Project Setting',
            'User Management',
            'Ticket Manage',
            'Authorization',
            'File Management',
            'Report Section',
            'Notification Section',
            'Change Password',
            'Recharge Section',
            'Withdraw',
        ];

        foreach ($groups as $group) {
            PermissionGroup::create([
                'name' => $group
            ]);
        }
        PermissionGroup::whereNotIn('name', $groups)->delete();
    }
}
