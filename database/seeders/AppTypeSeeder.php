<?php

namespace Database\Seeders;

use App\Models\AppType;
use Illuminate\Database\Seeder;

class AppTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach(['agent', 'user'] as $app_type) {
            $existed = AppType::whereName($app_type)->first();

            if(!$existed){
                AppType::create([
                    'name' => $app_type
                ]);
            }
        }
    }
}
