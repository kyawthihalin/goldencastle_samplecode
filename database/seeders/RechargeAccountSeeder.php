<?php

namespace Database\Seeders;

use App\Models\RechargeAccount;
use Illuminate\Database\Seeder;

class RechargeAccountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $accounts = [
            [
                'name' => 'KBZ Pay',
                'account_name' => 'Sai Khunt Eain',
                'phone_number' => '+95912345678'
            ],
            [
                'name' => 'Wave Money',
                'account_name' => 'Sai Khunt Eain',
                'phone_number' => '+95912345678'
            ]
        ];

        foreach ($accounts as $account) {
            $exist = RechargeAccount::whereName($account['name'])->first();

            if (!$exist) {
                RechargeAccount::create($account);
            }
        }
    }
}
