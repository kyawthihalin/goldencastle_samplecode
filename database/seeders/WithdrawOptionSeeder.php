<?php

namespace Database\Seeders;

use App\Models\WithdrawOption;
use Illuminate\Database\Seeder;

class WithdrawOptionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $options = [
            'KBZ Pay', 'Wave Pay', 'KBZ Bank'
        ];

        foreach ($options as $option) {
            $exist = WithdrawOption::whereName($option)->first();

            if (!$exist) {
                WithdrawOption::create([
                    'name' => $option,
                    'icon' => 'icon.jpg'
                ]);
            }
        }
    }
}
