<?php

namespace Database\Seeders;

use App\Models\AppContact;
use Illuminate\Database\Seeder;

class AppContactSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $contacts = [
            [
                'type' => 'Facebook',
                'link' => 'asidfjasdf'
            ],
            [
                'type' => 'Telegram',
                'link' => 'asidfjasdsf'
            ],
            [
                'type' => 'Viber',
                'link' => 'asidfsjasdf'
            ],
            [
                'type' => 'Phone Number',
                'link' => 'afdsidfjasdf'
            ],
            [
                'type' => 'Email',
                'link' => 'asidfjfdasdf'
            ],
        ];

        foreach($contacts as $contact){
            $exist = AppContact::where('type', $contact['type'])->where('link', $contact['link'])->first();

            if(!$exist){
                AppContact::create($contact);
            }
        }
    }
}
