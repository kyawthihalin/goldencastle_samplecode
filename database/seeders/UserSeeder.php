<?php

namespace Database\Seeders;

use App\Models\Agent;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Naung Ye Htet',
            'agent_id' => Agent::all()->random()->id,
            'user_name' => 'naungyehtet',
            'point' => 30000,
            'reference_id' => '01010101',
            'device_name' => 'IPhone 21 Pro Max Plus',
            'device_model' => 'IPhone 21 Pro Max Plus',
            'os_version' => '1.1l1',
            'os_type' => 'iOS',
            'app_version_id' => '1.2.3',
            'noti_token' => Str::random(80),
            'password' => bcrypt('NaungYe@123'), // password
        ]);

        User::create([
            'name' => 'Jue Jue',
            'agent_id' => Agent::all()->random()->id,
            'point' => 100000,
            'user_name' => 'jaingjaingpu',
            'reference_id' => '02020202',
            'device_name' => 'IPhone 21 Pro Max Plus',
            'device_model' => 'IPhone 21 Pro Max Plus',
            'os_version' => '1.1l1',
            'os_type' => 'iOS',
            'app_version_id' => '1.2.3',
            'noti_token' => Str::random(80),
            'password' => bcrypt('JaiPu@123'), // password
        ]);
    }
}
