<?php

namespace Database\Seeders;

use App\Constants\Lottery;
use App\Constants\Ticket;
use App\Models\Result;
use App\Models\ResultType;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Artisan;

class ResultTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $result_types = [
            ['name' => 'first', 'amount' => 6000000],
            [
                'name' => 'closest_first',
                'amount' => 2000000,
            ],
            ['name' => 'second', 'amount' => 3000000],
            ['name' => 'third', 'amount' => 1500000],
            ['name' => 'forth', 'amount' => 1000000],
            ['name' => 'fifth', 'amount' => 500000],
            ['name' => 'first_three', 'amount' => 300000],
            ['name' => 'last_three', 'amount' => 300000],
            ['name' => 'last_two', 'amount' => 150000],
        ];

        foreach ($result_types as $result_type) {
            $exist = ResultType::whereName($result_type['name'])->first();

            if (!$exist) {
                ResultType::create($result_type);
            }
        }
    }

    private function generateResultsByType(string $result_type)
    {
        $ticket_count = Ticket::RESUT_COUNT[$result_type];

        $results = [];
        $ticket_length = 6;
        if ($result_type === 'first_three' || $result_type === 'last_three') {
            $ticket_length = 3;
        }

        if ($result_type === 'last_two') {
            $ticket_length = 2;
        }

        while ($ticket_count > 0) {
            array_push($results, rand(pow(10, $ticket_length - 1), pow(10, $ticket_length) - 1));

            $ticket_count--;
        }
        return $results;
    }
}
