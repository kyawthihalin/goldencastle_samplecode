<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;

class PasswordGrantClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $clients = [
            [
                'provider' => 'users',
                'name' => 'User Password Grant Client'
            ],
            [
                'provider' => 'agents',
                'name' => 'Agent Password Grant Client'
            ],
        ];

        foreach($clients as $client){
            $exist = DB::table('oauth_clients')->where('name', $client['name'])->first();

            if(!$exist){
                Artisan::call('passport:client', [
                    '--password' => null, 
                    '--provider' => $client['provider'], 
                    '--name' => $client['name'],
                    '-n' => null,
                ]);
            }
        }
    }
}
