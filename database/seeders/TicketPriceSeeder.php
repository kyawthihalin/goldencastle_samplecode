<?php

namespace Database\Seeders;

use App\Models\TicketPrice;
use Illuminate\Database\Seeder;

class TicketPriceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(TicketPrice::first()){
            return;
        }
        TicketPrice::create([
            'price' => 7000
        ]);
    }
}
