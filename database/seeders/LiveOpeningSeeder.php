<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\LiveOpening;

class LiveOpeningSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!LiveOpening::first()) {
            LiveOpening::create([
                'link' => 'https://www.youtube.com/channel/UCArgLw34DK8YJwp6eXkytvA',
            ]);
        }
    }
}
