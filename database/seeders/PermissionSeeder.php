<?php

namespace Database\Seeders;

use App\Models\PermissionGroup;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $grouped_permissions = [
            'General' => [
                'dashboard'
            ],
            'Project Setting' => [
                'list ticket_price',
                'update ticket_price',
                'list app_version',
                'create app_version',
                'list faq',
                'create faq',
                'update faq',
                'delete faq',
                'list feedback',
                'list exchange',
                'create exchange',
                'update exchange',
                'delete exchange',
                'list live_opening',
                'update live_opening',
                'list app contact',
                'update app contact',
                'list contact message',
                'delete contact message',
                'list lottery date',
                'create lottery date',
                'delete lottery date',
                'list account deletion'
            ],
            'Authorization' => [
                'list role',
                'create role',
                'update role',
                'delete role',
            ],
            'User Management' => [
                'list admin',
                'create admin',
                'update admin',
                'delete admin',
                'freeze admin',
                'unfreeze admin',

                'list agent',
                'create agent',
                'update agent',
                'delete agent',
                'freeze agent',
                'unfreeze agent',

                'list user',
                'update user',
                'freeze user',
                'unfreeze user',

                'list user ticket'
            ],
            'Ticket Manage' => [
                'list user_ticket',
                'list result',
                'create result',
                'list ticket_result',
                'list result_type',
                'update result_type',
                'update agent_commission'
            ],
            'File Management' => [
                'list banner',
                'update banner',
                'list app file',
                'update app file',
            ],
            'Report Section' => [
                'list income',
                'create income',
                'update income',
                'delete income',
                'list winner',
                'create winner',
                'update winner',
                'delete winner',
            ],
            'Notification Section' => [
                'list topic',
                'create topic',
                'update topic',
                'delete topic',
                'list user_notification',
                'create user_notification',
                'list agent_notification',
                'create agent_notification',
                'list app_notification',
                'create app_notification',
                'list topic_notification',
                'create topic_notification',
            ],
            'Change Password' => [
                'list user_password',
                'create user_password',
                'update user_password',
                'delete user_password',
                'list agent_password',
                'create agent_password',
                'update agent_password',
                'delete agent_password',
            ],
            'Recharge Section' => [
                'list recharge_accounts',
                'create recharge_accounts',
                'update recharge_accounts',
                'delete recharge_accounts',
                'list recharge_requests',
                'update recharge_requests',
                'confirm recharge_requests',
                'reject recharge_requests',
                'list fill_points',
                'create fill_points',
            ],
            'Withdraw' => [
                'list withdraw_options',
                'create withdraw_options',
                'update withdraw_options',
                'delete withdraw_options',
                'list withdraw_requests',
                'complete withdraw_requests',
                'refund withdraw_requests',
            ]
        ];

        $admin_permissions = [];
        foreach ($grouped_permissions as $group => $permissions) {
            $group = PermissionGroup::where('name', $group)->first();
            array_merge($admin_permissions, $permissions);

            foreach ($permissions as $permission) {
                $existed = Permission::whereName($permission)->first();

                if (!$existed) {
                    $group->permissions()->create([
                        'name' => $permission
                    ]);
                }
            }
        }

        if (count($admin_permissions)) {
            Permission::whereNotIn($admin_permissions)->delete();
        }
    }
}
