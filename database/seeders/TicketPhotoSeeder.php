<?php

namespace Database\Seeders;

use App\Models\TicketPhoto;
use Illuminate\Database\Seeder;

class TicketPhotoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(TicketPhoto::first()){
            return;
        }
        TicketPhoto::create([
            'first_draw_photo' => "uploads/ticket_photo/lion.png",
            'second_draw_photo' => "uploads/ticket_photo/dragon.jfif",
        ]);
    }
}
