<?php

namespace Database\Seeders;

use App\Models\AgentCommission;
use Illuminate\Database\Seeder;

class AgentCommissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!AgentCommission::first()) {
            AgentCommission::create([
                'point' => 1000,
            ]);
        }
    }
}
