<?php

namespace Database\Seeders;

use App\Models\AppFile;
use Illuminate\Database\Seeder;

class AppFileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // last draw photo, first draw photo, ticket background photo
        $app_files = [
            [
                'file' => 'undefined',
                'type' => 'first draw photo',
            ],
            [
                'file' => 'undefined',
                'type' => 'last draw photo',
            ],
            [
                'file' => 'undefined',
                'type' => 'ticket background mobile',
            ],
            [
                'file' => 'undefined',
                'type' => 'ticket background website',
            ],
            [
                'file' => 'undefined',
                'type' => 'golden castle apk',
            ],
            [
                'file' => 'undefined',
                'type' => 'won photo',
            ],
            [
                'file' => 'undefined',
                'type' => 'lost photo',
            ],
        ];

        foreach ($app_files as $app_file) {
            $exist = AppFile::whereType($app_file['type'])->first();

            if (!$exist) {
                AppFile::create($app_file);
            }
        }
    }
}
