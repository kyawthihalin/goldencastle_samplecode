<?php

namespace Database\Seeders;

use App\Models\Admin;
use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Admin::create([
            'name' => "Golden Castle Admin",
            'email' => 'goldencastleadmin7117@gmail.com',
            'password' => !app()->environment('local') ?  bcrypt('gd7117c@stle') : bcrypt('password'),
        ]);

        $admin = Admin::where('name', 'Golden Castle Admin')->first();

        $admin->syncRoles(['Super Admin']);
    }
}
