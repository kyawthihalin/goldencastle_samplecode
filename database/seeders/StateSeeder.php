<?php

namespace Database\Seeders;

use App\Models\State;
use Illuminate\Database\Seeder;

class StateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $states = [
            [
                'name_en' => 'Ayeyarwady',
                'name_mm' => 'ဧရာဝတီတိုင်းဒေသကြီး',
                'code' => 'AY',
            ],
            [
                'name_en' => 'Bago',
                'name_mm' => 'ပဲခူးတိုင်းဒေသကြီး',
                'code' => 'BG'
            ],
            [
                'name_en' => 'Chin',
                'name_mm' => 'ချင်းပြည်နယ်',
                'code' => 'CN'
            ],
            [
                'name_en' => 'Kachin',
                'name_mm' => 'ကချင်ပြည်နယ်',
                'code' => 'KC',
            ],
            [
                'name_en' => 'Kayah',
                'name_mm' => 'ကယားပြည်နယ်',
                'code' => 'KY',
            ],
            [
                'name_en' => 'Kayin',
                'name_mm' => 'ကရင်ပြည်နယ်',
                'code' => 'KN'
            ],
            [
                'name_en' => 'Magway',
                'name_mm' => 'မကွေးတိုင်းဒေသကြီး',
                'code' => 'MG'
            ],
            [
                'name_en' => 'Mandalay',
                'name_mm' => 'မန္တလေးတိုင်းဒေသကြီး',
                'code' => 'MD'
            ],
            [
                'name_en' => 'Mon',
                'name_mm' => 'မွန်ပြည်နယ်',
                'code' => 'MN'
            ],
            [
                'name_en' => 'Nay Pyi Taw',
                'name_mm' => 'နေပြည်တော်',
                'code' => 'NP'
            ],
            [
                'name_en' => 'Rakhine',
                'name_mm' => 'ရခိုင်ပြည်နယ်',
                'code' => 'RK',
            ],
            [
                'name_en' => 'Sagaing',
                'name_mm' => 'စစ်ကိုင်းတိုင်းဒေသကြီး',
                'code' => 'SG'
            ],
            [
                'name_en' => 'Shan',
                'name_mm' => 'ရှမ်းပြည်နယ်',
                'code' => 'SN'
            ],
            [
                'name_en' => 'Tanintharyi',
                'name_mm' => 'တနင်္သာရီတိုင်းဒေသကြီး',
                'code' => 'TY'
            ],
            [
                'name_en' => 'Yangon',
                'name_mm' => 'ရန်ကုန်တိုင်းဒေသကြီး',
                'code' => 'YN'
            ],
        ];

        foreach ($states as $state) {
            $existed = State::whereNameEn($state['name_en'])->first();

            if (!$existed) {
                State::create($state);
            }
        }
    }
}
