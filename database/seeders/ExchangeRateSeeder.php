<?php

namespace Database\Seeders;

use App\Models\ExchangeRate;
use Illuminate\Database\Seeder;

class ExchangeRateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!ExchangeRate::first()) {
            ExchangeRate::create([
                'name' => 'Name because Kyaw Gyi added it',
                'flag' => 'Flag because kyaw Gyi added the flag with nullable.',
                'rate' => 10,
            ]);
        }
    }
}
