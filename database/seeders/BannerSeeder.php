<?php

namespace Database\Seeders;

use App\Models\Banner;
use Illuminate\Database\Seeder;

class BannerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (Banner::first()) {
            return;
        }
        Banner::create(
            [
                'images' => json_encode([
                    "uploads\\banner\\banner1.jfif",
                    "uploads\\banner\\banner2.jfif",
                ]),
                'type' => 'app'
            ],
        );
        Banner::create(
            [
                'images' => json_encode([
                    "uploads\\banner\\banner1.jfif",
                    "uploads\\banner\\banner2.jfif",
                ]),
                'type' => 'website'
            ]
        );
    }
}
