<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_results', function (Blueprint $table) {
            $table->uuid('id');
            $table->uuid('result_id');
            $table->uuid('result_type_id');
            $table->uuid('user_ticket_id');
            $table->double('amount')->default(0.00);
            $table->date('draw_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticket_results');
    }
}
