<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgentWithdrawRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agent_withdraw_requests', function (Blueprint $table) {
            $table->uuid('id')->unique();
            $table->uuid('withdraw_option_id');
            $table->bigIncrements('sequence');
            $table->string('reference_id')->unique();
            $table->uuid('agent_id');

            $table->string('payee');
            $table->string('account_number');
            $table->string('qr_code')->nullable();

            $table->unsignedInteger('point')->default(0);
            $table->unsignedInteger('withdrawn_amount')->default(0);
            
            $table->uuid('confirmed_by')->nullable();
            $table->string('screenshot')->nullable();
            $table->string('status')->default('Requested'); // Requested, Completed, Refunded   
            $table->string('description')->nullable();
            $table->dateTime('completed_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agent_withdraw_requests');
    }
}
