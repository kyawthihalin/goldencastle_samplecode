<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_tickets', function (Blueprint $table) {
            $table->uuid('id')->unique();
            $table->bigIncrements('sequence');
            $table->uuid('user_id');
            $table->string('reference_id')->unique();
            $table->string('number');
            $table->date('purchased_date');
            $table->date('draw_id');
            $table->unsignedDouble('amount');
            $table->unsignedDouble('commission');
            $table->unsignedDouble('profit');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_tickets');
    }
}
