<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRechargeAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recharge_accounts', function (Blueprint $table) {
            $table->uuid('id');
            $table->string('name');
            $table->string('account_name')->nullable();
            $table->string('phone_number')->nullable();
            $table->string('qr_code')->nullable();
            $table->string('account_photo')->nullable();
            $table->enum('status',['Active','Inactive'])->default('Active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recharge_accounts');
    }
}
