<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->uuid('id');
            $table->uuid('agent_id');
            $table->string('name');
            $table->unsignedInteger('point')->default(0);
            $table->string('reference_id')->unique();
            $table->string('phone_number')->nullable();
            $table->string('nrc')->nullable();
            $table->string('gender')->nullable();
            $table->string('user_name')->unique();
            $table->string('secret_key')->nullable();
            $table->string('password');
            $table->string('device_name');
            $table->string('device_model');
            $table->string('os_version');
            $table->string('os_type');
            $table->string('app_version_id');
            $table->string('fcm_token')->nullable();
            $table->dateTime('frozen_at')->nullable();
            $table->string('noti_token')->nullable();
            $table->tinyInteger('password_mistake_count')->default(0);
            $table->dateTime('password_mistook_at')->nullable();
            $table->string('language')->default('en');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
