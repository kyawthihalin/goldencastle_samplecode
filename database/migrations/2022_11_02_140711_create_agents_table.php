<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agents', function (Blueprint $table) {
            $table->uuid('id')->unique();
            $table->increments('sequence');
            $table->uuid('state_id');
            $table->uuid('admin_id');
            $table->string('name');
            // $table->string('reference_id')->unique();
            $table->string('code')->unique();
            $table->string('password');
            $table->string('nrc');
            $table->string("phone_number")->nullable();
            $table->string("secret_key")->nullable();
            $table->text("nrc_front")->nullable();
            $table->text("nrc_back")->nullable();
            // device details
            $table->string('fcm_token')->nullable();
            $table->string('device_name')->nullable();
            $table->string('device_model')->nullable();
            $table->string('os_version')->nullable();
            $table->string('os_type')->nullable();
            $table->string('app_version_id')->nullable();

            $table->dateTime('frozen_at')->nullable();
            $table->string('noti_token')->nullable();
            $table->integer('point')->default(0);
            $table->string('language')->default('en');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agents');
    }
}
