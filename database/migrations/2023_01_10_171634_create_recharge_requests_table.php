<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRechargeRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recharge_requests', function (Blueprint $table) {
            $table->uuid('id')->unique();
            $table->uuid('recharge_account_id');
            $table->bigIncrements('sequence');
            $table->string('reference_id')->unique();
            $table->uuid('user_id');
            $table->string('screenshot')->nullable();
            $table->unsignedInteger('confirmed_amount')->default(0);
            $table->uuid('confirmed_by')->nullable();
            $table->string('status')->default('Requested'); // Requested, Completed, Rejected   
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recharge_requests');
    }
}
